import React from 'react';
import {
  Alert,
  StyleSheet,
  Text,
  Platform,
  View,
  AsyncStorage,
  ActivityIndicator,
  ImageBackground,
  // NetInfo,
  Linking,
  AppState,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Geolocation from '@react-native-community/geolocation';
import {Provider} from 'react-redux';
import configureStore from './app/store/configureStore';
import LocationSwitch from 'react-native-location-switch';
import Routes from './app/routes/Route';
import OfflineNotice from './app/components/OfflineNotice';
import GPSNotice from './app/components/GPSNotice';
import PreLoader from './app/components/PreLoader';
import OneSignal from 'react-native-onesignal';
import {connect} from 'react-redux';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import API_URL from './app/configuration/configuration';
import database from '@react-native-firebase/database';

const store = configureStore();

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hideLocationBox: false,
      isConnected: true,
      token: '',
      isAppRated: {},
      appState: AppState.currentState,
      startTime: new Date().getTime(),
    };

    OneSignal.init('11a642c1-9671-4e10-ac73-ca5fba2c0cdf');
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.configure();
    // OneSignal.addEventListener('opened', this.onOpened);
  }

  hideLocationBoxHandler() {
    this.setState({hideLocationBox: true});
    this.rateSimSimMarkersApp();
  }
  componentWillMount() {
    OneSignal.configure();
  }

  rateSimSimMarkersApp = () => {
    const {counter, isRated, message} = this.state.isAppRated;
    const isRTL = this.props.isRTL;

    if (!isRated && counter === 4) {
      // const APP_STORE_LINK = 'itms-apps://itunes.apple.com/us/app/apple-store/id310633997?mt=8';
      const APP_STORE_LINK =
        'itms-apps://itunes.apple.com/us/app/id1464661512?mt=8';

      const PLAY_STORE_LINK = 'market://details?id=com.simsimproject';

      const AppURL = Platform.OS === 'ios' ? APP_STORE_LINK : PLAY_STORE_LINK;
      const heading = isRTL ? 'تقييم سم سم للأسواق' : 'Rate SimSim Markets';
      const message = isRTL
        ? 'هل تود تقييم سم سم للأسواق الآن؟'
        : 'Do you want to rate SimSim Markets App now?';
      const okButtonText = isRTL ? 'موافق' : 'Ok';
      const laterButtonText = isRTL ? 'لاحقا' : 'Later';
      const neverButtonText = isRTL ? 'ابدا' : 'Never';
      Alert.alert(
        heading,
        message,
        [
          {
            text: okButtonText,
            onPress: () => {
              Linking.openURL(AppURL).catch(error =>
                Alert.alert('Error', error.message),
              );
              AsyncStorage.setItem(
                'isAppRated',
                JSON.stringify({counter: 0, isRated: true, message: 'rated'}),
              );
            },
          },
          {
            text: laterButtonText,
            onPress: () => {
              AsyncStorage.setItem(
                'isAppRated',
                JSON.stringify({counter: 0, isRated: false, message: 'later'}),
              );
            },
          },
          {
            text: neverButtonText,
            onPress: () => {
              AsyncStorage.setItem(
                'isAppRated',
                JSON.stringify({counter: 0, isRated: true, message: 'never'}),
              );
            },
          },
        ],
        {cancelable: false},
      );
    }
  };
  componentDidMount() {
    this.getToken();
    this.checkLocation();
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange,
    );
    AppState.addEventListener('change', this._handleAppStateChange);

    AsyncStorage.getItem('isProfileCompleted').then(json => {
      const res = JSON.parse(json);

      if (res && !res.isCompleted) {
        let counter = res.counter;
        if (counter >= 4) {
          counter = 1;
        } else {
          counter += 1;
        }
        AsyncStorage.setItem(
          'isProfileCompleted',
          JSON.stringify({...res, counter: counter}),
        );
      }
    });

    AsyncStorage.getItem('isAppRated').then(json => {
      const res = JSON.parse(json);
      if (!res) {
        AsyncStorage.setItem(
          'isAppRated',
          JSON.stringify({counter: 1, isRated: false, message: ''}),
        );
      }

      if (res && !res.isRated) {
        let counter = res.counter;

        if (counter >= 4) {
          counter = 1;
        } else {
          counter += 1;
        }
        this.setState({isAppRated: {...res, counter}});
        AsyncStorage.setItem(
          'isAppRated',
          JSON.stringify({...res, counter: counter}),
        );
      }
    });
  }

  onIds = async device => {
    const playId = device.userId;
    const newPlayId = playId.toString();
    await AsyncStorage.setItem('playerId', newPlayId);
    this.setState({playerId: playId});
  };

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({isConnected: true});
    } else {
      this.setState({isConnected: false});
      // Alert.alert('No Internet Connection', '');
    }
  };

  sendLatLng(lat, lng) {
    const {token} = this.state;

    if (!token) {
      return;
    }

    const url = API_URL + '/update-lat-lng';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        lat: lat,
        lng: lng,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('response-->',responseData);
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  sendLatLngAlongWithPlayerId(lat, lng) {
    // console.warn('playerId-->>', );
    const playerId = this.state.playerId;
    if (!playerId || !lat || !lng) {
      return;
    }

    fetch(API_URL + '/save-player-id', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        lat: lat,
        lng: lng,
        player_id: this.state.playerId,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('response-->>', responseData)
        // if(responseData.status == 200){
        // }else if(responseData.status == 654864){
        // 	Alert.alert('Error in')
        // }
      })
      .catch(error => {
        Alert.alert('Error', error.message);
      });
  }

  getlocation() {
    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        this.sendLatLng(latitude, longitude);
        this.sendLatLngAlongWithPlayerId(latitude, longitude);
        AsyncStorage.setItem('region', JSON.stringify(position)).then(() => {
          console.warn('inside setItem');
          this.hideLocationBoxHandler();
        });
      },
      error => {
        // Alert.alert('Error',error.message);
        this.hideLocationBoxHandler();
      },
      {enableHighAccuracy: false, timeout: 5000, maximumAge: 1000},
    );
  }
  enableUserLocation = async () => {
    if (Platform.OS === 'android') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then(async data => {
          if (data == 'enabled') {
            await this.getlocation();
          }
        })
        .catch(async err => {
          if (err.code == 'ERR00') {
            await this.hideLocationBoxHandler();
          }
        });
    }
  };
  // enableUserLocation() {
  //   console.warn('zatin');
  //   LocationSwitch.enableLocationService(
  //     1000,
  //     true,
  //     () => {
  //       console.warn('aman');
  //       this.getlocation();
  //     },
  //     () => {
  //       console.warn('aman cc');
  //       this.hideLocationBoxHandler();
  //       const error = this.props.isRTL ? '' : 'Error';
  //       const msg = this.props.isRTL ? '' : 'Location could not enable.';

  //       Alert.alert(error, msg);
  //     },
  //   );
  // }

  checkLocation() {
    LocationSwitch.isLocationEnabled(
      () => {
        this.getlocation();
      },
      () => {
        const msg1 = this.props.isRTL ? 'ستخدام الموقع' : 'Use Location ?';
        const msg2 = this.props.isRTL
          ? 'ظهار العروض الخاصة ونقاط البيع حول موقعك، يرجى تشغيل محدد المواقع GPS'
          : 'To show offers and sales point around you, turn on GPS.';
        const not = this.props.isRTL ? 'يس الآن' : 'Not Now';
        const enable = this.props.isRTL ? 'تفعيل' : 'Enable';

        Alert.alert(
          msg1,
          msg2,
          [
            {
              text: not,
              onPress: () => {
                this.hideLocationBoxHandler();
              },
            },
            {
              text: enable,
              onPress: () => {
                this.enableUserLocation();
              },
            },
          ],
          {cancelable: false},
        );
      },
    );
  }

  async getToken() {
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    if (userData) {
      const token = userData.sData;
      this.setState({token: token});
    }
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('ids', this.onIds);
    OneSignal.removeEventListener('opened', this.onOpened);
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleConnectivityChange,
    );
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      this.setState({startTime: new Date().getTime()});
    } else {
      if (this.state.startTime !== null) {
        if (
          Math.abs(new Date().getTime() - this.state.startTime) / 1000 / 60 <
          1
        ) {
          database()
            .ref('TIMES/TIME/one/Android/' + new Date().toDateString())
            .transaction(total => {
              return (total || 0) + 1;
            });
          database()
            .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/one/')
            .transaction(total => {
              return (total || 0) + 1;
            });
        } else if (
          Math.abs(new Date().getTime() - this.state.startTime) / 1000 / 60 <
          3
        ) {
          database()
            .ref('TIMES/TIME/1to3/Android/' + new Date().toDateString())
            .transaction(total => {
              return (total || 0) + 1;
            });
          database()
            .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/1to3')
            .transaction(total => {
              return (total || 0) + 1;
            });
        } else if (
          Math.abs(new Date().getTime() - this.state.startTime) / 1000 / 60 <
          5
        ) {
          database()
            .ref('TIMES/TIME/3to5/Android/' + new Date().toDateString())
            .transaction(total => {
              return (total || 0) + 1;
            });
          database()
            .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/3to5')
            .transaction(total => {
              return (total || 0) + 1;
            });
        } else if (
          Math.abs(new Date().getTime() - this.state.startTime) / 1000 / 60 <
          10
        ) {
          database()
            .ref('TIMES/TIME/5to10/Android/' + new Date().toDateString())
            .transaction(total => {
              return (total || 0) + 1;
            });
          database()
            .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/5to10')
            .transaction(total => {
              return (total || 0) + 1;
            });
        } else if (
          Math.abs(new Date().getTime() - this.state.startTime) / 1000 / 60 >
          10
        ) {
          database()
            .ref('TIMES/TIME/ten/Android/' + new Date().toDateString())
            .transaction(total => {
              return (total || 0) + 1;
            });
          database()
            .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/ten')
            .transaction(total => {
              return (total || 0) + 1;
            });
        }
        database()
          .ref('T_TIMES/T_TIME/' + new Date().toDateString() + '/total')
          .transaction(total => {
            return (total || 0) + 1;
          });
      }
    }

    /// difference between both the times is the time spent by user on app
    this.setState({appState: nextAppState});
  };

  render() {
    return this.state.hideLocationBox ? <Routes /> : <GettingLocation />;
  }
}

function GettingLocation(props) {
  return (
    <ImageBackground
      source={require('./app/images/simsim.png')}
      style={{width: '100%', height: '100%'}}>
      <View style={styles.loading}>
        <ActivityIndicator size="large" color="#40b2ef" />
      </View>
    </ImageBackground>
  );
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

// function mapDispatchToProps(dispatch) {
//   return bindActionCreators(layoutActions, dispatch);
// }

const NewApp = connect(
  mapStateToProps,
  null,
)(App);

const RootApp = () => (
  <Provider store={store}>
    <NewApp />
  </Provider>
);

export default RootApp;

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
