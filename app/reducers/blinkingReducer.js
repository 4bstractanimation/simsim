const initialState = {
  blinkingImage: true,
};

export default function blinkingReducer(state = initialState, action) {
  switch (action.type) {
    case 'DELETE_BLINKING':
      return {
        ...state,
        blinkingImage: action.payload,
      };
    default:
      return state;
  }
}
