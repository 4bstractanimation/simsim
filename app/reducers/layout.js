import {
  SET_LAYOUT,
  GET_LAYOUT,
  SET_COUNTRY_DETAIL,
  OTP_VERIFY,
  SET_SELLER_TYPE,
} from '../actions/layout'
import data from '../components/data';

const initialState = {
  isRTL: false,
  countryLocation: {countryName: 'Saudi Arabia', countryCode: 'SA', currencyCode: 'SAR'},
  mobile: "",
  cCode: "",
  sellerType: '',
}

export default function reducer(state = initialState, action) {

  switch (action.type) {
    case SET_LAYOUT:
      return {
        ...state,
        isRTL: true,
      }
      case OTP_VERIFY : {
        return {
          ...state,
          mobile:action.data.mobile,
          cCode:action.data.cCode
        }
      }
        

    case GET_LAYOUT:
      return {
        ...state,
        isRTL: false,
      }
    case SET_COUNTRY_DETAIL :
    return {
        ...state,
        countryLocation: action.countryDetail,
      }
      case SET_SELLER_TYPE : 
                            return{
                                ...state, sellerType: action.sellerType,
                            }
    
  default:
    return state
  }
}
