import {NOTIFICATION_COUNT, RESET_NOTIFICATION, NOTIFICATION} from '../actions/notificationAction';

const initialState = {
	notificationCount: 0,
	check: false
}

export default function notificationReducer(state = initialState, action) {
	switch (action.type) {
		case "NOTIFICATION_COUNT":
			return {
			...state,
			notificationCount: action.count
		};
		case "RESET_NOTIFICATION":
			return {
				...state,check: true
			}
		case "NOTIFICATION": 
			return {
				...state, notificationCount: action.notificationCount,
			check: false
			}
		default:
			return state
	}
}