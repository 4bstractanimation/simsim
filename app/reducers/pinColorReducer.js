import {SELECTED_MARKER} from '../actions/pinColor';

const initialState = {
	selectedMarkerIndex:null,
	selectedMarkerIndices:[]
}

export default function pinColorReducer(state = initialState, action) {
	switch (action.type) {
		case "SELECTED_MARKER":
			return {
			...state,
			selectedMarkerIndex: action.payload,
			selectedMarkerIndices: [...state.selectedMarkerIndices, action.payload]
		};
		case "MARKER":
			return {
 			...state
 		};
	}
	return state;
}