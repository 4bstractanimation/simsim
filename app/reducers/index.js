import {combineReducers} from 'redux';
import network from './layout';
import search from './SearchReducer';
import pin from './pinColorReducer';
import notification from './notificationReducer';
import pinReducer from './pinReducer';
import loginReducer from './loginReducer';
import blinkingReducer from './blinkingReducer';

export default combineReducers({
  network,
  search,
  pin,
  notification,
  pinReducer,
  loginReducer,
  blinkingReducer,
});
