import { SET_PIN_DATA } from '../actions/pinAction';

const initialState = {
  isRTL: false,
  data: {}
}

export default function pinReducer(state = initialState, action) {

  switch (action.type) {
    case SET_PIN_DATA:
      return {
        ...state,
       data:action.data
      }
  default:
    return state
  }
}