import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';

const {width, height } = Dimensions.get('window') ;

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Comments';

class Comments extends Component {
	constructor(props) {
		super(props);
		this.state = {loadmore: true};
	}
	
	render() {
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		const commentlist = this.props.commentlist;
		const commnetListLength = commentlist.comments.length;
		const comments = commentlist.comments.map((comment, index) => {
			return (
					<View style={styles.container} key={index}>
						<View key={index} style={styles.comment}>
							<View style={styles.commentContent}>
								<View style={styles.userName}>
									<Text style={styles.userNameText}>{comment.userName}</Text>
								</View>
								<View style={styles.commentTextBox}>
									<Text style={styles.commentText}>
										{comment.userComment}
		          					</Text>
								</View>
								<Text>
									{comment.createdAt}
								</Text>
							</View>	
						</View>						
							{ commnetListLength == index+1 ? null : <View style={styles.seprator}></View>	}					
						
					</View>
				);
		});
		return (
			<View style={{flex:1}}>			
				 {comments}
				 <TouchableOpacity style={styles.viewMore} >
					<Text style={styles.viewMoreText}>View More</Text>
				</TouchableOpacity>
			</View>	
		);
	}
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Comments)