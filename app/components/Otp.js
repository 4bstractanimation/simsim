import React, {Component} from 'react';
// import Api from './../api/index';

import {
  // AppRegistry,
  // StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  // Platform,
  AsyncStorage,
  Alert,
  Keyboard,
  ActivityIndicator,
} from 'react-native';
//import SmsListener from 'react-native-android-sms-listener'
// import SignUp from './SignUp';
// import Header from './Header';
// import Home from './maps/HomeMap';
// import ResetPassword from './ResetPassword';
import OneSignal from 'react-native-onesignal';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Otp';
import DeviceInfo from 'react-native-device-info';
import API_URL from '../configuration/configuration';

class Otp extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      return {
        title:
          state.params.isRTL === true ? 'تحقق من رقمك' : 'Verify your Number',
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      otpval: '',
      isLoading: false,
      playerId: '',
    };
    this.mobile = '';
  }

  componentDidMount() {
    const {setParams} = this.props.navigation;
    OneSignal.configure();
    OneSignal.init('11a642c1-9671-4e10-ac73-ca5fba2c0cdf');
    OneSignal.addEventListener('ids', this.onIds);
    setParams({isRTL: this.props.isRTL});
  }

  onIds = device => {
    this.setState({playerId: device.userId});
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  loginUser() {
    const {navigate} = this.props.navigation;
    fetch(API_URL + '/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        countryCode: this.props.navigation.state.params.signup.code,
        userId: this.props.navigation.state.params.signup.mobile,
        password: this.props.navigation.state.params.signup.pass,
        deviceId: this.state.playerId,
        playerId: this.state.playerId,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        this.setState({isLoading: false});
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 236486
        ) {
          const errorText = isRTL ? 'خطأ' : 'Error';
          const message = isRTL ? 'فشل في تسجيل الدخول ' : 'Failed to Login';
          Alert.alert(errorText);
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 646483
        ) {
          this.setState({invalid: true, isLoading: false});
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          const token = responseData.sData;
          // this.getUserDetails(token);
          // let userCredential = null;
          // if (this.state.checked) {
          // 	const mobile = this.state.mobile;
          // 	const password = this.state.password;
          // 	const countryCode = this.state.countryCode;
          // 	userCredential = { mobile, password, countryCode };
          // }
          // await AsyncStorage.setItem('user_data', JSON.stringify(responseData));
          AsyncStorage.multiSet([
            ['user_data', JSON.stringify(responseData)],
            // ['userCredential', JSON.stringify(userCredential)]
          ]);
          // this.setState({ isLogin: true, userToken:token });
          // this.props.setLoginData(responseData);
          navigate('Home');
          // this.props.navigation.goBack()
          // this.notification();
        }
      })
      .catch(err => {
        console.warn(err);
        this.setState({isLoading: false});
      });
  }

  async onSubmitOtp() {
    Keyboard.dismiss();
    this.setState({isLoading: true});
    const {navigate} = this.props.navigation;
    const mobile = this.props.navigation.state.params.mobile;
    const mobileNo = this.props.navigation.state.params.mobileNo;
    const userName = this.props.navigation.state.params.name;
    const cCode = this.props.navigation.state.params.cCode;
    const getUserData = await AsyncStorage.getItem('user_data');
    const deviceId = await DeviceInfo.getDeviceId();
    //console.warn('onSubmitOtp-->>',this.props.navigation.state.params);
    //retrun;
    //For SignUp OTP
    if (this.props.navigation.state.params.signup) {
      if (this.state.otpval !== '') {
        fetch(API_URL + '/signup', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            otpvl: this.state.otpval,
            username: this.props.navigation.state.params.signup.name,
            pswd: this.props.navigation.state.params.signup.pass,
            countryCode: this.props.navigation.state.params.signup.code,
            mobile: this.props.navigation.state.params.signup.mobile,
            deviceId: deviceId ? deviceId : 1234,
            regType: 4864,
          }),
        })
          .then(response => response.json())
          .then(responseData => {
            this.setState({isLoading: false});
            if (
              responseData.status == 502368 &&
              responseData.eData.eCode == 684605
            ) {
              // alert('validation error');
            } else if (
              responseData.status == 502368 &&
              responseData.eData.eCode == 641292
            ) {
              alert('condition error');
            } else if (
              responseData.status == 502368 &&
              responseData.eData.eCode == 681245
            ) {
              alert('OTP not matched');
              // this.setState({isLoading: false});
            } else if (
              responseData.status == 805465 &&
              responseData.eData === null
            ) {
              this.loginUser();
              Alert.alert('', 'SignUp successful');
              // this.setState({isLoading: false});
              // navigate('Home');
            }
          })
          .catch(err => {
            Alert.alert('Error:', err.message);
            this.setState({isLoading: false});
          });
      } else {
        alert('Please enter the OTP');
        this.setState({isLoading: false});
      }
    } else if (this.props.navigation.state.params.mobileNo) {
      //For Verification of forgot password

      if (this.state.otpval !== '') {
        fetch(API_URL + '/verify-forgot-password-sms', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            otpvl: this.state.otpval,
            countryCode: this.props.navigation.state.params.cCode,
            mobile: mobileNo,
          }),
        })
          .then(response => response.json())
          .then(responseData => {
            // console.warn(responseData);
            this.setState({isLoading: false});
            if (
              responseData.status == 502368 &&
              responseData.eData.eCode == 684605
            ) {
              alert('validation error');
            } else if (
              responseData.status == 502368 &&
              responseData.eData.eCode == 648664
            ) {
              alert('Data not found');
              // this.setState({isLoading: false});
            } else if (
              responseData.status == 805465 &&
              responseData.eData === null
            ) {
              Alert.alert('Thank You', 'Your OTP has been verified', [
                {
                  text: 'Ok',
                  onPress: () => {
                    // this.setState({isLoading: false});
                    navigate('ResetPassword', {
                      countryCode: this.props.navigation.state.params.cCode,
                      otp: this.state.otpval,
                      mob: this.props.navigation.state.params.mobileNo,
                    });
                  },
                },
              ]);
            }
          })
          .catch(err => {
            console.log(err);
            this.setState({isLoading: false});
          });
      } else {
        alert('Please enter the correct Otp');
        this.setState({isLoading: false});
      }
    } else if (
      getUserData &&
      this.props.navigation.state.params.mobile &&
      this.props.navigation.state.params.type === 'profile'
    ) {
      //For Add Pin Verification
      const userData = JSON.parse(getUserData);
      const token = userData.sData;

      const data = this.props.navigation.state.params.data;
      if (this.state.otpval !== '') {
        fetch(API_URL + '/otpverifypin', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: token,
          },
          body: JSON.stringify({
            otpvl: this.state.otpval,
            mobile: mobile,
          }),
        })
          .then(response => response.json())
          .then(responseData => {
            //console.warn('otpverifypin-->>',responseData);
            this.setState({isLoading: false});
            if (
              responseData.status == 805465 &&
              responseData.eData &&
              responseData.eData.eCode == 545864
            ) {
              alert('validation error');
            } else if (
              responseData.status == 805465 &&
              responseData.eData === null &&
              responseData.sData === null
            ) {
              alert('Authenticated successfully.');

              //Profile updated here
              fetch(API_URL + '/profile-edit', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  Authorization: token,
                },
                body: JSON.stringify(data),
              })
                .then(response => response.json())
                .then(responseData => {
                  this.setState({isLoading: false});
                  //console.warn('profile-edit-->>>',responseData)
                  if (
                    responseData.status == 654864 &&
                    responseData.eData.eCode == 236486
                  ) {
                    const errors = responseData.eData.errors;
                    //console.warn(errors)
                    Alert.alert(errors[0]);
                  } else if (
                    responseData.status == 654864 &&
                    responseData.eData.eCode == 545864
                  ) {
                    alert('Failed to update');
                    // this.setState({isLoading: false});
                  } else if (
                    responseData.status == 516324 &&
                    responseData.eData === null
                  ) {
                    // console.warn('-->>',responseData.userData)
                    const {gender, dob, name, mobile} = responseData.userData;
                    //console.warn(responseData.userData)
                    if (gender && dob && name && mobile) {
                      AsyncStorage.setItem(
                        'isProfileCompleted',
                        JSON.stringify({
                          isCompleted: true,
                          counter: 0,
                        }),
                      );
                    }
                    Alert.alert('', 'Profile updated successfully');
                    // this.setState({isLoading: false});
                    this.props.navigation.goBack();
                  }
                })
                .catch(err => {
                  console.warn(err);
                });
            }
          })
          .catch(err => {
            console.warn(err);
            this.setState({isLoading: false});
          });
      } else {
        alert('Please enter the correct Otp');
        this.setState({isLoading: false});
      }
    } else if (
      getUserData &&
      this.props.navigation.state.params.mobile &&
      this.props.navigation.state.params.type === 'addPin'
    ) {
      //  console.warn('TCL: onSubmitOtp -> ', this.props.navigation.state.params.type);
      //For Add Pin Verification
      const userData = JSON.parse(getUserData);
      const token = userData.sData;

      // const data = this.props.navigation.state.params.data;
      if (this.state.otpval !== '') {
        fetch(API_URL + '/otpverifypin', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: token,
          },
          body: JSON.stringify({
            otpvl: this.state.otpval,
            mobile: mobile,
          }),
        })
          .then(response => response.json())
          .then(responseData => {
            // console.warn('otpverifypin-->>', responseData);
            this.setState({isLoading: false});
            if (
              responseData.status == 805465 &&
              responseData.eData &&
              responseData.eData.eCode == 545864
            ) {
              alert('validation error');
              // this.setState({isLoading: false});
            } else if (
              responseData.status == 805465 &&
              responseData.eData === null &&
              responseData.sData === null
            ) {
              // this.props.dispatchForOTP(layoutActions.checkOtpVerify(mobile,cCode));
              this.props.checkOtpVerify(mobile, cCode);
              this.props.navigation.goBack();
              alert('Authenticated successfully.');
              // this.setState({isLoading: false});
            }
          })
          .catch(err => {
            console.warn(err.message);
            this.setState({isLoading: false});
          });
      } else {
        alert('Please enter the correct Otp');
        this.setState({isLoading: false});
      }
    }
  }

  render() {
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);

    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={{color: '#7A7A7A', fontSize: 17, fontWeight: 'bold'}}>
            {strings('otp.sent')}
          </Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={{color: '#7A7A7A', fontSize: 17, fontWeight: 'bold'}}>
              {' '}
              {this.props.navigation.state.params.cCode ||
                this.props.navigation.state.params.code ||
                this.props.navigation.state.params.signup.code}{' '}
              {this.props.navigation.state.params.mobileNo ||
                this.props.navigation.state.params.mobile ||
                this.props.navigation.state.params.signup.mobile}
              , <Text style={{color: '#00ADDE'}}>{strings('otp.number')}</Text>
            </Text>
          </TouchableOpacity>

          <View style={styles._textbox}>
            <TextInput
              style={styles._textboxText}
              underlineColorAndroid="white"
              returnKeyType="next"
              value={this.state.otpval}
              onChangeText={text => this.setState({otpval: text})}
              keyboardType={'numeric'}
              placeholderTextColor="#7A7A7A"
              placeholder=" -  -  -  -  - "
            />
          </View>
          {this.state.isLoading ? (
            <ActivityIndicator size="small" color="#40b2ef" />
          ) : (
            <TouchableOpacity
              style={[
                styles._button,
                {
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}
              onPress={this.onSubmitOtp.bind(this)}>
              <Text style={styles._buttonText}>
                {strings('adNewAd.submit')}
              </Text>
              {/* {this.state.isLoading && (
              <ActivityIndicator size="small" color="#000000" />
            )} */}
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

// function mapDispatchToProps(dispatch) {
//   return bindActionCreators(layoutActions, dispatch);
// }

export default connect(
  mapStateToProps,
  {
    ...layoutActions,
  },
)(Otp);
