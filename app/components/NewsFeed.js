import React, {Component} from 'react';
import {
  View,
  Alert,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import SellerAdsTab from './SellerAdsTab';
import moment from 'moment';
// import Video from 'react-native-video';
// import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import VideoFile from './VideoFile';
import Toast from 'react-native-simple-toast';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/NewsFeed';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import Share from 'react-native-share';
import API_URL, {WEB_URL} from '../configuration/configuration';

class NewsFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newslist: [],
      userId: '',
      isLoading: true,
      token: '',
    };
    // this.getNewsFeeds = this.getNewsFeeds.bind(this);
  }

  async getNewsFeeds(userId, pinId) {
    const {isRTL} = this.props;
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    if (userData) {
      const token = userData.sData;
      const userId = userData.userId;
      this.setState({userId: userId, token});
    }
    fetch(API_URL + '/news-feed', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userId: userId,
        pinId: pinId,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.log('news-->>', responseData)
        if (responseData.status == 654864) {
          this.setState({isLoading: false, newslist: []});
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          this.setState({
            newslist: responseData.newsFeed.reverse(),
            isLoading: false,
          });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }
  componentDidMount() {
    // console.warn(this.props.navigation.state.params.item);
    const userId = this.props.navigation.state.params.item.user_id;
    const pinId = this.props.navigation.state.params.item.id;
    //const { navigate } = this.props.navigation;

    this.getNewsFeeds(userId, pinId);
    this.props.navigation.addListener('didFocus', async event => {
      this.getNewsFeeds(userId, pinId);
    });
  }

  deleteNews(id) {
    const {isRTL} = this.props;
    const message = isRTL
      ? 'هل تريد مسح هذه الاخبار؟ '
      : 'Do you want to delete this news?';
    const yesText = this.props.isRTL ? 'نعم' : 'Yes';
    const noText = this.props.isRTL ? 'لا' : 'No';
    Alert.alert(
      '',
      message,
      [
        {text: noText, style: 'cancel'},
        {text: yesText, onPress: () => this.deleteNewsData(id)},
      ],
      {cancelable: false},
    );
  }

  async deleteNewsData(id) {
    const userId = this.props.navigation.state.params.item.user_id;
    const pinId = this.props.navigation.state.params.item.id;
    const {isRTL} = this.props;
    // console.log('userId  ', userId, pinId)
    fetch(API_URL + '/deleteNews', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        newsId: id,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          alert('News data not found in DB!');
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          // Alert.alert("",'News deleted successfully!');
          const message = isRTL
            ? 'تم مسح الاخبار بنجاح'
            : 'News deleted successfully!';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          this.getNewsFeeds(userId, pinId);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  shareNews = news => {
    const {title, description} = news;
    const {id, pin_name, user_id} = this.props.navigation.state.params.item;
    const url =
      WEB_URL +
      '/pin/' +
      pin_name.replace(/ +/g, '_') +
      '/' +
      user_id +
      '/' +
      id;
    const titleDesc = title + ':- ' + description;
    const shareOptions = {
      title: 'SimSim Markets',
      message: titleDesc,
      url: url,
    };
    Share.open(shareOptions, function(e) {
      console.warn(e);
    });
  };

  render() {
    // console.warn(this.state.newslist)
    const {navigate} = this.props.navigation;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.isLoading ? (
            <PreLoader />
          ) : !this.state.newslist.length ? (
            <NoDataFound />
          ) : (
            this.state.newslist.map((news, index) => {
              //Check image or video
              let uri = news.news_document;
              let type;
              const dateTime = news.created_at;
              var date = moment(dateTime).format('DD MMM');
              if (uri) {
                const extension = uri
                  .split('.')
                  .pop()
                  .toLowerCase();

                type =
                  extension == 'png' || 'jpeg' || 'svg' ? 'image' : 'video';
              }
              return (
                <View style={styles.newsContent} key={index}>
                  <View style={styles.userDetail}>
                    <View style={styles.userImage}>
                      <View style={styles.userNameBox}>
                        <Text style={styles.userNameText}>{news.title}</Text>
                      </View>
                    </View>
                    {news.user_id == this.state.userId ? (
                      <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity onPress={() => this.shareNews(news)}>
                          <View
                            style={{
                              borderWidth: 1,
                              marginRight: 5,
                              borderRadius: 25,
                            }}>
                            <Text style={{fontSize: 12, padding: 3}}>
                              {this.props.isRTL ? 'نشر' : 'Publish'}
                            </Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => {
                            navigate('AddNewNews', {
                              news,
                              getNewsFeeds: this.getNewsFeeds,
                            });
                          }}>
                          <Image
                            style={{width: 25, height: 25}}
                            source={require('./../icons/edit2x.png')}
                          />
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={this.deleteNews.bind(this, news.id)}>
                          <Image
                            style={{width: 25, height: 25, marginHorizontal: 5}}
                            source={require('./../icons/delete2x.png')}
                          />
                        </TouchableOpacity>
                      </View>
                    ) : null}

                    <View style={{}}>{<Text>{date}</Text>}</View>
                  </View>

                  <View style={styles.mediaBox}>
                    {type === 'image' ? (
                      <Image
                        style={styles.media}
                        source={{uri: news.news_document}}
                      />
                    ) : type === 'video' ? (
                      <View style={{}}>
                        <VideoFile uri={uri} />
                      </View>
                    ) : null}
                  </View>
                  <View style={styles.newsTextBox}>
                    <Text style={styles.newsText}>{news.description}</Text>
                  </View>
                </View>
              );
            })
          )}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsFeed);
