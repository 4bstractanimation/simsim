import React, {PureComponent} from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
  AsyncStorage,
  Platform,
  Linking,
  Share,
  TimePickerAndroid,
  KeyboardAvoidingView,
  Keyboard,
  Picker,
  DatePickerIOS,
} from 'react-native';
import Switch from 'react-native-customisable-switch';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import Policy from './Policy';
import Header from './Header';
import CheckBox from 'react-native-check-box';
import ImagePicker from 'react-native-image-crop-picker';
import SocialSharing from './SocialSharing';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import ModalDropdown from 'react-native-modal-dropdown';
import MapView, {Marker} from 'react-native-maps';
import GetUserlocation from './GetUserlocation';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AddPin';
import NotificationHeader from './NotificationHeader';
import PreLoader from './PreLoader';
import ImageResizer from 'react-native-image-resizer';
import RNFetchBlob from 'rn-fetch-blob';
import RNPickerSelect from 'react-native-picker-select';
import countriesCode from './CountriesCode';
import Modal from 'react-native-modalbox';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';
import LocationSwitch from 'react-native-location-switch';

class AddPin extends PureComponent {
  static navigationOptions = ({navigation}) => {
    let state = navigation.state;
    if (state.params && state.params.data) {
      return {
        title:
          state.params.isRTL === true
            ? 'تحرير نقطة المبيعات'
            : 'Edit Sales Point',
        headerRight: <NotificationHeader navigation={navigation} />,
      };
    } else if (state.params) {
      return {
        title:
          state.params.isRTL === true ? 'أضف نقطة بيع' : 'Add Sales Point',
        headerRight: <NotificationHeader navigation={navigation} />,
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      token: '',
      isLoading: false,
      pinId: null,
      pinNo: '',
      enterpriseName: '',
      pinImg: 'xyz.png',
      editImg: false,
      pinFile: '',
      errorZero: false,
      pinName: '',
      nameOnMap: '',
      sellerType: '',
      sellerIndex: null,
      fromDay: '',
      toDay: '',
      fromTime: '',
      toTime: '',
      fromTimeIos: new Date(),
      toTimeIos: new Date(),
      contactNo: '',
      lat: '',
      long: '',
      location: '',
      delivery_note: '',
      delivery_status: 'No',
      building: '',
      floor: null,
      officeNo: null,
      description: '',
      checked: false,
      fileName: '',
      isTimePickerVisible: false,
      isToTimePickerVisible: false,
      isWeenEndTimeFromPickerVisible: false,
      isWeenEndTimeToPickerVisible: false,
      weekEndTimeFrom: 'Close',
      weekEndTimeTo: 'Close',
      weekEndTimeFromIos: new Date(),
      weekEndTimeToIos: new Date(),
      weekendDayFrom: '',
      weekEndDayTo: '',
      showIosStartTime: false,
      showIosEndTime: false,
      showIosWeekndStartTime: false,
      showIosWeekndEndTime: false,
      enterprise_document: '',
      type: '',
      pinsType: '',
      editData: '',
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      headerButton: '',
      switchValue: true,
      weekendDays: [],
      checkedWeekend: false,
      contactVisibility: 'Yes',
      pinNumber: '',
      delivery_index: 1,
      countryCode: '+966',
      oldContact: '',
      isOpenInWeekend: 'Close',
      isOpenInWeekendIndex: 1,
    };
    this.onSelect = this.onSelect.bind(this);
    this.type = '';
    this.openMap = this.openMap.bind(this);
    this.mobile = '';
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  _showDateTimePicker = () => {
    if (Platform.OS === 'ios') {
      this.refs.modal1.open();
    } else {
      this.setState({isTimePickerVisible: true});
    }
  };

  _hideDateTimePicker = () => {
    if (Platform.OS === 'ios') {
      this.setState({showIosStartTime: true});
      this.refs.modal1.close();
    } else {
      this.setState({isTimePickerVisible: false});
    }
  };

  cancelFromTimePicker = () => {
    this.setState({showIosStartTime: false});
    this.refs.modal1.close();
  };

  cancelToTimePicker = () => {
    this.setState({showIosEndTime: false});
    this.refs.modal2.close();
  };

  cancelWeekndFromTimePicker = () => {
    this.setState({showIosWeekndStartTime: false});
    this.refs.modal3.close();
  };

  cancelWeekndToTimePicker = () => {
    this.setState({showIosWeekndEndTime: false});
    this.refs.modal4.close();
  };

  _handlefromTimePicked = time => {
    //console.warn("TCL: time", time)
    const fromTime = moment(time).format('hh:mm:ss A');
    Platform.OS === 'ios'
      ? this.setState({fromTimeIos: time})
      : this.setState({fromTime: fromTime});
    Platform.OS === 'android' ? this._hideDateTimePicker() : null;
  };

  onFocus(status) {
    this.setState({headerButton: status});
  }

  _showTimePicker = () =>
    Platform.OS === 'ios'
      ? this.refs.modal2.open()
      : this.setState({isToTimePickerVisible: true});

  //_hideToDateTimePicker = () => Platform.OS === 'ios' ? this.refs.modal2.close() :	this.setState({ isToTimePickerVisible: false });

  _hideToDateTimePicker = () => {
    if (Platform.OS === 'ios') {
      this.setState({showIosEndTime: true});
      this.refs.modal2.close();
    } else {
      this.setState({isToTimePickerVisible: false});
    }
  };

  _handletoTimePicked = time => {
    const toTime = moment(time).format('hh:mm:ss A');
    Platform.OS === 'ios'
      ? this.setState({toTimeIos: time})
      : this.setState({toTime: toTime});
    Platform.OS === 'android' ? this._hideToDateTimePicker() : null;
  };

  _handleWeekEndFromTimePicked = time => {
    const fromTime = moment(time).format('hh:mm:ss A');
    Platform.OS === 'ios'
      ? this.setState({weekEndTimeFromIos: time})
      : this.setState({
          weekEndTimeFrom: fromTime,
          isWeenEndTimeFromPickerVisible: false,
        });
    //	this.setState({ weekEndTimeFrom: fromTime, isWeenEndTimeFromPickerVisible: false });
    // this._hideWeekEndTimeFromPicker();
  };

  _hideWeekEndTimeFromPicker = () => {
    if (Platform.OS === 'ios') {
      this.setState({showIosWeekndStartTime: true});
      this.refs.modal3.close();
    } else {
      this.setState({
        isWeenEndTimeFromPickerVisible: false,
        weekEndTimeFrom: 'Close',
      });
    }
  };

  _handleWeekEndToTimePicked = time => {
    const fromTime = moment(time).format('hh:mm:ss A');
    Platform.OS === 'ios'
      ? this.setState({weekEndTimeToIos: time})
      : this.setState({
          weekEndTimeTo: fromTime,
          isWeenEndTimeToPickerVisible: false,
        });
    //	this.setState({ weekEndTimeTo: fromTime, isWeenEndTimeToPickerVisible: false });
    // this._hideWeekEndTimeToPicker();
  };

  _hideWeekEndTimeToPicker = () => {
    if (Platform.OS === 'ios') {
      this.setState({showIosWeekndEndTime: true});
      this.refs.modal4.close();
    } else {
      this.setState({
        isWeenEndTimeToPickerVisible: false,
        weekEndTimeTo: 'Close',
      });
    }
  };
  uploadImage() {
    ImagePicker.openPicker({
      includeBase64: true,
    })
      .then(item => {
        const str = item.path;
        var filename = str.replace(/^.*[\\\/]/, '');
        this.setState({
          enterprise_document: item.data,
          fileName: filename,
        });
      })
      .catch(e => alert(e));
  }

  async PinImage() {
    ImagePicker.openPicker({
      includeBase64: true,
      compressImageQuality: 0.2,
    })
      .then(item => {
        const str = item.path;
        var filename = str.replace(/^.*[\\\/]/, '');
        let img = item.data;
        this.setState({
          pinImg: img,
          pinFile: filename,
          editImg: true,
        });
      })
      .catch(e => alert(e));
  }

  //  async convertIntoBase64(fileUri) {
  // 	return RNFetchBlob.fs.readFile(fileUri, 'base64')
  // 		.then( async (data) => {
  // 			return data;

  // 		})
  // }

  getAllPin(token) {
    fetch(API_URL + '/usersAllPin', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        if (
          responseData.status == 654864 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          const pinType = responseData.allPins[0].pin_type;
          const sellerIndex =
            pinType === 'enterprise' || pinType === 'Enterprise' ? 1 : 0;
          this.setState({
            pinsType: pinType,
            sellerType: pinType,
            sellerIndex,
            enterprise_document: responseData.allPins[0].enterprise_document,
          });
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  getPinNumber(token) {
    const {isRTL} = this.props;
    fetch(API_URL + '/auto-generate-pinnumber', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('pinNumber ',responseData)
        if (responseData.status == 516324 && responseData.eData === null) {
          this.setState({pinNumber: responseData.autoGenerate});
          //setTimeout(() => console.warn(this.state.adNo), 200);
        } else {
          const message = isRTL
            ? 'لم يتم اصدار رقم لنقطة البيع، يرجى محاولة انشاء نقطة البيع مرة أخرى'
            : 'Pin numbre not generated!, please try to create sales point again';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        }
      });
  }

  async componentDidMount() {
    // lat, long and set location
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;

    let data = {};
    data = this.props.navigation.state.params;
    //console.warn('--->>', data.data)
    this.props.navigation.addListener('didFocus', async event => {
      let coordinates = {};

      AsyncStorage.getItem('coordinates').then(json => {
        coordinates = JSON.parse(json);
        // console.warn('coordinates-->',coordinates)
        if (coordinates && coordinates.latitude) {
          this.setState({
            lat: coordinates.latitude,
            long: coordinates.longitude,
          });

          fetch(
            'https://maps.googleapis.com/maps/api/geocode/json?address=' +
              coordinates.latitude +
              ',' +
              coordinates.longitude +
              '&key=AIzaSyBDZPn0qaarEZUlUJrNV9yvZn79UjPTR-Q',
          )
            .then(response => response.json())
            .then(responseJson => {
              this.setState({
                location: responseJson.results[1].formatted_address,
              });
              AsyncStorage.removeItem('coordinates');
            });
        }
      });

      AsyncStorage.getItem('userCredential').then(json => {
        const userData = JSON.parse(json);
        this.setState({
          contactNo: this.props.mobile
            ? this.props.mobile
            : data && data.data
            ? data.data.contact_no
            : userData.mobile,
          countryCode: this.props.cCode
            ? this.props.cCode
            : data && data.data
            ? data.data.countryCode
            : userData.countryCode,
          oldContact:
            data && data.data ? data.data.contact_no : userData.mobile,
        });
      });
    });

    // check the lanuage settings
    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});

    // Edit Pin details Autofilled

    // console.warn("TCL: userData.countryCode", userData.countryCode)
    // console.warn("TCL: componentDidMount -> countryCode", this.state.countryCode)

    const pinImageFileName =
      data && data.data.pin_image
        ? data.data.pin_image.replace(/^.*[\\\/]/, '')
        : null;
    const enterpriseFileName =
      data && data.data.enterprise_document
        ? data.data.enterprise_document.replace(/^.*[\\\/]/, '')
        : null;

    if (data) {
      this.setState({
        pinId: data.data.id,
        pinNumber: data.data.pin_no,
        pinImg: data.data.pin_image,
        pinName: data.data.pin_name,
        pinFile: pinImageFileName,
        nameOnMap: data.data.name_on_map,
        sellerType: data.data.pin_type,
        pinsType: data.data.pin_type,
        sellerIndex: data.data.pin_type == 'Individual' ? 0 : 1,
        contactNo: data.data.contact_no,
        switchValue: data.data.contact_visibility == 'yes' ? true : false,
        fromTime: data.data.timing_from,
        lat: data.data.lat,
        long: data.data.lng,
        location: data.data.location,
        toTime: data.data.timing_to,
        delivery_status: data.data.delivery_status,
        delivery_index:
          data.data.delivery_status.toLocaleLowerCase() === 'yes' ? 0 : 1,
        delivery_note: data.data.delivery_note,
        building: data.data.building,
        floor: data.data.floor,
        officeNo: data.data.office_no,
        description: data.data.description,
        checked: data.data.term_condition ? data.data.term_condition : false,
        // working_days: data.data.working_days,
        fromTime: data.data.timing_from_wd,
        toTime: data.data.timing_to_wd,
        fromTimeIos: data.data.timing_from_wd,
        toTimeIos: data.data.timing_to_wd,
        showIosStartTime: data.data.timing_from_wd ? true : false,
        showIosEndTime: data.data.timing_to_wd ? true : false,
        // fromDay: data.data.working_from_wd,
        // toDay: data.data.working_to_wd,
        weekEndTimeFrom: data.data.timing_from_wn,
        weekEndTimeTo: data.data.timing_to_wn,
        weekEndTimeFromIos: data.data.timing_from_wn,
        weekEndTimeToIos: data.data.timing_to_wn,
        showIosWeekndStartTime: data.data.timing_from_wn ? true : false,
        showIosWeekndEndTime: data.data.timing_to_wn ? true : false,
        weekendDays: data.data.working_from_wn
          ? data.data.working_from_wn.split(',')
          : [],
        // weekEndDayTo: data.data.working_to_wn,
        enterpriseName: data.data.enterprise_name,
        enterprise_document: data.data.enterprise_document,
        filename: enterpriseFileName,
        contactVisibility: data.data.contact_visibility,
        countryCode: data.data.countryCode,
      });
      this.type = data.type;
    } else {
      this.getPinNumber(token);
      this.getAllPin(token);
    }
  }

  async _handleSubmit() {
    Keyboard.dismiss();

    this.setState({isLoading: true});
    const {isRTL} = this.props;
    const {navigate} = this.props.navigation;
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    // const working_days = this.state.fromDay + '-' + this.state.toDay;
    let url = '';
    let addData = {};
    var contactVisibility = this.state.switchValue ? 'yes' : 'no';

    if (this.type === 'edit') {
      url = API_URL + '/edit-pin';

      addData = {
        pinId: this.state.pinId,
        pin_no: this.state.pinNumber,
        // enterprise_name: this.state.enterpriseName,
        pin_image: this.state.editImg ? this.state.pinImg : '',
        pinName: this.state.pinName,
        nameOnMap: this.state.nameOnMap,
        type: this.state.sellerType || this.state.pinsType,
        contactNo: this.state.contactNo,
        contactVisibility: contactVisibility,
        lat: this.state.lat,
        long: this.state.long,
        location: this.state.location,
        missingDelivery: this.state.delivery_status,
        deliveryNotes: this.state.delivery_note,
        building: this.state.building,
        floor: this.state.floor,
        officeNo: this.state.officeNo,
        description: this.state.description,
        agreement: this.state.checked,
        timing_from_wd:
          Platform.OS === 'ios'
            ? moment(this.state.fromTimeIos, 'hh:mm:ss A').format('hh:mm:ss A')
            : this.state.fromTime,
        timing_to_wd:
          Platform.OS === 'ios'
            ? moment(this.state.toTimeIos, 'hh:mm:ss A').format('hh:mm:ss A')
            : this.state.toTime,
        // working_from_wd: this.state.fromDay,
        // working_to_wd: this.state.toDay,
        timing_from_wn:
          Platform.OS === 'ios'
            ? this.state.showIosWeekndStartTime
              ? moment(this.state.weekEndTimeFromIos, 'hh:mm:ss A').format(
                  'hh:mm:ss A',
                )
              : strings('addPin.iOSWeekendTimeFrom')
            : this.state.weekEndTimeFrom,
        timing_to_wn:
          Platform.OS === 'ios'
            ? this.state.showIosWeekndEndTime
              ? moment(this.state.weekEndTimeToIos, 'hh:mm:ss A').format(
                  'hh:mm:ss A',
                )
              : strings('addPin.iOSWeekendTimeTo')
            : this.state.weekEndTimeTo,
        working_from_wn: this.state.weekendDays.toString(),
        // working_to_wn: this.state.weekEndDayTo,
        enterprise_document: this.state.enterprise_document,
        countryCode: this.state.countryCode,
      };
    } else {
      url = API_URL + '/addpin';
      // console.warn("this.state.pinNumber ", this.state.pinNumber);
      addData = {
        pinNumber: this.state.pinNumber,
        // enterprise_name: this.state.enterpriseName,
        pin_image: this.state.pinImg,
        pinName: this.state.pinName,
        nameOnMap: this.state.nameOnMap,
        type: this.state.sellerType || this.state.pinsType,
        contactNo: this.state.contactNo,
        contactVisibility: contactVisibility,
        lat: this.state.lat,
        long: this.state.long,
        location: this.state.location,
        missingDelivery: this.state.delivery_status,
        deliveryNotes: this.state.delivery_note,
        building: this.state.building,
        floor: this.state.floor,
        officeNo: this.state.officeNo,
        description: this.state.description,
        agreement: this.state.checked ? '1' : '',
        timing_from_wd:
          Platform.OS === 'ios'
            ? moment(this.state.fromTimeIos, 'hh:mm:ss A').format('hh:mm:ss A')
            : this.state.fromTime,
        timing_to_wd:
          Platform.OS === 'ios'
            ? moment(this.state.toTimeIos, 'hh:mm:ss A').format('hh:mm:ss A')
            : this.state.toTime,
        // working_from_wd: this.state.fromDay,
        // working_to_wd: this.state.toDay,
        timing_from_wn:
          Platform.OS === 'ios'
            ? moment(this.state.weekEndTimeFromIos, 'hh:mm:ss A').format(
                'hh:mm:ss A',
              )
            : this.state.weekEndTimeFrom,
        timing_to_wn:
          Platform.OS === 'ios'
            ? moment(this.state.weekEndTimeToIos, 'hh:mm:ss A').format(
                'hh:mm:ss A',
              )
            : this.state.weekEndTimeTo,
        working_from_wn: this.state.weekendDays.toString(),
        // working_to_wn: this.state.weekEndDayTo,
        enterprise_document: this.state.enterprise_document,
        countryCode: this.state.countryCode,
      };
    }

    //post the pin details
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify(addData),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('create pin-->>',responseData)
        this.setState({isLoading: false});
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 236486
        ) {
          const error = responseData.eData.errors;
          Alert.alert('Error:', error.toString());
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          Alert.alert('', 'Data not saved');
        } else if (
          responseData.status == 502368 &&
          responseData.eData.eCode == 684605
        ) {
          const error = responseData.eData.errors;
          Alert.alert('Error:', error.toString());
        } else if (
          responseData.status === 502368 &&
          responseData.eData.eCode === 564626
        ) {
          const message = isRTL
            ? 'لم يتم توثيق رقم الجوال '
            : 'Mobile number not verified';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 805465 &&
          responseData.eData === null
        ) {
          console.warn(
            'TCL: ',
            this.state.sellerType,
            '  ',
            this.state.pinsType,
          );
          if (
            this.state.sellerType === 'Enterprise' ||
            this.state.sellerType === 'enterprise' ||
            this.state.pinsType === 'Enterprise' ||
            this.state.pinsType === 'enterprise'
          ) {
            const msg = this.props.isRTL
              ? 'سيتم إضافة نقطة مبيعات المؤسسة إلى حسابك بعد موافقة الادارة'
              : 'Enterprise Sales point will be added to your account after the approval from Admin';
            Alert.alert('', msg);
            this.props.navigation.goBack();
          } else {
            this.props.navigation.replace('SellerContainer', {
              item: responseData.pindata,
            });
            const title = isRTL
              ? 'شكرا على إضافة نقطة بيع جديدة '
              : 'Thanks for adding a new Sales Point';
            const message = isRTL
              ? 'هل تود إضافة منتجات وخدمات جديدة لنقطة بيعك؟'
              : 'Would you like to add a new product or services';
            const yesText = this.props.isRTL ? 'نعم' : 'Yes';
            const noText = this.props.isRTL ? 'ليس الآن ' : 'Not Now';

            Alert.alert(title, message, [
              {
                text: yesText,
                onPress: () => {
                  navigate('AddNewAd', {
                    token,
                    pinId: responseData.pindata.id,
                    pinType: responseData.pindata.pin_type,
                  });
                },
              },
              {
                text: noText,
                onPress: () => {
                  // navigate('Home')
                  this.props.navigation.goBack();
                },
              },
            ]);
            this.setState({
              pinId: '',
              enterpriseName: '',
              pinImg: '',
              pinName: '',
              nameOnMap: '',
              sellerType: '',
              contactNo: '',
              contactVisibility: '',
              lat: '',
              long: '',
              location: '',
              delivery_status: '',
              delivery_note: '',
              building: '',
              floor: '',
              officeNo: '',
              description: '',
              checked: '',
              data: '',
              timing_from_wd: '',
              timing_to_wd: '',
              // working_from_wd: '',
              // working_to_wd: '',
              timing_from_wn: '',
              timing_to_wn: '',
              working_from_wn: [],
              // working_to_wn: '',
            });
          }
        } else if (
          responseData.status == 516324 &&
          responseData.sData === null
        ) {
          const message = isRTL
            ? 'تم تحديث نقطة بيعك بنجاح '
            : 'You have successfully updated your pin';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          // this.setState({isLoading:false})
          this.props.navigation.goBack();
        }
      })
      .catch(err => {
        this.setState({isLoading: false});
        Alert.alert('Error', err.message);
      });
    this.props.checkOtpVerify('', '');
  }

  shareOpt() {
    let shareOptions = {
      title: 'SimSim Market',
      message: 'This is the best app for your search.',
      url: 'http://facebook.github.io/react-native/',
      subject: 'Market app for search', // for email
    };
    Share.share(shareOptions);
  }

  getuser = value => {
    this.setState({region: value});
  };
  openMap = async () => {
    LocationSwitch.isLocationEnabled(
      () => {
        const {navigate} = this.props.navigation;
        navigate('GetUserLocation');
      },
      () => {
        const msg1 = this.props.isRTL ? 'تشغيل محدد المواقع' : 'Use Location ?';
        const msg2 = this.props.isRTL
          ? 'لإظهار العروض الخاصة ونقاط البيع حول موقعك، يرجى تشغيل محدد المواقع GPS'
          : 'To show offers and sales point around you, turn on GPS.';
        const not = this.props.isRTL ? 'ليس الآن' : 'Not Now';
        const enable = this.props.isRTL ? 'تفعيل' : 'Enable';

        Alert.alert(
          msg1,
          msg2,
          [
            {
              text: not,
              onPress: () => {
                //this.hideLocationBoxHandler();
              },
            },
            {
              text: enable,
              onPress: () => {
                this.enableUserLocation();
              },
            },
          ],
          {cancelable: false},
        );
      },
    );
    // const jsonData =  await AsyncStorage.getItem('region');
    //  const lastLocation = JSON.parse(jsonData);
    // const region = {
    //   latitude: Number(lastLocation.coords.latitude),
    //   longitude: Number(lastLocation.coords.longitude),
    //   latitudeDelta: LATITUDE_DELTA,
    //   longitudeDelta: LONGITUDE_DELTA
    // }

    // if(this.mapView) {
    //   this.mapView._root.animateToRegion(region, 1000);
    // }
  };
  enableUserLocation = async () => {
    if (Platform.OS === 'android') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then(async data => {
          if (data == 'enabled') {
            const {navigate} = this.props.navigation;
            navigate('GetUserLocation');
          }
        })
        .catch(async err => {
          if (err.code == 'ERR00') {
            await this.hideLocationBoxHandler();
          }
        });
    }
  };
  // openMap() {
  //   const {navigate} = this.props.navigation;
  //   navigate('GetUserLocation');
  // }

  async _onPressSearch(event) {
    if (this.state.contactNo.charAt(0) == 0) {
      this.setState({errorZero: true});
      return;
    }

    const {isRTL} = this.props;
    if (this.state.contactNo.trim() === this.state.oldContact.trim()) {
      return;
    }
    const mobile = event.nativeEvent.text;
    const cCode = this.state.countryCode;
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    fetch(API_URL + '/sendpinsms', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        mobile: mobile,
        countryCode: cCode,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 236486
        ) {
          alert('Failed to add pin');
        } else if (
          responseData.status == 805465 &&
          responseData.sData !== null
        ) {
          const message = isRTL
            ? 'رقم الجوال موثق سابقا '
            : 'Mobile is already verified';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 805465 &&
          responseData.sData === null
        ) {
          this.setState({errorZero: false});
          const title = isRTL ? 'شكرا لك ' : 'Thank You';
          const message1 = isRTL
            ? 'تم ارسال رسالة مع رمز توثيق'
            : 'A message with pin code send to ';
          const message2 = isRTL
            ? 'يرجى ادخال رمز التوثيق لتحديث رقم الجوال في نقطة البيع'
            : 'please enter pin code to update mobile no ';
          const okText = isRTL ? 'نعم' : 'Ok';

          Alert.alert(
            title,
            message1 +
              this.state.countryCode +
              ' ' +
              this.state.contactNo +
              ', ' +
              message2,
            [
              {
                text: okText,
                onPress: () => {
                  this.props.navigation.navigate('Otp', {
                    mobile,
                    cCode,
                    type: 'addPin',
                  });
                },
              },
            ],
          );
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  onSelect(index, value) {
    this.setState({
      sellerType: value,
      sellerIndex: index,
    });
  }
  onChanged(text) {
    // console.warn("TCL: onChanged -> text", text.charAt(0))

    this.setState({
      contactNo: text.replace(/[^0-9]/g, ''),
    });
  }
  onClick = data => {
    // console.warn('Data-->>',data);
    const weekendDays = this.state.weekendDays;
    const index = weekendDays.indexOf(data);
    if (index < 0) {
      weekendDays.push(data);
      this.setState({weekendDays: weekendDays});
    } else {
      weekendDays.splice(index, 1);
      this.setState({weekendDays: weekendDays});
    }
  };

  getArabicText(pinType) {
    if (!pinType) {
      return;
    }

    if (pinType.toLowerCase() === 'enterprise') {
      return 'منشأة';
    } else {
      return 'فردي';
    }
  }

  render() {
    for (let i = 0; i < countriesCode.length; i++) {
      countriesCode[i].value = countriesCode[i].dial_code;
      countriesCode[i].label =
        countriesCode[i].dial_code + '-' + countriesCode[i].name;
      countriesCode[i].color = '#000000';
    }

    let weekendDays = [];
    if (
      this.props.navigation.state.params &&
      this.props.navigation.state.params.data
    ) {
      weekendDays = this.props.navigation.state.params.data.working_from_wn
        ? this.props.navigation.state.params.data.working_from_wn.split(',')
        : [];
    }
    const {navigate} = this.props.navigation;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : null}>
        <ScrollView
          keyboardShouldPersistTaps="always"
          style={{backgroundColor: '#ecf2f9', marginHorizontal: 10}}>
          <View
            style={[
              {
                flex: 1,
                justifyContent: 'center',
                marginVertical: 50,
              },
              Platform.OS === 'ios' ? {marginTop: 75} : null,
            ]}>
            <View style={{alignItems: 'center', marginTop: 20}}>
              <Image
                style={styles.logo}
                source={require('../icons/logo1x.png')}
              />
            </View>
            <View style={{alignItems: 'center', marginTop: 10}}>
              <Text style={{fontWeight: 'bold'}}>
                {this.props.isRTL ? 'رقم نقطة البيع' : 'Pin Number'}:
                {this.state.pinNumber}
              </Text>
            </View>
            <View style={{marginVertical: 10}}>
              <View
                style={[
                  styles._textbox,
                  this.state.headerButton === 'pinName'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  style={styles._textboxText}
                  onChangeText={text => this.setState({pinName: text})}
                  onFocus={() => this.onFocus('pinName')}
                  onBlur={() => this.setState({headerButton: ''})}
                  underlineColorAndroid="white"
                  keyboardType={'default'}
                  value={this.state.pinName}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.pinName')}
                />
              </View>
              <View
                style={[
                  styles._textbox,
                  this.state.headerButton === 'mapName'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  maxLength={15}
                  style={styles._textboxText}
                  onChangeText={text => this.setState({nameOnMap: text})}
                  onFocus={() => this.onFocus('mapName')}
                  onBlur={() => this.setState({headerButton: ''})}
                  underlineColorAndroid="white"
                  keyboardType={'default'}
                  value={this.state.nameOnMap}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.mapName')}
                />
              </View>
              <Text style={{fontSize: 10}}>
                {this.props.isRTL
                  ? 'الحد الأقصى لعدد أحرف الاسم على الخريطة 15 حرفا'
                  : 'Name on map can be up to 15 letters'}
              </Text>
              <View style={styles._radiotextbox}>
                <Text style={[styles._textboxText, {color: '#7A7A7A'}]}>
                  {strings('addPin.type')}{' '}
                </Text>
                {this.state.pinsType ? (
                  <Text style={[styles._textboxText, {color: '#00ADDE'}]}>
                    {this.props.isRTL
                      ? this.getArabicText(this.state.pinsType)
                      : this.state.pinsType.toUpperCase()}
                  </Text>
                ) : (
                  <RadioGroup
                    color="#7A7A7A"
                    activeColor="#00ADDE"
                    selectedIndex={this.state.sellerIndex}
                    onSelect={(index, value) => this.onSelect(index, value)}
                    style={styles._radioButton}>
                    <RadioButton
                      color="#00ADDE"
                      value="Individual"
                      style={{marginTop: 6}}>
                      <Text style={styles._radioText}>
                        {strings('addPin.individual')}
                      </Text>
                    </RadioButton>
                    <RadioButton
                      color="#00ADDE"
                      value="Enterprise"
                      style={{marginTop: 6}}>
                      <Text style={styles._radioText}>
                        {strings('addPin.enterprises')}
                      </Text>
                    </RadioButton>
                  </RadioGroup>
                )}
              </View>
              {this.state.sellerType === 'Enterprise' ||
              this.state.sellerType === 'enterprise' ? (
                <View>
                  {
                    // <View
                    // 	style={[
                    // 		styles._textbox
                    // 	]}
                    // >
                    // 	<TextInput
                    // 		style={styles._textboxText}
                    // 		onChangeText={text =>
                    // 			this.setState({ enterpriseName: text })
                    // 		}
                    // 		onFocus={() => this.onFocus('enterpriseName')}
                    // 		onBlur={() =>
                    // 			this.setState({ headerButton: '' })
                    // 		}
                    // 		underlineColorAndroid="white"
                    // 		keyboardType={'default'}
                    // 		value={this.state.enterpriseName}
                    // 		placeholderTextColor="#7A7A7A"
                    // 		placeholder={strings('addPin.enterpriseName')}
                    // 	/>
                    // </View>
                  }
                  <View style={[styles._textbox]}>
                    <TouchableOpacity
                      onPress={this.uploadImage.bind(this)}
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      {this.state.fileName ? (
                        <Text style={styles._textboxText}>
                          {this.state.fileName}
                        </Text>
                      ) : (
                        <Text style={styles._textboxText}>
                          {strings('addPin.upload')}
                        </Text>
                      )}
                      <Image
                        style={{
                          marginRight: 15,
                          marginVertical: 8,
                        }}
                        source={require('./../icons/upload2x.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              ) : null}

              <Text
                style={{
                  marginTop: 10,
                  fontWeight: 'bold',
                  textAlign: this.props.isRTL ? 'right' : 'left',
                }}>
                {this.props.isRTL ? 'خلال الاسبوع' : 'During Weekdays'}
              </Text>

              {Platform.OS === 'ios' ? (
                <View
                  style={{
                    flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View style={[styles._textbox, {width: '48%'}]}>
                    <TouchableOpacity
                      onPress={this._showDateTimePicker}
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      {this.state.showIosStartTime ? (
                        <Text style={styles._textboxText}>
                          {typeof this.state.fromTimeIos === 'string'
                            ? this.state.fromTimeIos
                            : moment(this.state.fromTimeIos).format(
                                'hh:mm:ss A',
                              )}
                        </Text>
                      ) : (
                        <Text style={styles._textboxText}>
                          {strings('addPin.timeFrom')}{' '}
                        </Text>
                      )}
                    </TouchableOpacity>
                  </View>
                  <View style={[styles._textbox, {width: '48%'}]}>
                    <TouchableOpacity
                      onPress={this._showTimePicker}
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      {this.state.showIosEndTime ? (
                        <Text style={styles._textboxText}>
                          {typeof this.state.toTimeIos === 'string'
                            ? this.state.toTimeIos
                            : moment(this.state.toTimeIos).format('hh:mm:ss A')}
                        </Text>
                      ) : (
                        <Text style={styles._textboxText}>
                          {strings('addPin.timeTo')}
                        </Text>
                      )}
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                <View
                  style={{
                    flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View style={[styles._textbox, {width: '48%'}]}>
                    <TouchableOpacity
                      onPress={this._showDateTimePicker}
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      {this.state.fromTime ? (
                        <Text style={styles._textboxText}>
                          {this.state.fromTime}
                        </Text>
                      ) : (
                        <Text style={styles._textboxText}>
                          {strings('addPin.timeFrom')}{' '}
                        </Text>
                      )}
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.isTimePickerVisible}
                      onConfirm={this._handlefromTimePicked.bind(this)}
                      onCancel={this._hideDateTimePicker.bind(this)}
                      mode="time"
                      datePickerModeAndroid="spinner"
                      is24Hour={false}
                    />
                  </View>
                  <View style={[styles._textbox, {width: '48%'}]}>
                    <TouchableOpacity
                      onPress={this._showTimePicker}
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      {this.state.toTime ? (
                        <Text style={styles._textboxText}>
                          {this.state.toTime}
                        </Text>
                      ) : (
                        <Text style={styles._textboxText}>
                          {strings('addPin.timeTo')}
                        </Text>
                      )}
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.isToTimePickerVisible}
                      onConfirm={this._handletoTimePicked.bind(this)}
                      onCancel={this._hideToDateTimePicker.bind(this)}
                      mode="time"
                      datePickerModeAndroid="spinner"
                      is24Hour={false}
                    />
                  </View>
                </View>
              )}
              {/**
               *	During weekend timing...
               */}
              <Text
                style={{
                  marginTop: 10,
                  fontWeight: 'bold',
                  textAlign: this.props.isRTL ? 'right' : 'left',
                }}>
                {this.props.isRTL ? 'نهاية الاسبوع' : 'Select Weekend Days'}
              </Text>

              <View
                style={{
                  flex: 1,
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  // justifyContent: 'space-between'
                }}>
                <RenderCheckBox
                  day={this.props.isRTL ? 'الاحد' : 'Sunday'}
                  onClick={this.onClick}
                  isChecked={
                    weekendDays.indexOf('Sunday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
                <RenderCheckBox
                  day={this.props.isRTL ? 'الاثنين' : 'Monday'}
                  onClick={this.onClick}
                  isChecked={
                    weekendDays.indexOf('Monday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  // justifyContent: 'space-between'
                }}>
                <RenderCheckBox
                  day={this.props.isRTL ? 'الثلاثاء' : 'Tuesday'}
                  onClick={this.onClick}
                  checked={
                    weekendDays.indexOf('Tuesday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
                <RenderCheckBox
                  day={this.props.isRTL ? 'الأربعاء  ' : 'Wednesday'}
                  onClick={this.onClick}
                  checked={
                    weekendDays.indexOf('Wednesday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  // justifyContent: 'space-between'
                }}>
                <RenderCheckBox
                  day={this.props.isRTL ? 'الخميس' : 'Thursday'}
                  onClick={this.onClick}
                  checked={
                    weekendDays.indexOf('Thursday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
                <RenderCheckBox
                  day={this.props.isRTL ? 'الجمعة' : 'Friday'}
                  onClick={this.onClick}
                  checked={
                    weekendDays.indexOf('Friday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  // justifyContent: 'space-between'
                }}>
                <RenderCheckBox
                  day={this.props.isRTL ? 'السبت' : 'Saturday'}
                  onClick={this.onClick}
                  checked={
                    weekendDays.indexOf('Saturday') >= 0 ? 'true' : 'false'
                  }
                  isRTL={this.props.isRTL}
                />
              </View>

              <View
                style={{
                  flex: 1,
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  // justifyContent: 'space-between'
                }}>
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>
                  {this.props.isRTL
                    ? 'اختر وقت العمل في نهاية الاسبوع'
                    : 'Select Weekend Time'}
                  :
                </Text>
                <RadioGroup
                  onSelect={(index, value) =>
                    this.setState({
                      isOpenInWeekend: value,
                      isOpenInWeekendIndex: index,
                    })
                  }
                  color="#545454"
                  activeColor="#00ADDE"
                  selectedIndex={this.state.isOpenInWeekendIndex}
                  style={styles._radioButton}>
                  <RadioButton
                    value="Open"
                    // style={{ marginTop: 5 }}
                  >
                    <Text
                      style={{
                        color: '#545454',
                        paddingBottom: 2,
                      }}>
                      {this.props.isRTL ? 'مفتوح' : 'Open'}
                    </Text>
                  </RadioButton>
                  <RadioButton
                    value="Close"
                    // style={{ marginTop: 5 }}
                  >
                    <Text
                      style={{
                        color: '#545454',
                        paddingBottom: 2,
                      }}>
                      {this.props.isRTL ? 'مغلق' : 'Close'}
                    </Text>
                  </RadioButton>
                </RadioGroup>
              </View>
              {this.state.isOpenInWeekend === 'Open' && (
                <View>
                  {Platform.OS === 'ios' ? (
                    <View
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View style={[styles._textbox, {width: '48%'}]}>
                        <TouchableOpacity
                          onPress={() =>
                            Platform.OS === 'ios'
                              ? this.refs.modal3.open()
                              : this.setState({
                                  isWeenEndTimeFromPickerVisible: true,
                                })
                          }
                          style={{
                            flexDirection: this.props.isRTL
                              ? 'row-reverse'
                              : 'row',
                            justifyContent: 'space-between',
                          }}>
                          {this.state.showIosWeekndStartTime ? (
                            <Text style={styles._textboxText}>
                              {typeof this.state.weekEndTimeFromIos === 'string'
                                ? this.state.weekEndTimeFromIos
                                : moment(this.state.weekEndTimeFromIos).format(
                                    'hh:mm:ss A',
                                  )}
                            </Text>
                          ) : (
                            <Text style={styles._textboxText}>
                              {strings('addPin.iOSWeekendTimeFrom')}{' '}
                            </Text>
                          )}
                        </TouchableOpacity>
                      </View>
                      <View style={[styles._textbox, {width: '48%'}]}>
                        <TouchableOpacity
                          onPress={() =>
                            Platform.OS === 'ios'
                              ? this.refs.modal4.open()
                              : this.setState({
                                  isWeenEndTimeToPickerVisible: true,
                                })
                          }
                          style={{
                            flexDirection: this.props.isRTL
                              ? 'row-reverse'
                              : 'row',
                            justifyContent: 'space-between',
                          }}>
                          {this.state.showIosWeekndEndTime ? (
                            <Text style={styles._textboxText}>
                              {typeof this.state.weekEndTimeToIos === 'string'
                                ? this.state.weekEndTimeToIos
                                : moment(this.state.weekEndTimeToIos).format(
                                    'hh:mm:ss A',
                                  )}
                            </Text>
                          ) : (
                            <Text style={styles._textboxText}>
                              {strings('addPin.iOSWeekendTimeTo')}
                            </Text>
                          )}
                        </TouchableOpacity>
                      </View>
                    </View>
                  ) : (
                    <View
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View style={[styles._textbox, {width: '48%'}]}>
                        <TouchableOpacity
                          onPress={() =>
                            Platform.OS === 'ios'
                              ? this.refs.modal3.open()
                              : this.setState({
                                  isWeenEndTimeFromPickerVisible: true,
                                })
                          }
                          style={{
                            flexDirection: this.props.isRTL
                              ? 'row-reverse'
                              : 'row',
                            justifyContent: 'space-between',
                          }}>
                          {this.state.weekEndTimeFrom ? (
                            <Text style={styles._textboxText}>
                              {this.state.weekEndTimeFrom}
                            </Text>
                          ) : (
                            <Text style={styles._textboxText}>
                              {strings('addPin.timeFrom')}{' '}
                            </Text>
                          )}
                        </TouchableOpacity>
                        <DateTimePicker
                          isVisible={this.state.isWeenEndTimeFromPickerVisible}
                          onConfirm={this._handleWeekEndFromTimePicked.bind(
                            this,
                          )}
                          onCancel={this._hideWeekEndTimeFromPicker}
                          mode="time"
                          datePickerModeAndroid="spinner"
                          is24Hour={false}
                        />
                      </View>
                      <View style={[styles._textbox, {width: '48%'}]}>
                        <TouchableOpacity
                          onPress={() =>
                            Platform.OS === 'ios'
                              ? this.refs.modal4.open()
                              : this.setState({
                                  isWeenEndTimeToPickerVisible: true,
                                })
                          }
                          style={{
                            flexDirection: this.props.isRTL
                              ? 'row-reverse'
                              : 'row',
                            justifyContent: 'space-between',
                          }}>
                          {this.state.weekEndTimeTo ? (
                            <Text style={styles._textboxText}>
                              {this.state.weekEndTimeTo}
                            </Text>
                          ) : (
                            <Text style={styles._textboxText}>
                              {strings('addPin.timeTo')}
                            </Text>
                          )}
                        </TouchableOpacity>
                        <DateTimePicker
                          isVisible={this.state.isWeenEndTimeToPickerVisible}
                          onConfirm={this._handleWeekEndToTimePicked.bind(this)}
                          onCancel={this._hideWeekEndTimeToPicker.bind(this)}
                          mode="time"
                          datePickerModeAndroid="spinner"
                          is24Hour={false}
                        />
                      </View>
                    </View>
                  )}
                </View>
              )}

              <View style={styles._radiotextbox}>
                <Text style={[styles._textboxText, {color: '#7A7A7A'}]}>
                  {strings('addPin.delivery')}
                </Text>
                <RadioGroup
                  color="#7A7A7A"
                  activeColor="#00ADDE"
                  selectedIndex={this.state.delivery_index}
                  onSelect={(index, value) =>
                    this.setState({
                      delivery_status: value,
                    })
                  }
                  style={styles._radioButton}>
                  <RadioButton
                    color="#00ADDE"
                    value="Yes"
                    style={{marginTop: 6}}>
                    <Text style={styles._radioText}>
                      {strings('addPin.yes')}
                    </Text>
                  </RadioButton>
                  <RadioButton
                    color="#00ADDE"
                    value="No"
                    style={{marginTop: 6}}>
                    <Text style={styles._radioText}>
                      {strings('addPin.no')}
                    </Text>
                  </RadioButton>
                </RadioGroup>
              </View>
              {this.state.delivery_status.toLocaleLowerCase() == 'yes' && (
                <View
                  style={[
                    styles._textbox,
                    this.state.headerButton === 'notes'
                      ? {
                          borderWidth: 2,
                          borderColor: '#71c1f2',
                        }
                      : {},
                  ]}>
                  <TextInput
                    style={styles._textboxText}
                    onChangeText={text =>
                      this.setState({
                        delivery_note: text,
                      })
                    }
                    onFocus={() => this.onFocus('notes')}
                    onBlur={() => this.setState({headerButton: ''})}
                    underlineColorAndroid="white"
                    keyboardType={'default'}
                    value={this.state.delivery_note}
                    placeholderTextColor="#7A7A7A"
                    placeholder={strings('addPin.notes')}
                  />
                </View>
              )}
              <View style={[styles._textbox]}>
                <TouchableOpacity
                  onPress={this.PinImage.bind(this)}
                  style={{
                    flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                    justifyContent: 'space-between',
                  }}>
                  {this.state.pinFile ? (
                    <Text style={styles._textboxText}>
                      {this.state.pinFile}
                    </Text>
                  ) : (
                    <Text style={styles._textboxText}>
                      {strings('addPin.pinImage')}
                    </Text>
                  )}
                  <Image
                    style={{
                      marginRight: 15,
                      marginVertical: 8,
                      width: 28,
                      resizeMode: 'contain',
                    }}
                    source={require('./../icons/upload2x.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={[styles._textbox, {height: 70}]}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                  onPress={this.openMap}>
                  {this.state.location ? (
                    <View style={{flex: 3.5, marginRight: 5}}>
                      <Text
                        numberOfLines={2}
                        ellipsizeMode="tail"
                        style={styles._textboxText}>
                        {this.state.location}
                      </Text>
                    </View>
                  ) : (
                    <Text style={[styles._textboxText, {flex: 3.5}]}>
                      {strings('addPin.location')}
                    </Text>
                  )}
                  <View style={{flex: 0.5}}>
                    <Image
                      style={{
                        // marginLeft: 5,
                        // marginVertical: 8,
                        width: 28,
                        resizeMode: 'contain',
                      }}
                      source={require('./../icons/location.png')}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.ccode}>
                {Platform.OS === 'android' ? (
                  <Picker
                    selectedValue={this.state.countryCode}
                    onValueChange={(itemValue, itemIndex) =>
                      this.setState({countryCode: itemValue})
                    }>
                    <Picker.Item label="Code" />
                    {countriesCode.map((item, key) => (
                      <Picker.Item
                        key={key}
                        label={item.dial_code + '-' + item.name}
                        value={item.dial_code}
                      />
                    ))}
                  </Picker>
                ) : (
                  <View
                    style={[
                      styles.ccode,
                      Platform.OS === 'ios'
                        ? {left: 20, top: 0, marginBottom: 10}
                        : {left: 20, top: 0},
                    ]}>
                    <RNPickerSelect
                      placeholder={{
                        label: 'Select CountryCode',
                        value: null,
                      }}
                      value={this.state.countryCode}
                      onValueChange={(itemValue, index) =>
                        this.setState({countryCode: itemValue})
                      }
                      style={{
                        placeholderColor: 'black',
                      }}
                      hideIcon
                      items={countriesCode}
                    />
                  </View>
                )}
              </View>
              <View
                style={[
                  styles._textbox,
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  },
                  this.state.headerButton === 'contact'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  maxLength={13}
                  style={[styles._textboxText, {width: '60%'}]}
                  onChangeText={text => this.onChanged(text)}
                  onFocus={() => this.onFocus('contact')}
                  onBlur={() => this.setState({headerButton: ''})}
                  onEndEditing={this._onPressSearch.bind(this)}
                  underlineColorAndroid="white"
                  keyboardType={'numeric'}
                  value={this.state.contactNo}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.contact')}
                />

                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Switch
                    activeText={this.props.isRTL ? 'اظهار' : 'Show'}
                    inactiveText={this.props.isRTL ? 'اخفاء' : 'Hide'}
                    switchWidth={100}
                    switchHeight={48}
                    switchBorderRadius={25}
                    activeBackgroundColor={'rgb(38, 198, 242)'}
                    value={this.state.switchValue}
                    onChangeValue={() =>
                      this.setState({
                        switchValue: !this.state.switchValue,
                      })
                    }
                  />
                </View>
              </View>
              {this.state.errorZero && (
                <Text style={{color: 'red'}}>
                  {this.props.isRTL
                    ? '50xxxxx77 :' +
                      'اكتب رقم جوالك من دون صفر في بداية الرقم، مثلا'
                    : 'Write your mobile number without zero at begining, example: 50xxxxx77'}
                </Text>
              )}

              <View
                style={[
                  styles._textbox,
                  this.state.headerButton === 'building'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  style={styles._textboxText}
                  onChangeText={text => this.setState({building: text})}
                  onFocus={() => this.onFocus('building')}
                  onBlur={() => this.setState({headerButton: ''})}
                  underlineColorAndroid="white"
                  keyboardType={'default'}
                  value={this.state.building}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.building')}
                />
              </View>
              <View
                style={[
                  styles._textbox,
                  this.state.headerButton === 'floor'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  style={styles._textboxText}
                  onChangeText={text => this.setState({floor: text})}
                  onFocus={() => this.onFocus('floor')}
                  onBlur={() => this.setState({headerButton: ''})}
                  underlineColorAndroid="white"
                  keyboardType={'default'}
                  value={this.state.floor}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.floor')}
                />
              </View>
              <View
                style={[
                  styles._textbox,
                  this.state.headerButton === 'store'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  style={styles._textboxText}
                  onChangeText={text => this.setState({officeNo: text})}
                  onFocus={() => this.onFocus('store')}
                  onBlur={() => this.setState({headerButton: ''})}
                  underlineColorAndroid="white"
                  keyboardType={'default'}
                  value={this.state.officeNo}
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('addPin.store')}
                />
              </View>
              <View
                style={[
                  styles._descriptionbox,
                  this.state.headerButton === 'description'
                    ? {
                        borderWidth: 2,
                        borderColor: '#71c1f2',
                      }
                    : {},
                ]}>
                <TextInput
                  style={styles._textboxText}
                  onChangeText={text => this.setState({description: text})}
                  onFocus={() => this.onFocus('description')}
                  onBlur={() => this.setState({headerButton: ''})}
                  returnKeyType={'done'}
                  multiline={true}
                  underlineColorAndroid="white"
                  numberOfLines={4}
                  keyboardType={'default'}
                  value={this.state.description}
                  placeholderTextColor="#7A7A7A"
                  textAlignVertical="top"
                  placeholder={strings('addPin.description')}
                  blurOnSubmit={true}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                  }}
                />
              </View>

              {this.type !== 'edit' && (
                <View
                  style={{
                    flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  }}>
                  <CheckBox
                    style={{padding: 10}}
                    onClick={() =>
                      this.setState({
                        checked: !this.state.checked,
                      })
                    }
                    isChecked={this.state.checked}
                    checkBoxColor={'#5DADE2'}
                  />
                  <TouchableOpacity
                    style={{marginLeft: 0}}
                    onPress={() => navigate('Policy')}>
                    <Text
                      style={{
                        marginTop: 10,
                        color: '#0000FF',
                        fontSize: 15,
                        textDecorationLine: 'underline',
                      }}>
                      {strings('addPin.terms')}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              {this.state.isLoading ? <PreLoader /> : <View />}
              <TouchableOpacity
                style={[styles._textbox, styles._button]}
                onPress={this._handleSubmit.bind(this)}>
                <Text style={styles._buttonText}>
                  {strings('addPin.submit')}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        <Modal
          style={{
            borderRadius: 20,
            height: '50%',
            width: '80%',
            borderWidth: 1,
            borderColor: 'black',
          }}
          position={'center'}
          ref={'modal1'}
          isDisabled={this.state.isDisabled}>
          <View
            style={{
              backgroundColor: 'white',
              marginTop: 15,
              borderRadius: 20,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: 'grey',
                borderRadius: 20,
                width: '80%',
                marginBottom: 5,
              }}>
              <DatePickerIOS
                date={new Date(this.state.fromTimeIos)}
                onDateChange={this._handlefromTimePicked}
                mode="time"
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={this.cancelFromTimePicker}>
                <View
                  style={{
                    right: 10,
                    left: -20,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this._hideDateTimePicker}>
                <View
                  style={{
                    left: 10,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Confirm
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          style={{
            borderRadius: 20,
            height: '50%',
            width: '80%',
            borderWidth: 1,
            borderColor: 'black',
          }}
          position={'center'}
          ref={'modal2'}
          isDisabled={this.state.isDisabled}>
          <View
            style={{
              backgroundColor: 'white',
              marginTop: 15,
              borderRadius: 20,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: 'grey',
                borderRadius: 20,
                width: '80%',
                marginBottom: 5,
              }}>
              <DatePickerIOS
                date={new Date(this.state.toTimeIos)}
                onDateChange={this._handletoTimePicked}
                mode="time"
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={this.cancelToTimePicker}>
                <View
                  style={{
                    right: 10,
                    left: -20,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this._hideToDateTimePicker}>
                <View
                  style={{
                    left: 10,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Confirm
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          style={{
            borderRadius: 20,
            height: '50%',
            width: '80%',
            borderWidth: 1,
            borderColor: 'black',
          }}
          position={'center'}
          ref={'modal3'}
          isDisabled={this.state.isDisabled}>
          <View
            style={{
              backgroundColor: 'white',
              marginTop: 15,
              borderRadius: 20,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: 'grey',
                borderRadius: 20,
                width: '80%',
                marginBottom: 5,
              }}>
              <DatePickerIOS
                date={new Date(this.state.weekEndTimeFromIos)}
                onDateChange={this._handleWeekEndFromTimePicked}
                mode="time"
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={this.cancelWeekndFromTimePicker}>
                <View
                  style={{
                    right: 10,
                    left: -20,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this._hideWeekEndTimeFromPicker}>
                <View
                  style={{
                    left: 10,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Confirm
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          style={{
            borderRadius: 20,
            height: '50%',
            width: '80%',
            borderWidth: 1,
            borderColor: 'black',
          }}
          position={'center'}
          ref={'modal4'}
          isDisabled={this.state.isDisabled}>
          <View
            style={{
              backgroundColor: 'white',
              marginTop: 15,
              borderRadius: 20,
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                borderWidth: 2,
                borderColor: 'grey',
                borderRadius: 20,
                width: '80%',
                marginBottom: 5,
              }}>
              <DatePickerIOS
                date={new Date(this.state.weekEndTimeToIos)}
                onDateChange={this._handleWeekEndToTimePicked}
                mode="time"
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={this.cancelWeekndToTimePicker}>
                <View
                  style={{
                    right: 10,
                    left: -20,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this._hideWeekEndTimeToPicker}>
                <View
                  style={{
                    left: 10,
                    borderWidth: 1,
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 20,
                    width: '125%',
                  }}>
                  <Text
                    style={{fontSize: 20, fontWeight: 'bold', color: 'grey'}}>
                    Confirm
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </KeyboardAvoidingView>
    );
  }
}

const RenderCheckBox = props => {
  const {day, checked, onClick, isRTL} = props;

  const val = checked == 'true';
  const flexValue = Platform.OS === 'ios' ? 0.2 : 0.3;

  return isRTL ? (
    <CheckBox
      style={{flex: flexValue, padding: 5}}
      onClick={() => onClick(day)}
      isChecked={val}
      leftText={day}
    />
  ) : (
    <CheckBox
      style={{flex: 1, padding: 5}}
      onClick={() => onClick(day)}
      isChecked={val}
      rightText={day}
    />
  );
};

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    mobile: state.network.mobile,
    cCode: state.network.cCode,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddPin);
