import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  // ListView,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  ActivityIndicator,
  RefreshControl,
  Alert,
  NativeModules,
  FlatList,
} from 'react-native';
const {GPSState} = NativeModules;
import Tabs from './TabView';
import FloatingButton from './FloatingButton';
import StarRating from 'react-native-star-rating';
import HomePageHeader from './maps/HomePageHeader';
import Filter from './homeFilter';
import DeviceInfo from 'react-native-device-info';
// import GPSState from './GPSState';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/ListView';
import * as seachActions from '../actions/Search';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import SpecialOffers from './SpecialOffers';
import API_URL from '../configuration/configuration';

const Favourite = props => {
  return (
    <View>
      <Image
        source={
          props.item.isfavourite || props.item.Favorites == 1
            ? require('./../icons/favourite2x.png')
            : null
        }
        style={props.styles.favourites}
      />
    </View>
  );
};

class List extends Component {
  static navigationOptions = ({navigation}) => ({
    header: <HomePageHeader navigation={navigation} viewType="list" />,
  });

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      dataSource: [],
      source: false,
      id: '',
      isfavourite: false,
      pinId: '',
      userId: '',
      favorite: [],
      token: '',
      refreshing: false,
      isLoading: true,
      favouritePins: '',
    };
    this.handleFavourite = this.handleFavourite.bind(this);
    this.setModalStatus = this.setModalStatus.bind(this);
    this.countVisitors = this.countVisitors.bind(this);
  }

  getAllList = async () => {
    this.setState({isLoading: true});
    const {navigate} = this.props.navigation;
    // const deviceId = await DeviceInfo.getDeviceId();
    AsyncStorage.getItem('region').then(json => {
      const region = JSON.parse(json);
      // console.warn('---->>',region)
      const lat = region.coords.latitude;
      const lng = region.coords.longitude;
      this.props.searchWithLocationAPI(this.state.userId, lat, lng, 'listing');
    });

    // if (
    //   this.props.filterData !== undefined &&
    //   this.props.filterData.searchPins != undefined
    // ) {
    //   this.setState({ dataSource: this.props.filterData.searchPins });
    // } else {

    //   fetch('http://www.simtest.n1.iworklab.com/public/api/get-pins', {
    //     method: 'GET',
    //     headers: {
    //       Accept: 'application/json',
    //       'Content-Type': 'application/json'
    //     }
    //   })
    //     .then(response => response.json())
    //     .then(async responseData => {
    //       // console.warn('pins-->>',responseData)
    //       if (
    //         responseData.status == 654864 &&
    //         responseData.eData.eCode == 545864
    //       ) {
    //         alert('Failed to get pin');
    //       } else if (
    //         responseData.status == 516324 &&
    //         responseData.eData === null
    //       ) {
    //         this.setState({
    //           dataSource: responseData.allPins,
    //           refreshing: false,
    //           isLoading: false
    //         });
    //       }
    //     })
    //     .catch(err => {
    //       Alert.alert('Internet Connection is not Working/Available',err);
    //     });
    // }
  };
  getFavouritesPin(pinId) {
    const favouritePins = this.state.favouritePins;
    const fav = favouritePins.length
      ? favouritePins.find(ob => ob.id == pinId)
      : null;
    if (fav) {
      return true;
    } else {
      return false;
    }
  }
  getFavouritesList() {
    fetch(API_URL + '/favorite', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('favouritePins-->>',responseData);
        // if (
        //   responseData.status == 654864 &&
        //   responseData.eData.eCode == 545864
        // ) {
        //   Alert.alert('', 'Favorite pins not found');
        // } else
        if (responseData.status == 516324 && responseData.eData === null) {
          this.setState({favouritePins: responseData.FavPinList});
        }
      })
      .catch(err => {
        Alert.alert('');
        // console.warn(err);
      });
  }

  componentDidMount() {
    // console.warn('props-->>',this.props.searchWithLocationAPI)
    AsyncStorage.getItem('user_data').then(json => {
      if (json) {
        const userData = JSON.parse(json);
        this.setState({token: userData.sData, userId: userData.userId});
        this.getFavouritesList();
      }
    });
    this.getAllList();
    // this.props.navigation.addListener('didFocus', async event => {
    //   this.getAllList();
    // })
  }

  _onRefresh() {
    this.setState({refreshing: true});
    if (this.state.token) {
      this.getFavouritesList();
    }
    this.getAllList();
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('requestLoader-->>',nextProps.requestLoader)
    if (nextProps.filterData && nextProps.filterData.allPins) {
      this.setState({
        dataSource: nextProps.filterData.allPins,
        isLoading: false,
      });
      // if (nextProps.filterData.allPins && nextProps.filterData.allPins !== this.state.dataSource) {
      //   this.setState({ dataSource: nextProps.filterData.allPins, isLoading: false });
      // } else {
      //   this.setState({ dataSource: [], isLoading: false });
      // }
    } else {
      this.setState({dataSource: [], isLoading: false});
    }
  }

  async handleFavourite(id, uId, index) {
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    if (this.state.isfavourite) {
      this.handleRemoveFavourite(id, uId);
    } else {
      fetch(API_URL + '/add-favorite', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          pinId: id,
          userId: uId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            alert('Failed to post Ad');
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            //this.setState({favourite: [...this.state.favourite, id]})
            const copyNoteData = Object.assign([], this.state.dataSource);
            //copyNoteData.noteData[index] = e.target.value;
            copyNoteData[index].Favorites = 1;
            this.setState({dataSource: copyNoteData, isfavourite: true});
          }
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
  }

  SubsColor(plan) {
    let color = '';
    if (plan === 'Premium') {
      color = 'green';
    } else {
      color = 'blue';
    }
  }

  async countVisitors(item) {
    const deviceId = await DeviceInfo.getDeviceId();
    const url = API_URL + '/no-of-visitor';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pinId: item.id,
        userId: this.state.userId,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {})
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  async setHistory(item) {
    this.countVisitors(item);
    const {navigate} = this.props.navigation;
    const token = this.state.token;
    navigate('SellerContainer', {item, token});
    if (token) {
      fetch(API_URL + '/create-pin-history-per-click', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          pinId: item.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          // console.warn(responseData);
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
  }
  setModalStatus(status) {
    this.setState({modalVisible: status});
  }

  Capitalize(str) {
    if (!str) {
      return;
    }

    const name = str.charAt(0).toUpperCase() + str.slice(1);

    if (name.toLowerCase() === 'platinum') {
      return 'Golden';
    }

    return name;
  }

  getArabicType(name) {
    // console.warn('getArabicType')
    if (name.toLowerCase() == 'premium') {
      return 'مميز';
    } else if (name.toLowerCase() == 'platinum') {
      return 'ذهبي';
    } else if (name == 'Free' || name == 'free') {
      return 'مجاني';
    } else if (name.toLowerCase() == 'individual') {
      return 'فردي';
    } else if (name.toLowerCase() == 'enterprise') {
      return 'منشأة';
    } else if (name.toLowerCase() == 'delivery') {
      return 'توصيل';
    } else {
      return name;
    }
  }

  render() {
    // console.warn('loader-->>',this.props.requestLoader)
    const {favorite} = this.state;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const isRTL = this.props.isRTL;
    const {navigate} = this.props.navigation;
    if (this.state.dataSource === undefined) {
      return null;
    } else {
      const style = this.props.isRTL ? {left: 28} : {right: 28};
      return (
        <View style={styles.container}>
          <View style={{flex: 0}}>
            <Tabs mapView={false} />
          </View>
          <View style={{flex: 1}}>
            {this.props.requestLoader ? (
              <PreLoader />
            ) : this.state.dataSource.length ? (
              <FlatList
                data={this.state.dataSource}
                keyExtractor={item => item.id.toString()}
                extraData={isRTL}
                renderItem={({item}) => {
                  item.isfavourite = this.getFavouritesPin(item.id);
                  return (
                    <TouchableOpacity
                      onPress={this.setHistory.bind(this, item)}
                      key={item.id}>
                      <View style={styles.adBox}>
                        <Image
                          style={styles.icon}
                          source={{uri: item.pin_image ? item.pin_image : null}}
                        />
                        <View style={styles._textBox}>
                          <View
                            style={{
                              flexDirection: this.props.isRTL
                                ? 'row-reverse'
                                : 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text style={styles.title}>{item.pin_name} </Text>
                            <View
                              style={{
                                flexDirection: this.props.isRTL
                                  ? 'row-reverse'
                                  : 'row',
                              }}>
                              <View
                                style={{
                                  paddingTop: 4,
                                  flexDirection: this.props.isRTL
                                    ? 'row-reverse'
                                    : 'row',
                                }}>
                                <Text style={{color: 'green'}}>
                                  {item.rating ? item.rating.toFixed(2) : 0}{' '}
                                </Text>
                                <StarRating
                                  disabled={true}
                                  maxStars={5}
                                  rating={item.rating}
                                  starSize={12}
                                  emptyStarColor="#cacdd1"
                                  fullStarColor="green"
                                  emptyStar="star"
                                  containerStyle={{paddingTop: 3}}
                                />
                              </View>
                              {this.state.token ? (
                                <Favourite
                                  favouritePins={this.state.favouritePins}
                                  handleFavourite={this.handleFavourite}
                                  item={item}
                                  styles={styles}
                                  index={item.id}
                                />
                              ) : null}
                            </View>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              flexDirection: this.props.isRTL
                                ? 'row-reverse'
                                : 'row',
                            }}>
                            <Text style={styles.description}>
                              {this.Capitalize(item.description)}
                            </Text>
                          </View>
                          <View
                            style={{
                              flexDirection: this.props.isRTL
                                ? 'row-reverse'
                                : 'row',
                              marginVertical: 10,
                              marginHorizontal: 10,
                            }}>
                            {item.subscription_plan.toLowerCase() ===
                            'free' ? null : (
                              <View
                                style={[
                                  styles.button,
                                  {
                                    backgroundColor:
                                      item.subscription_plan === 'Premium'
                                        ? 'green'
                                        : '#ffbf00',
                                  },
                                ]}>
                                <Text
                                  style={[
                                    styles.button_text,
                                    {
                                      color:
                                        this.Capitalize(
                                          item.subscription_plan,
                                        ) === 'Golden'
                                          ? '#804000'
                                          : '#FFFFFF',
                                    },
                                  ]}>
                                  {isRTL
                                    ? this.getArabicType(item.subscription_plan)
                                    : this.Capitalize(item.subscription_plan)}
                                </Text>
                              </View>
                            )}
                            {item.pin_type &&
                            item.pin_type.toLowerCase() === 'enterprise' ? (
                              <View
                                style={[
                                  styles.button,
                                  {backgroundColor: 'black'},
                                ]}>
                                <Text style={styles.button_text}>
                                  {isRTL
                                    ? this.getArabicType(item.pin_type)
                                    : this.Capitalize(item.pin_type)}
                                </Text>
                              </View>
                            ) : (
                              <View
                                style={[
                                  styles.button,
                                  {backgroundColor: 'grey'},
                                ]}>
                                <Text style={styles.button_text}>
                                  {isRTL
                                    ? this.getArabicType(item.pin_type)
                                    : this.Capitalize(item.pin_type)}
                                </Text>
                              </View>
                            )}
                            {item.delivery_status ? (
                              <View>
                                {item.delivery_status.toLowerCase() ===
                                'no' ? null : (
                                  <View
                                    style={[
                                      styles.button,
                                      {backgroundColor: 'red'},
                                    ]}>
                                    <Text style={styles.button_text}>
                                      {isRTL
                                        ? this.getArabicType('delivery')
                                        : 'Delivery'}
                                    </Text>
                                  </View>
                                )}
                              </View>
                            ) : null}
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            ) : (
              <NoDataFound />
            )}
          </View>

          <View
            style={{
              right: 28,
              bottom: 80,
              position: 'absolute',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setModalStatus(true);
              }}>
              <Image
                style={{width: 55, height: 55}}
                source={require('../icons/filter1x.png')}
              />
            </TouchableOpacity>
          </View>
          <FloatingButton {...this.props} />
          <Filter
            modalVisible={this.state.modalVisible}
            setModalStatus={this.setModalStatus}
          />
          <SpecialOffers {...this.props} />
        </View>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    filterData: state.search.filterData,
    requestLoader: state.search.loader,
    status: state.search.status,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...layoutActions, ...seachActions}, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(List);
