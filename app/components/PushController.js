import React, { Component } from 'react';
import PushNotification from 'react-native-push-notification';

export default class PushController extends Component {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    // console.warn(this.props.navigation.navigate('Messages'))
    const { navigate } = this.props.navigation;
    PushNotification.configure({
      onNotification: function(notification) {
        if(notification.target === 'Messages'){
          console.warn('onNotification--')
          navigate('Messages');
        }
        console.log( 'NOTIFICATION:', notification.target );
      },
    });
  }

  render() {
    return null;
  }
}