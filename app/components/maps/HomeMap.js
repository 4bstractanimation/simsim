import React, {Component} from 'react';
import {
  // Text,
  View,
  StyleSheet,
  // ScrollView,
  Image,
  // NetInfo,
  ImageBackground,
  Text,
  TouchableOpacity,
  AsyncStorage,
  Platform,
  Linking,
  Dimensions,
  // AppState,
  Alert,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Tabs from '../TabView';
import Maps from './Maps';
import FloatingButton from '../FloatingButton';
import HomePageHeader from './HomePageHeader';
// import DeviceInfo from 'react-native-device-info';
// import App from '../App';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as layoutActions from '../../actions/layout';
import SpecialOffers from './../SpecialOffers';
// import OneSignal from 'react-native-onesignal';
import Geocoder from 'react-native-geocoding';
import getCurrencyCode from './../Currency.js';
import * as blinkingActions from '../../actions/blinkingAction';
import API_URL from '../../configuration/configuration';

const KEYS_TO_FILTERS = ['title'];
let width = Dimensions.get('window').width;
let height = Dimensions.get('window').height;
class HomeMap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: '',
      userToken: '',
      initNotif: '',
      userid: '',
      isIndication: false,
    };
    // OneSignal.init("11a642c1-9671-4e10-ac73-ca5fba2c0cdf");
    //   	OneSignal.addEventListener('opened', this.onOpened);
  }

  static navigationOptions = ({navigation}) => ({
    header: <HomePageHeader navigation={navigation} viewType="map" />,
  });

  // onReceived(notification) {
  //     console.log('recieve notification')
  //     console.log("Notification received: ", notification);
  // }

  // onOpened = (openResult) => {
  // 	 	// console.warn('Message: ', openResult.notification.payload.body);
  // 	    // console.warn('data: ', openResult.notification.payload.additionalData);
  // 	 	const data = openResult.notification.payload.additionalData;
  // 	 	// console.warn('data-->>', data)

  // 	 	const { navigate } = this.props.navigation;
  // 	 	switch(data.type) {

  // 	 		case 'message': navigate('Message');
  // 	 			break;
  // 	 		case 'news': const newsNotification = {id: data.pinId, pin_name: data.pin_name, isfavourite: true}
  // 	 			this.countVisitors(data.pinId)
  // 	 			navigate('SellerContainer',{newsNotification});
  // 	 			break;
  // 	 		case 'pin': const specialOfferAlarmNotification = {id: data.pinId, pin_name: data.pin_name, isfavourite: true}
  // 	 			this.countVisitors(data.pinId)
  // 	 			navigate('SellerContainer',{item: specialOfferAlarmNotification});
  // 	 			break;
  // 	 		case 'pin approved': navigate('Pin');
  // 	 			break;
  // 	 		case 'publish': navigate('Pin');
  // 	 			break;
  // 	 		case 'offer': const item1 = {id: data.pinId, pin_name: data.pinName };
  // 	 			this.countVisitors(data.pinId)
  // 	 			navigate('SellerContainer',{item: item1});
  // 	 			break;
  // 	 		// case 'CommercialNotify': const item2 = {id: data.data.pinId, pin_name: data.data.pinName };
  // 	 		// 	// this.countVisitors(item2.id)
  // 	 		// 	navigate('SellerContainer',{item: item2});
  // 	 		// 	break;
  // 	 		case 'budget':
  // 	 			navigate('ManageCommercial');
  // 	 			break;
  // 	 		case 'commercial request':
  // 	 			navigate('ManageCommercial');
  // 	 			break;
  // 	 		case 'plan':
  // 	 			navigate('Subscription');
  // 	 			break;
  // 	 		case 'extendads':
  // 	 			navigate('SellerContainer',{item: data.pinDetail});
  // 	 			break;
  // 	 		case 'CommercialNotify':
  // 	 			const userId = data.data.user_id;
  // 	 			navigate('SellerContainer',{item: {id: data.data.pinId, pin_name: data.data.pinName}});
  // 	 			this.countVisitors(data.data.pinId)
  // 	 			fetch( API_URL+'/update-pushnotification-amount', {
  // 					method: 'POST',
  // 					headers: {
  // 					'Accept': 'application/json',
  // 					'Content-Type': 'application/json',
  // 					'Authorization': this.state.userToken
  // 				},
  // 				body: JSON.stringify({userId: userId})
  // 				}).then((response) => response.json())
  // 				.then((responseData) => {
  // 					// console.warn('deduct-->>', responseData)

  // 				}).catch((err) => {
  // 					Alert.alert('Error',err.message);

  // 				});

  // 	 			break;
  // 	 		default: break;

  // 	 	}
  // 	 }

  // 	 countVisitors(pinid) {

  // 	    const url = API_URL+'/no-of-visitor';
  // 	    fetch(url, {
  // 	      method: 'POST',
  // 	      headers: {
  // 	        Accept: 'application/json',
  // 	        'Content-Type': 'application/json'
  // 	      },
  // 	      body: JSON.stringify({
  // 	        pinId: pinid
  // 	      })
  // 	    })
  // 	      .then(response => response.json())
  // 	      .then( responseData => {
  // 	        // console.warn('no-of-visitor-->',responseData);
  // 	      })
  // 	      .catch(err => {
  // 	        Alert.alert('Error',err.message);
  // 	      });
  // 	}

  checkProfileCompleted() {
    AsyncStorage.getItem('isProfileCompleted').then(json => {
      const res = JSON.parse(json);
      if (res && !res.isCompleted) {
        if (res.counter === 4) {
          const {isRTL} = this.props;
          const message = isRTL
            ? 'يرجى اكمال حسابك الشخصي'
            : 'Please complete your profile';
          const yesText = this.props.isRTL ? 'نعم' : 'Yes';
          const laterButtonText = isRTL ? 'لاحقا' : 'Later';
          Alert.alert(
            '',
            message,
            [
              {
                text: yesText,
                onPress: () => {
                  this.props.navigation.navigate('Profile');
                },
              },
              {
                text: laterButtonText,
                onPress: () => {
                  res.counter = 0;
                  AsyncStorage.setItem(
                    'isProfileCompleted',
                    JSON.stringify(res),
                  );
                },
              },
            ],
            {cancelable: false},
          );
        }
      }
    });
  }

  async getCurrentLocation() {
    const jsonData = await AsyncStorage.getItem('region');

    const location = JSON.parse(jsonData);

    if (!location || !location.coords) {
      return true;
    }

    const latitude = location.coords.latitude;
    const longitude = location.coords.longitude;

    /*** Getting country name and country code here **/
    Geocoder.from(latitude, longitude)
      .then(json => {
        const {long_name, short_name} = json.results[0].address_components[6];

        const currencyCode = getCurrencyCode(short_name);

        let countryLocation = {
          countryName: long_name,
          countryCode: short_name,
          currencyCode: currencyCode,
        };
        // console.warn('countryLocation-->>',countryLocation)
        this.props.setCountryDetail(countryLocation);
      })
      .catch(error => Alert.alert('Error', error.message));
  }

  async componentDidMount() {
    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleConnectivityChange,
    );

    AsyncStorage.getItem('user_data').then(json => {
      if (json) {
        const userData = JSON.parse(json);
        this.setState({userToken: userData.sData, userid: userData.userId});
        this.props.setLoginData(userData);
        this.checkProfileCompleted();

        this.getSellerSubscriptionPlan();

        this.props.navigation.addListener('didFocus', async event => {
          this.getSellerSubscriptionPlan();
        });
      }
    });

    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        this.navigate(url);
      });
    } else {
      Linking.addEventListener('url', this.handleOpenURL);
    }
    let isIndication = await AsyncStorage.getItem('isIndication');
    isIndication = isIndication == null || isIndication == true ? true : false;
    this.setState({isIndication},()=>{
      this.props.blinkAction(this.state.isIndication);
    });
  }

  getSellerSubscriptionPlan() {
    const token = this.props.loginData.sData;
    if (!token) {
      return;
    }

    if (this.props.sellerType != '') {
      return;
    }

    const url = API_URL + '/get-seller-subscription-plan';

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        if (responseData && responseData.status == 200) {
          const {total_pin, used_pin, seller_type} = responseData.data;
          // console.warn('seller_type',seller_type)
          this.props.setSellerType(seller_type);
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleConnectivityChange,
    );
    // OneSignal.removeEventListener('opened', this.onOpened);
  }

  handleOpenURL = event => {
    // D
    this.navigate(event.url);
  };

  navigate = url => {
    if (!url) {
      return;
    }
    //url should follow "https://simsimmarket/pin/pin_name/user_id/pin_id"
    //https://simsimmarket/ad/ad_name/pin_id/pinName/ad_id
    const {navigate} = this.props.navigation;

    const token = 'no';
    const route = url.replace(/.*?:\/\//g, '');
    const appName = route.split('/');
    const routeName = appName[1];

    let item = {};

    if (routeName === 'pin') {
      item = {
        id: route.match(/\/([^\/]+)\/?$/)[1],
        pin_name: appName[2].split('_').join(' '),
        user_id: appName[3],
      };
      navigate('SellerContainer', {item, token});
    } else if (routeName === 'ad') {
      item = {
        id: route.match(/\/([^\/]+)\/?$/)[1],
        ad_name: appName[2].split('_').join(' '),
        pin_id: appName[3],
        pinName: appName[4].split('_').join(' '),
      };
      navigate('AdDetails', {item});
    }
  };

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({isConnected});
      //this.checkLocation()
    } else {
      this.setState({isConnected});
    }
  };
  closeIndication = async () => {
    await this.setState({isIndication: false}, () => {
      AsyncStorage.setItem('isIndication', JSON.stringify(false));
      this.props.blinkAction(this.state.isIndication);
    });
  };
  render() {
    console.warn('TCL', this.props.isRTL);
    const btnStyle = this.props.isRTL ? {left: 20} : {right: 21};
    const {isIndication} = this.state;
    return isIndication == true ? (
      //     <ImageBackground imageStyle={{height:height - 30, width:width}}	 source={require('../../images/indicator.png')} style={{height: height,width:width} } resizeMode='cover' >
      //  {/* <TouchableOpacity onPress={this.closeIndication} style={{position:'absolute', top:70, right:12}}>
      //           <Text style={{color: 'red', fontSize:30}}> X </Text>
      //         </TouchableOpacity>  */}
      //   </ImageBackground>
      this.props.isRTL ? (
        <TouchableOpacity
          onPress={this.closeIndication}
          style={{
            height: height,
            width: width,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={require('../../images/indicatorRight.png')}
            resizeMode="stretch"
            style={{height: '100%', width: '100%'}}
          />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={this.closeIndication}
          style={{
            height: height,
            width: width,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={require('../../images/indicator.png')}
            resizeMode="stretch"
            style={{height: '100%', width: '100%'}}
          />
        </TouchableOpacity>
      )
    ) : (
      <View style={styles.container}>
        <View
          style={{
            ...StyleSheet.absoluteFillObject,
            flex: 1,
            zIndex: Platform.OS === 'ios' ? 2 : 0,
          }}>
          <Maps {...this.props} />
          <View style={{marginTop: 45}}>
            <Tabs {...this.props} mapView={true} />
          </View>
          <FloatingButton mapView={true} {...this.props} />
          <SpecialOffers {...this.props} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  bottomImage: {
    position: 'absolute',
    bottom: 18,
  },
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...layoutActions, ...blinkingActions}, dispatch);
}

function mapStateToProps(state) {
  //console.warn("TCL: mapStateToProps -> state", state.network)

  return {
    isRTL: state.network.isRTL,
    sellerType: state.network.sellerType,
    loginData: state.loginReducer.loginUserData,
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomeMap);
