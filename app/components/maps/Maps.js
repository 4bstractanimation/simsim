import React, {Component} from 'react';
import {
  Text,
  TextInput,
  ScrollView,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  Animated,
  KeyboardAvoidingView,
  Alert,
  ActivityIndicator,
  AsyncStorage,
  View,
  BackHandler,
  DeviceEventEmitter,
  Platform,
} from 'react-native';
import PreLoader from './../PreLoader';
// import NoDataFound from './../NoDataFound';
import {Marker, PROVIDER_GOOGLE, MapKit} from 'react-native-maps';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import Geolocation from '@react-native-community/geolocation';
import MapView from 'react-native-map-clustering';
// import StarRating from 'react-native-star-rating';
// import CustomCallout from './CustomCallout';
// import TabView from './../TabView';
// import Header from './../Header';
// import SearchInput, { createFilter } from 'react-native-search-filter';
// import Profile from './../Profile';
import Filter from './../homeFilter';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
// import { strings, isRTL } from '../../../locales/i18n';
import * as layoutActions from '../../actions/layout';
import * as seachActions from '../../actions/Search';
import * as pinActions from '../../actions/pinColor';
import StyleSheetFactory from './../../styles/ListView';
import Icon from 'react-native-vector-icons/FontAwesome';
// import getCurrencyCode from './../Currency.js';
// import Modal from "react-native-simple-modal";
import DeviceInfo from 'react-native-device-info';
import LocationSwitch from 'react-native-location-switch';
import Toast from 'react-native-simple-toast';
import API_URL from '../../configuration/configuration';

// const KEYS_TO_FILTERS = ['title'];
const {width, height} = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 21.5996;
const LONGITUDE = 39.1664;
const LATITUDE_DELTA = 0.25;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const greenMarker = require('../../icons/greenMarker.png');
const blueMarker = require('../../icons/goldenMarker.png');
const grayMarker = require('../../icons/greyMarker.png');

// const grm = <Image source={greenMarker} style={{resizeMode: 'cover', width: 24, height: 35}} />
// const bm = <Image  source={blueMarker}  style={{resizeMode: 'cover', width: 34, height: 50,}} /> ;

// const greyMarker = <Image  source={grayMarker}  style={{resizeMode: 'cover', width: 23, height: 35}} />

// const grm = <Icon name="map-marker" size={40} color="green" />;
// const bm = <Icon name="map-marker" size={60} color="#ffbf00" />;
// const greyMarker = <Icon name="map-marker" size={40} color="gray" />;

const ym = <Icon name="map-marker" size={40} color="yellow" />;
let zoom = null;

class MapViewExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isClustering: true,
      modalVisible: false,
      region: {
        latitude: 21.5996,
        longitude: 39.1664,
        latitudeDelta: 0.25,
        longitudeDelta: LONGITUDE_DELTA,
        accuracy: '',
      },
      currentLocation: {
        latitude: null,
        longitude: null,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        accuracy: '',
      },
      ready: true,
      selectedMarkerIndex: [],
      Data: [],
      data: '',
      pinColorStatus: '',
      token: '',
      toastMessage: [],
      isToast: false,
      userId: null,
      zoom: null,
      subscriptionArray: [],
      deviceId: DeviceInfo.getDeviceId(),
      isMapNotLoading: false,
      toastLocation: {},
      newRegion: {},
      zoomLevel: null,
      isMapReady: false,
      targetPins: [],
    };
    this.getPinsStatus = false;
    this.userLoc = false;
    this.accessToken = '';
    this.zoom = '';
    //this.onRegionChange = this.onRegionChange.bind(this);
    this.getCurrentLocationPins = this.getCurrentLocationPins.bind(this);
    this.mapRef = null;
    this.setModalStatus = this.setModalStatus.bind(this);
    this.myLocation = {};
    this.delta = 0;
    this.callout = this.callout.bind(this);
    this.mapView;
  }

  // getMapData() {
  //   LocationSwitch.isLocationEnabled(
  //     () => {
  //       this.getlocation();
  //     },
  //     () => {
  //       this.getDefaultLocationPins();
  //     },
  //   );
  // }

  async componentDidMount() {
    const getUserData = await AsyncStorage.getItem('user_data');
    if (getUserData) {
      const userData = JSON.parse(getUserData);
      const token = userData.sData;
      this.setState({token: token, userId: userData.userId});
    }
    // console.warn('DeviceInfo.getDeviceId()',DeviceInfo.getDeviceId())
    this.getDefaultLocationPins();
    // Geocoder.init("AIzaSyBDZPn0qaarEZUlUJrNV9yvZn79UjPTR-Q");
    const {navigate} = this.props.navigation;
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('data--->>>',nextProps.filterData.targetPins.length,nextProps.filterData.allPins.length)
    if (nextProps.status === 'searchData' && nextProps.filterData) {
      if (
        nextProps.filterData &&
        nextProps.filterData.allPins !== this.props.filterData.allPins
      ) {
        function calculateDistance(lat1, lon1, lat2, lon2, unit) {
          var radlat1 = (Math.PI * lat1) / 180;
          var radlat2 = (Math.PI * lat2) / 180;
          var radlon1 = (Math.PI * lon1) / 180;
          var radlon2 = (Math.PI * lon2) / 180;
          var theta = lon1 - lon2;
          var radtheta = (Math.PI * theta) / 180;
          var dist =
            Math.sin(radlat1) * Math.sin(radlat2) +
            Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
          dist = Math.acos(dist);
          dist = (dist * 180) / Math.PI;
          dist = dist * 60 * 1.1515;
          if (unit == 'K') {
            dist = dist * 1.609344;
          }
          if (unit == 'N') {
            dist = dist * 0.8684;
          }
          return dist;
        }
        for (i = 0; i < nextProps.filterData.allPins.length; i++) {
          nextProps.filterData.allPins[i].distance = calculateDistance(
            nextProps.filterData.allPins[i].lat,
            nextProps.filterData.allPins[i].lng,
            this.state.region.latitude,
            this.state.region.longitude,
            'K',
          );
        }
        nextProps.filterData.allPins.sort(function(a, b) {
          return a.distance - b.distance;
        });
        const targetResult = nextProps.filterData.allPins.filter(
          item => item.distance < 10,
        );
        console.warn('aman', nextProps.filterData.targetPins.length);
        this.setState(
          {
            Data: nextProps.filterData.allPins,
            selectedMarkerIndex: [],
            isLoading: true,
            targetPins:
              nextProps.filterData.targetPins &&
              nextProps.filterData.targetPins.length > 0
                ? nextProps.filterData.targetPins
                : [nextProps.filterData.allPins[0]],
          },
          // ()=> this.setState({isLoading:true})
        );

        this.getPinsStatus = true;
      }
    }

    if (nextProps.pinId !== this.state.pinColorStatus) {
      this.setState({pinColorStatus: nextProps.pinId});
    }
  }

  // mapReloadHandler = () => {
  //   if(this.state.isLocationEnable) {
  //     this.getCurrentLocationPins();
  //   } else {
  //     this.getDefaultLocationPins()
  //   }

  // }

  /** Get user last location. If last location not available then get default location **/
  async getDefaultLocationPins() {
    console.warn('get defau;t location');
    const {isRTL} = this.props;
    this.setState({isLoading: false});
    const jsonData = await AsyncStorage.getItem('region');

    const lastLocation = jsonData && JSON.parse(jsonData);
    console.warn('defaultlocation1-->>', lastLocation);
    if (lastLocation && lastLocation.coords) {
      const lat = parseFloat(lastLocation.coords.latitude);
      const lng = parseFloat(lastLocation.coords.longitude);
      this.getToastlist(lat, lng);
      this.props.searchWithLocationAPI(
        this.state.userId,
        lat,
        lng,
        this.state.deviceId,
      );
      this.setState({
        region: {
          latitude: lat,
          longitude: lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
      });
    } else {
      fetch(API_URL + '/defaultLocation', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then(response => response.json())
        .then(responseData => {
          console.warn('checin g ;op,');
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الوصول لنقطة البيع'
              : 'Failed to get pin';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            const {lat, lng} = responseData.defaultlocation;
            const coords = {latitude: lat, longitude: lng};
            //console.warn('defaultlocation-->>',coords)
            AsyncStorage.setItem('region', JSON.stringify({coords}));

            this.props.searchWithLocationAPI(
              this.state.userId,
              lat,
              lng,
              this.state.deviceId,
            );
            this.setState(
              {
                region: {
                  latitude: parseFloat(lat),
                  longitude: parseFloat(lng),
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA,
                },
                isMapNotLoading: false,
              },
              // () => this.setState({ isLoading: true })
            );
            this.getToastlist(lat, lng);
          }
        })
        .catch(err => {
          console.warn('err message', err.message);
        });
    }
  }

  //get the user current location
  async getCurrentLocationPins(myLocation) {
    this.setState({isLoading: false});
    // console.warn('myLocation-->>',myLocation)
    const {latitude, longitude} = myLocation.coords;
    this.getToastlist(latitude, longitude);

    this.props.searchWithLocationAPI(
      this.state.userId,
      latitude,
      longitude,
      this.state.deviceId,
    );

    this.setState(
      {
        region: {
          latitude: latitude,
          longitude: longitude,
          latitudeDelta: 0.043,
          longitudeDelta: 0.034,
          // isLoading: false
        },
        isMapNotLoading: false,
      },
      // () => this.setState({ isLoading: true })
    );

    /** Toast messages will show only for login users **/
  }

  async getToastlist(lat, lng) {
    console.warn('lat ', lat, lng);
    if (!this.props.toast) {
      fetch(API_URL + '/get-toast-messages', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          lat: lat,
          lng: lng,
        }),
      })
        .then(response => response.json())
        .then(responseData => {
          console.warn('toastMessage-->>', responseData);
          if (
            responseData &&
            responseData.data &&
            responseData.data.length > 0
          ) {
            let coordinates;
            if (responseData) {
              coordinates = {
                latitude: parseFloat(responseData.data[0].lat),
                longitude: parseFloat(responseData.data[0].lng),
                longitudeDelta: 0.001,
                latitudeDelta: 0.0009,
              };
              // console.warn('coordinates->>',coordinates)
              this.setState(
                {
                  toastMessage: responseData.data,
                  isToast: true,
                  toastLocation: coordinates,
                },
                () => {
                  this.props.setToast(this.state.isToast);
                },
              );
            }
          }
        })
        .catch(err => {
          // console.warn('error:',err.message)
          Alert.alert('Error', err.message);
        });
    }
  }

  // componentWillUnmount() {
  //   navigator.geolocation.clearWatch(this.watchID);
  // }

  getPinColor(pinData) {
    const subscription = pinData.subscription_plan;
    const id = pinData.id;

    if (!subscription) {
      return;
    }

    let marker = {};

    if (this.state.selectedMarkerIndex.indexOf(id) > -1) {
      return grayMarker;
    }

    if (subscription.toLowerCase() === 'free') {
      marker = greenMarker;
    } else if (subscription.toLowerCase() === 'premium') {
      marker = greenMarker;
    } else {
      marker = blueMarker;
    }
    return marker;
  }

  countVisitors(pinid) {
    if (!pinid) {
      return;
    }
    const url = API_URL + '/no-of-visitor';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pinId: pinid,
        userId: this.state.userId,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('no-of-visitor-->',responseData);
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  onPressMarker(id, tId, data, toastMessageClicked) {
    // console.warn('onPressMarker....')

    if (toastMessageClicked) {
      this.checktoast(data.id, tId);
    }
    const arr = [...this.state.selectedMarkerIndex];
    arr.push(data.id);
    this.setState({isToast: false, selectedMarkerIndex: arr});
    this.props.pinActions(data.id);
    this.callout(data);
    this.countVisitors(data.id);
  }

  async callout(item) {
    const {navigate} = this.props.navigation;
    navigate('SellerContainer', {item, map: true});
    const token = this.state.token;
    if (token) {
      fetch(API_URL + '/create-pin-history-per-click', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          pinId: item.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          //console.warn('create-pin-history', responseData)
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 236486
          ) {
            // console.log('Failed to add history');
          } else if (
            responseData.status == 805465 &&
            responseData.sData === null
          ) {
            //console.log('Successfully added to history');
          }
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
  }

  setModalStatus(status) {
    this.setState({modalVisible: status});
  }

  async checktoast(id, tId) {
    //console.warn('checktoast-->>',id, tId)

    const url = API_URL + '/update-toast-messages';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pinId: id,
        toastId: tId,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('update-toast-messages res-->>', responseData)
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
    AsyncStorage.setItem('toastId', String(tId));
  }

  Capitalize(str) {
    if (!str) {
      return;
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  getlocation() {
    Geolocation.getCurrentPosition(
      async position => {
        // console.warn('position-->>', position);
        await AsyncStorage.setItem('region', JSON.stringify(position));
        const lat = parseFloat(position.coords.latitude);
        const lng = parseFloat(position.coords.longitude);
        this.props.searchWithLocationAPI(
          this.state.userId,
          lat,
          lng,
          this.state.deviceId,
        );
      },
      error => {
        // Alert.alert('Error',error.message);
        this.getDefaultLocationPins();
      },
      {enableHighAccuracy: false, timeout: 50000, maximumAge: 1000},
    );
  }

  // enableUserLocation() {
  //   LocationSwitch.enableLocationService(
  //     1000,
  //     true,
  //     () => {
  //       this.getlocation();
  //       // this.setState({ isLoading: false });
  //     },
  //     () => {
  //       // Alert.alert('Error','Location could not be enabled.')
  //     },
  //   );
  // }
  enableUserLocation = async () => {
    if (Platform.OS === 'android') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then(async data => {
          if (data == 'enabled') {
            this.setState({isLoading: false, Data: []});
            await this.getlocation();
          }
        })
        .catch(async err => {
          if (err.code == 'ERR00') {
            await this.hideLocationBoxHandler();
          }
        });
    }
  };
  locationButtonPress = async () => {
    LocationSwitch.isLocationEnabled(
      () => {
        this.setState({isLoading: false, Data: []});
        this.getlocation();
      },
      () => {
        const msg1 = this.props.isRTL ? 'تشغيل محدد المواقع' : 'Use Location ?';
        const msg2 = this.props.isRTL
          ? 'لإظهار العروض الخاصة ونقاط البيع حول موقعك، يرجى تشغيل محدد المواقع GPS'
          : 'To show offers and sales point around you, turn on GPS.';
        const not = this.props.isRTL ? 'ليس الآن' : 'Not Now';
        const enable = this.props.isRTL ? 'تفعيل' : 'Enable';

        Alert.alert(
          msg1,
          msg2,
          [
            {
              text: not,
              onPress: () => {
                //this.hideLocationBoxHandler();
              },
            },
            {
              text: enable,
              onPress: () => {
                this.enableUserLocation();
              },
            },
          ],
          {cancelable: false},
        );
      },
    );
    // const jsonData =  await AsyncStorage.getItem('region');
    //  const lastLocation = JSON.parse(jsonData);
    // const region = {
    //   latitude: Number(lastLocation.coords.latitude),
    //   longitude: Number(lastLocation.coords.longitude),
    //   latitudeDelta: LATITUDE_DELTA,
    //   longitudeDelta: LONGITUDE_DELTA
    // }

    // if(this.mapView) {
    //   this.mapView._root.animateToRegion(region, 1000);
    // }
  };

  salesPointLocator() {
    this.getPinsStatus = false;
    const markers = this.state.targetPins;
    console.warn('markers', markers);
    let arraysOfMarkers = markers.map(marker => ({
      latitude: marker.lat,
      longitude: marker.lng,
    }));
    arraysOfMarkers.push({
      latitude: this.state.region.latitude,
      longitude: this.state.region.longitude,
    });
    console.warn('ab', arraysOfMarkers);
    if (arraysOfMarkers.length && this.mapView && this.mapView._root) {
      const region = this.getRegionForCoordinates(arraysOfMarkers);
      this.mapView._root.animateToRegion(region, 1000);
      setTimeout(() => {
        this.setState({isToast: false});
      }, 2000);
    }
  }

  animate(coordinates, markers) {
    const arraysOfMarkers = markers.map(marker => marker.item.props.coordinate);
    const region = this.getRegionForCoordinates(arraysOfMarkers);
    if (this.mapView) {
      this.mapView._root.animateToRegion(region, 1000);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.mapView &&
      this.state.isToast &&
      this.state.isMapReady &&
      this.state.Data.length
    ) {
      setTimeout(() => {
        this.mapView._root.animateToRegion(this.state.toastLocation, 1000);
      }, 1000);
    }

    if (
      this.getPinsStatus &&
      this.state.Data.length &&
      !this.state.isToast &&
      this.state.isMapReady
    ) {
      setTimeout(() => {
        this.salesPointLocator();
      }, 2000);
    }
  }

  getRegionForCoordinates(points) {
    let minX, maxX, minY, maxY;

    // init first point
    (point => {
      minX = point.latitude;
      maxX = point.latitude;
      minY = point.longitude;
      maxY = point.longitude;
    })(points[0]);

    // calculate rect
    points.map(point => {
      minX = Math.min(minX, point.latitude);
      maxX = Math.max(maxX, point.latitude);
      minY = Math.min(minY, point.longitude);
      maxY = Math.max(maxY, point.longitude);
    });

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = maxX - minX;
    const deltaY = maxY - minY;

    return {
      latitude: midX,
      longitude: midY,
      latitudeDelta: deltaX,
      longitudeDelta: deltaY * 2,
    };
  }

  // getCluster(subscription) {
  //   let cluster = false;

  //   if (subscription === 'Free') {
  //     cluster = true;
  //   } else if (subscription === 'Premium') {
  //     cluster = true;
  //   } else if (subscription === 'Platinum') {
  //     cluster = true;
  //   }
  //   return cluster;
  // }

  closeToastMessage = () => {
    this.setState({isToast: false});
  };

  onMapLayout = () => {
    this.setState({isMapReady: true});
  };

  render() {
    console.warn('hgh', this.state.toastMessagea);
    let pinId = '';
    let toastTitle = '';
    let toastDesc = '';
    let toastImg = '';
    let toastId = '';

    if (this.state.toastMessage && this.state.toastMessage.length) {
      pinId = this.state.toastMessage[0].pin_id;
      toastTitle = this.state.toastMessage[0].title;
      toastDesc = this.state.toastMessage[0].description;
      toastImg = this.state.toastMessage[0].upload_media;
      toastId = this.state.toastMessage[0].id;
    }

    let pinData, pinIndex;

    const {region, markers} = this.state;
    const filterStyle = {right: 28};
    const gm = <Icon name="map-marker" size={40} color="gray" />;
    // console.warn('region-->>',region,typeof region.latitude)
    return (
      <View style={styles.container}>
        {!this.state.isLoading && !this.state.Data.length ? (
          <PreLoader />
        ) : (
          <MapView
            ref={ref => (this.mapView = ref)}
            provider={PROVIDER_GOOGLE}
            clustering={this.state.isToast ? false : true}
            onClusterPress={(coordinates, markers) =>
              this.animate(coordinates, markers)
            }
            //   initialRegion={{latitude: 52.5, longitude: 19.2,
            //  latitudeDelta: 8.5, longitudeDelta: 8.5}}
            // region={{
            //   latitude: LATITUDE,
            //   longitude: LONGITUDE,
            //   latitudeDelta: LATITUDE_DELTA,
            //   longitudeDelta: LONGITUDE_DELTA,
            // }}
            initialRegion={this.state.region}
            style={styles.map}
            showsUserLocation={true}
            followUserLocation={true}
            zoomEnabled={true}
            ScrollEnabled={true}
            showsBuildings={true}
            showsMyLocationButton={false}
            loadingEnabled={true}
            radius={5}
            // maxZoomLevel={10}
            // onRegionChangeComplete={this.onRegionChangeComplete}
            onLayout={this.onMapLayout}>
            {this.state.Data.length > 0 &&
              this.state.Data.map((data, i) => {
                // console.warn('toast-->>',this.state.isToast,pinId, data.id)
                if (pinId === data.id) {
                  pinData = data;
                  pinIndex = i;
                }
                return (
                  <Marker
                    // cluster={this.getCluster(data.subscription_plan)}
                    cluster={true}
                    onPress={() => this.onPressMarker(i, toastId, data)}
                    // onCalloutPress={() => {console.warn("calout Press")} }
                    // onSelect={this.markerPressed}
                    key={data.id}
                    coordinate={{latitude: data.lat, longitude: data.lng}}
                    image={this.getPinColor(data)}>
                    <View style={{marginTop: 10}}>
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 12,
                          marginBottom: 45,
                          fontWeight: 'bold',
                        }}>
                        {this.Capitalize(data.name_on_map)}
                      </Text>
                    </View>
                  </Marker>
                );
              })}
          </MapView>
        )}

        {this.state.isToast &&
          this.state.isMapReady &&
          this.state.Data.length > 0 && (
            <TouchableOpacity
              style={styles.toastContainer}
              onPress={() =>
                this.onPressMarker(pinIndex, toastId, pinData, true)
              }>
              <View style={{flex: 1, margin: 10}}>
                {toastImg && (
                  <Image
                    style={{resizeMode: 'contain', width: 100, height: 80}}
                    source={{uri: toastImg}}
                  />
                )}
              </View>
              <View style={[styles._textBox, {margin: 10}]}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={[styles._title, {color: '#cc0000', fontSize: 16}]}>
                    {this.Capitalize(toastTitle)}
                  </Text>
                </View>
                <Text style={styles._description}>
                  {this.Capitalize(toastDesc)}
                </Text>
              </View>
              <TouchableOpacity
                onPress={this.closeToastMessage}
                style={styles.closeToast}>
                <Text style={{color: 'red', fontSize: 20}}> X </Text>
              </TouchableOpacity>
            </TouchableOpacity>
          )}

        <View style={[styles.filterButton, filterStyle]}>
          <TouchableOpacity
            onPress={() => {
              this.setModalStatus(true);
            }}>
            <Image
              style={{width: 55, height: 55}}
              source={require('../../icons/filter1x.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={[styles.bottomImage, {right: 25}]}>
          <TouchableOpacity onPress={this.locationButtonPress}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../../icons/location3x.png')}
            />
          </TouchableOpacity>
        </View>
        <Filter
          modalVisible={this.state.modalVisible}
          setModalStatus={this.setModalStatus}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  _calloutContainer: {
    flex: 1,
    justifyContent: 'space-between',
    width: 300,
  },
  markerWrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  _searchText: {
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    width: 250,
    height: 50,
    backgroundColor: 'white',
    marginTop: 10,
    textAlign: 'center',
    marginLeft: 10,
    marginBottom: 20,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
  },
  _title: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  _messageBox: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 10,
    alignItems: 'center',
    padding: 0,
    margin: 5,
    backgroundColor: '#FFFFFF',
  },
  _image: {
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
  _textBox: {
    flex: 2,
  },
  _descriptionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  _description: {
    fontFamily: 'System',
    fontSize: 12,
    color: '#000000',
    flexWrap: 'wrap',
    marginRight: 10,
  },
  bottomImage: {
    position: 'absolute',
    borderRadius: 64,
    borderWidth: 10,
    borderColor: '#00AADB',
    backgroundColor: '#00AADB',
    bottom: 35,
    marginRight: 10,
    marginLeft: 10,
  },
  filterButton: {
    bottom: 75,
    position: 'absolute',
  },
  toastContainer: {
    width: width - 40,
    height: 100,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    // marginHorizontal: 7,
    zIndex: 10,
    backgroundColor: '#FFF',
    borderRadius: 10,
    // marginTop: height - 900
    marginBottom: 250,
    // backgroundColor: '#ebebe0',
  },
  closeToast: {
    borderRadius: 50,
    padding: 10,
    paddingTop: 5,
    paddingRight: 5,
  },
});

function mapStateToProps(state) {
  return {
    filterData: state.search.filterData,
    isRTL: state.network.isRTL,
    status: state.search.status,
    pinId: state.pin.selectedMarkerIndex,
    modalOpen: state.search.modalOpen,
    offerData: state.search.offerData,
    toast: state.search.toast,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {...layoutActions, ...seachActions, ...pinActions},
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MapViewExample);
