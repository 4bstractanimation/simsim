import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
  TextInput,
  Image,
  // ListView,
  StyleSheet,
  TouchableOpacity,
  Platform,
  AsyncStorage,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import DeviceInfo from 'react-native-device-info';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as layoutActions from '../../actions/layout';
import * as seachActions from '../../actions/Search';
import * as notificationAction from '../../actions/notificationAction';
import API_URL from '../../configuration/configuration';
import database from '@react-native-firebase/database';
// var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class HomePageHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      emptyValue: false,
      SearchPins: [],
      token: '',
      searchedKeyword: [],
      keyword: [],
      searchText: false,
      notification_count: null,
      latitude: '',
      longitude: '',
      isIndication: true,
      userId: '',
    };
    this.mapView = true;
  }

  async onChangetext(text) {
    if (text.length === 0) {
      this.setState({search: text}, () => {
        this.searchData();
      });
      return;
    }
    this.setState({search: text});
    var searchedKeyword = this.state.keyword.filter(function(word) {
      return word.keyword.toLowerCase().indexOf(text.toLowerCase()) > -1;
    });

    this.setState({searchedKeyword: searchedKeyword});
  }

  searchData = async () => {
    try {
      if (this.state.search !== '') {
        database()
          .ref('KEYS/KEY/' + this.state.search + '/total')
          .transaction(total => {
            return (total || 0) + 1;
          });

        database()
          .ref(
            'KEYS/KEY/' +
              this.state.search +
              '/date/' +
              new Date().toDateString() +
              '/total',
          )
          .transaction(total => {
            return (total || 0) + 1;
          });
      }
    } catch (e) {}

    // const lat =
    // 	this.props.location !== undefined &&
    // 	this.props.location.latitude !== undefined
    // 		? this.props.location.latitude
    // 		: '';
    // const long =
    // 	this.props.location !== undefined &&
    // 	this.props.location.longitude !== undefined
    // 		? this.props.location.longitude
    //
    //		: '';

    const {latitude, longitude} = this.state;

    const deviceId = this.mapView ? 'map' : 'listing';

    this.props.searchWithLocationAPI(
      this.state.userId,
      this.state.latitude,
      this.state.longitude,
      deviceId,
      this.state.search,
    );
  };

  searchHistory() {
    fetch(API_URL + '/search-history', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        this.setState({keyword: responseData.searchHistory});
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  async componentDidMount() {
    AsyncStorage.getItem('user_data').then(json => {
      if (json) {
        const userData = JSON.parse(json);
        this.setState({
          token: userData.sData,
          userId: userData.userId,
        });
        this.props.getData(userData.sData);
      }
    });
    AsyncStorage.getItem('region').then(json => {
      if (json) {
        const region = JSON.parse(json);
        this.setState({
          latitude: region.coords.latitude,
          longitude: region.coords.longitude,
        });
      }
    });
    this.searchHistory();
    let isIndication = await AsyncStorage.getItem('isIndication');
    console.warn('osindic', isIndication);
    isIndication = isIndication == null || isIndication == true ? true : false;
    this.setState({isIndication});
  }

  renderAdress = word => (
    <TouchableOpacity
      onPress={() => this.setState({search: word.keyword, searchText: false})}>
      <View style={{backgroundColor: '#FFF'}}>
        <Text style={{padding: 10}}>{word.keyword}</Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    const notificationCount = this.props.notifCount;
    const isRTL = this.props.isRTL;
    const {navigate, state} = this.props.navigation;
    const searchPin = this.state.SearchPins;
    console.warn(
      'trhis',
      this.state.isIndication === true,
      this.props.blinkingImage === true,
    );
    return this.state.isIndication === true &&
      this.props.blinkingImage === true ? null : (
      <View style={styles.containerTransparent}>
        <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
          <TouchableOpacity
            style={{marginTop: 5}}
            onPress={() => {
              navigate('DrawerToggle');
            }}>
            <Image
              style={{
                marginTop: 0,
                width: 28,
                height: 28,
                resizeMode: 'contain',
              }}
              source={require('../../icons/toggle.png')}
            />
          </TouchableOpacity>
          <View
            style={[
              styles._textbox,
              {
                flex: 1,
                flexDirection: isRTL ? 'row-reverse' : 'row',
                marginHorizontal: 20,
              },
            ]}>
            <View
              style={[
                styles._textbox,
                {
                  width: '85%',
                  height: Platform.OS === 'ios' ? '60%' : '90%',
                  marginTop: Platform.OS === 'ios' ? 15 : 0,
                  marginLeft: Platform.OS === 'android' ? 5 : 0,
                  flex: 2,
                },
              ]}>
              {/*
							<TextInput

								value={this.state.search}
								onChangeText={text => this.onChangetext(text)}
								keyboardType={'default'}
								underlineColorAndroid="white"
								returnKeyType='go'
								// onFocus={() =>
								// 	this.setState({ searchText: true },() => {
								// 		this.props.getAllPins();
								// 		this.props.setSelectedcategory({});
								// 	})
								// }
								// onBlur={()=> this.setState({searchText: false})}
								style={{ textAlign: isRTL ? 'right' : null }}
								onSubmitEditing={this.searchData}
							/>
							*/}
              <TextInput
                value={this.state.search}
                onChangeText={text => this.onChangetext(text)}
                keyboardType={'default'}
                underlineColorAndroid="white"
                textAlignVertical="top"
                returnKeyType={'done'}
                style={{
                  height: Platform.OS === 'ios' ? 30 : null,
                  textAlign: this.props.isRTL ? 'right' : null,
                }}
                onSubmitEditing={this.searchData}
              />
            </View>
            <TouchableOpacity onPress={this.searchData}>
              <Image
                style={{
                  width: 17,
                  height: 17,
                  marginVertical: 15,
                  marginHorizontal: 10,
                  resizeMode: 'contain',
                }}
                source={require('../../icons/search3x.png')}
              />
            </TouchableOpacity>
          </View>
          {this.props.viewType === 'map' ? (
            <TouchableOpacity
              style={{marginHorizontal: 0}}
              onPress={() => {
                this.mapView = false;
                this.props.setSelectedcategory({});
                this.props.getAllPins();
                navigate('ListView');
              }}>
              <Image
                style={{
                  marginTop: 5,
                  width: 28,
                  height: 28,
                  resizeMode: 'contain',
                }}
                source={require('../../icons/information3x.png')}
              />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                this.mapView = true;
                this.props.setSelectedcategory({});
                this.props.getAllPins();
                navigate('Home', {...this.props});
              }}>
              <Image
                style={{
                  marginTop: 5,
                  width: 28,
                  height: 28,
                  resizeMode: 'contain',
                }}
                source={require('../../icons/location.png')}
              />
            </TouchableOpacity>
          )}

          {this.state.token ? (
            <TouchableOpacity
              style={{
                marginLeft: 15,
                marginRight: isRTL ? 15 : 0,
              }}
              onPress={() => {
                this.props.setSelectedcategory({});
                navigate('Notification');
              }}>
              <View style={{flex: 1}}>
                {notificationCount ? (
                  <View style={styles.notification}>
                    <Text
                      style={{
                        marginHorizontal: 0,
                        top: 0,
                        color: 'white',
                        fontSize: 10,
                        textAlign: 'center',
                      }}>
                      {notificationCount}
                    </Text>
                  </View>
                ) : null}
                <Image
                  style={{
                    marginTop: notificationCount ? -15 : 5,
                    width: 28,
                    height: 28,
                    marginRight: 0,
                    resizeMode: 'contain',
                  }}
                  source={require('../../icons/alert3x.png')}
                />
              </View>
            </TouchableOpacity>
          ) : null}
        </View>
        {this.state.searchText && this.state.search ? (
          // <ListView
          // 	style={{ marginLeft: 60, marginRight: 140 }}
          // 	dataSource={ds.cloneWithRows(
          // 		this.state.searchedKeyword
          // 	)}
          // 	renderRow={this.renderAdress}
          // />
          <Text>List View</Text>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerTransparent: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 20 : 10,
    marginBottom: Platform.OS === 'ios' ? 15 : 10,
    marginBottom: 10,
    marginHorizontal: 15,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  container: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 10,
    backgroundColor: 'red',
    position: 'absolute',
    top: 0,
  },
  _textbox: {
    borderRadius: 30,
    height: 43,
    backgroundColor: 'white',
  },
  _textboxText: {
    marginLeft: 20,
    paddingTop: 10,
  },
  searchBar: {
    flex: 1,
    marginHorizontal: 10,
    backgroundColor: '#fff',
    borderRadius: 25,
    width: '40%',
    paddingHorizontal: 15,
  },
  notification: {
    borderRadius: 64,
    width: 20,
    height: 20,
    backgroundColor: '#00ADDE',
    marginLeft: 15,
    zIndex: 2,
  },
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    location: state.search.location,
    status: state.search.status,
    notifCount: state.notification.notificationCount,
    blinkingImage: state.blinkingReducer.blinkingImage,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {...layoutActions, ...seachActions, ...notificationAction},
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePageHeader);
