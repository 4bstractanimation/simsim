import React from 'react';

import {Text, View, TextInput, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Header = ({navigation}) => {
  const title = 'Drawer';
  const open = navigation.state.index;
  const header = (
    <View style={{flex: 1, flexDirection: 'row'}}>
      <TouchableOpacity
        style={{marginHorizontal: 10, marginTop: 5}}
        onPress={() => {
          navigation.navigate('DrawerToggle');
        }}>
        <Image
          style={{width: 32, height: 32, backgroundColor: 'white'}}
          source={require('../icons/manu-icon.png')}
        />
      </TouchableOpacity>
    </View>
  );

  const myIcon = (
    <Icon
      onPress={() => {
        alert('Language change not implemented');
      }}
      style={{marginLeft: 10}}
      name="language"
      size={30}
      color="#000000"
    />
  );
  const bellIcon = (
    <Icon
      onPress={() => {
        alert('Notification...');
      }}
      style={{marginRight: 10}}
      name="bell"
      size={30}
      color="#000000"
    />
  );
  const headerPostion = open == 0 ? 'headerLeft' : 'headerRight';
  return {
    headerLeft: open == 1 ? myIcon : header,
    headerRight: open == 1 ? header : bellIcon,
  };
};

export default Header;
