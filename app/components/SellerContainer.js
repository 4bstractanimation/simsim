import React, {Component} from 'react';
import {
  View,
  Text,
  // Button,
  // Dimensions,
  Image,
  StyleSheet,
  // TouchableHighlight,
  TouchableOpacity,
  AsyncStorage,
  Alert,
} from 'react-native';
// import {Rating, AirbnbRating} from 'react-native-ratings';
import SellerAdsTab from './SellerAdsTab';
// import Footer from './Footer';
import SellerAds from './SellerAds';
import SellerDetails from './SellerDetails';
import NewsFeed from './NewsFeed';
// import AddNewAd from './AddNewAd';
// import SocialSharing from './SocialSharing';
import FooterContainer from './FooterContainer';
import NotificationHeader from './NotificationHeader';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';
import API_URL from '../configuration/configuration';
import database from '@react-native-firebase/database';

// const {width, height} = Dimensions.get('window');

// var take;

class SellerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 1,
      isfavourite: false,
      token: '',
      toggle: false,
    };

    // this.handleFavourite = this.handleFavourite.bind(this);
    this.addFavourite = this.addFavourite.bind(this);
    this.removefavourite = this.removefavourite.bind(this);
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle: (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            marginTop: 10,
            marginRight: 10,
          }}>
          {navigation.state.params.item.pin_name}
        </Text>

        <TouchableOpacity
          onPress={function() {
            // console.warn('-->>',)
            function userNotLoggedIn() {
              const {navigate} = navigation;

              Alert.alert(
                '',
                'You need to login',
                [
                  {text: 'Login', onPress: () => navigate('Login')},
                  {text: 'SignUp', onPress: () => navigate('SignUp')},
                  {text: 'Cancel'},
                ],
                {cancelable: false},
              );
            }
            if (navigation.state.params.token) {
              navigation.state.params.handleSave();
            } else {
              userNotLoggedIn();
            }
          }}>
          {navigation.state.params.isfavourite ? (
            <Image
              resizeMode="cover"
              style={{
                width: 30,
                height: 50,
                resizeMode: 'contain',
              }}
              source={require('./../icons/favourite2x.png')}
            />
          ) : (
            <Image
              resizeMode="cover"
              style={{
                width: 30,
                height: 50,
                resizeMode: 'contain',
              }}
              source={require('./../icons/beforefavourite.png')}
            />
          )}
        </TouchableOpacity>
      </View>
    ),
    headerRight: <NotificationHeader navigation={navigation} />,
  });

  async componentDidMount() {
    // console.warn('didmount  --  isfavourite  -->>', this.props.navigation.state.params.item.isfavourite);
    // console.warn('-->>',this.props.navigation.state.params.item);
    let isfavourite = Boolean(
      this.props.navigation.state.params.item.isfavourite,
    );
    // console.warn("TCL: componentDidMount -> isfavourite", isfavourite)
    this.setState({isfavourite});
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    if (userData) {
      this.props.navigation.setParams({token: userData.sData});
      this.setState({token: userData.sData, userId: userData.userId});
    }
    if (this.state.isfavourite) {
      this.props.navigation.setParams({
        handleSave: this.removefavourite,
        isfavourite: true,
      });
    } else {
      this.props.navigation.setParams({
        handleSave: this.addFavourite,
        isfavourite: false,
      });
    }
  }

  addFavourite() {
    const {isRTL} = this.props;
    if (this.state.token) {
      fetch(API_URL + '/add-favorite', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: this.state.token,
        },
        body: JSON.stringify({
          pinId: this.props.navigation.state.params.item.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          //console.warn("TCL: addFavourite -> responseData", responseData)
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الإضافة لقائمة المفضلة  '
              : 'Failed to add in favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            this.setState({isfavourite: true});
            const message = isRTL
              ? 'تم الإضافة بنجاح لقائمة المفضلة '
              : 'Added in favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
            this.props.navigation.setParams({
              handleSave: this.removefavourite,
              isfavourite: true,
            });
            database()
              .ref(
                'SHOPS/SHOP/' +
                  this.props.navigation.state.params.item.pin_name +
                  '/favorite',
              )
              .transaction(total => {
                return (total || 0) + 1;
              });

            // DATE UPDATE

            database()
              .ref(
                'SHOPS/SHOP/' +
                  this.props.navigation.state.params.item.pin_name +
                  '/date/' +
                  new Date().toDateString() +
                  '/favorite',
              )
              .transaction(total => {
                return (total || 0) + 1;
              });

            database()
              .ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/favorite')
              .transaction(total => {
                return (total || 0) + 1;
              });
          }
        })
        .catch(err => {
          const errorText = isRTL ? 'خطأ' : 'Error';
          Alert.alert(errorText, err.message);
        });
    }
  }

  async removefavourite() {
    const token = this.state.token;
    const {isRTL} = this.props;
    if (token) {
      fetch(API_URL + '/remove-favorite', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          pinId: this.props.navigation.state.params.item.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          //console.warn('res-->',responseData);
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الازالة من قائمة المفضلة '
              : 'Failed to remove from favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            this.setState({isfavourite: false});
            const message = isRTL
              ? ' تمت الازالة من قائمة المفضلة '
              : 'Removed from favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
            this.props.navigation.setParams({
              handleSave: this.addFavourite,
              isfavourite: false,
            });
          }
        })
        .catch(err => {
          const errorText = isRTL ? 'خطأ' : 'Error';
          Alert.alert(errorText, err.message);
        });
    }
  }

  setTabIndex = val => {
    this.setState({tabIndex: val});
  };

  getTabComponent = val => {
    switch (val) {
      case 1:
        return (
          <SellerAds
            {...this.props}
            map={this.props.navigation.state.params.map}
          />
        );
      case 2:
        return <SellerDetails {...this.props} />;
      case 3:
        return <NewsFeed {...this.props} />;
    }
  };

  toggleVisible = () => {
    this.setState(prevState => {
      return {toggle: !prevState.toggle};
    });
  };

  render() {
    console.warn('pin-->>', this.props);
    const {
      id,
      pin_name,
      user_id,
      description,
    } = this.props.navigation.state.params.item;
    return (
      <View style={{flex: 1}}>
        <View style={styles.container}>
          <SellerAdsTab
            {...this.props}
            setTabIndex={this.setTabIndex}
            tabIndex={this.state.tabIndex}
          />
          {this.getTabComponent(this.state.tabIndex)}
        </View>
        <FooterContainer
          {...this.props}
          sharingInfo={{
            sharingObject: 'pin',
            id,
            pin_name,
            user_id,
            description,
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf2f9',
  },
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

export default connect(mapStateToProps)(SellerContainer);
