import React, { Component } from 'react';
import { 
	View, 
	Text,
	TouchableOpacity,
	StyleSheet,
	Picker, 
	Alert,
	AsyncStorage,
	Platform,
} from 'react-native';
import Tabs from './TabView';
import RNPickerSelect from 'react-native-picker-select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AddOffer';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class AddOffer extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      return {
        title: state.params.isRTL === true ? 'منبه العروض' : 'Add Offer Alarm',
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      categoryList: [],
      category: null,
      subCategoryList: [],
      subCategory: null,
      subSubCategoryList: [],
      subSubCategory: null,
    };
  }

  componentDidMount() {
    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
    const isRTL = this.props.isRTL;
    fetch(API_URL + '/get-category', {
      //Category select from here which is dynamic data
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn("TCL: responseData ", responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول الى الفئات'
            : 'Failed to get category';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          for (let i = 0; i < responseData.categoryList.length; i++) {
            responseData.categoryList[i].value =
              responseData.categoryList[i].id;
            responseData.categoryList[i].label = this.props.isRTL
              ? responseData.categoryList[i].arb_name
              : responseData.categoryList[i].name;
            responseData.categoryList[i].color = '#7A7A7A';
          }
          this.setState({categoryList: responseData.categoryList})
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }
  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  selectedCategory(value) {
    this.setState({category: value});
    if (value) {
      const url = API_URL + '/get-sub-category?cid=' + value;
      fetch(url, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
        .then(response => response.json())
        .then(responseData => {
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الوصول الى الفئات الفرعية'
              : 'Failed to get sub-category';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            const subcat = responseData.subCategoryList.filter(
              item => item.name !== 'Special Offers',
            );
            for (let i = 0; i < subcat.length; i++) {
              subcat[i].value = subcat[i].id;
              subcat[i].label = this.props.isRTL
                ? subcat[i].arb_name
                : subcat[i].name;
              subcat.color = 'grey';
            }
            this.setState({subCategoryList: subcat});
          }
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
  }

  async applyPromotion() {
    const {navigate, goBack} = this.props.navigation;
    if (!this.state.category || !this.state.subCategory) {
      const message = this.props.isRTL
        ? 'كلا الفئة والفئة الفرعية إلزامية'
        : 'Both Category and Sub Category are mandatory';
      Alert.alert('', message);
      return;
    }
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    fetch(API_URL + '/addOfferAlert', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        catId: this.state.category,
        subCatId: this.state.subCategory,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('add alarm-->',responseData);
        if (responseData.status == 654864) {
          if (responseData.eData === null) {
            const errMsg = responseData.eMsg;
            if (this.props.isRTL) {
              Alert.alert(
                '',
                'تم اختيار هذه الفئة سابقا، يرجى اختيار فئة أخرى',
              );
              return;
            }
            Alert.alert('', errMsg);
          } else if (responseData.eData.eCode == 545864) {
            const errMsg = responseData.eMsg;
            Alert.alert('', errMsg);
          }
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          this.props.navigation.goBack();
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  render() {
    // console.warn(this.state.subCategory, this.state.category)
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {navigate} = this.props.navigation;
    const isRTL = this.props.isRTL;
    //console.warn("TCL: selectedCategory -> ", this.state.subCategory)
    return (
      <View style={styles.container}>
        <View style={[styles.category]}>
          {Platform.OS === 'android' ? (
            <Picker
              style={{flex: 1, marginHorizontal: 10, color: 'black'}}
              selectedValue={this.state.category}
              onValueChange={(itemValue, itemIndex) =>
                this.selectedCategory(itemValue)
              }>
              <Picker.Item label={strings('adNewAd.category')} value="" />
              {this.state.categoryList.map((item, key) => {
                // console.warn('item',item)
                return (
                  <Picker.Item
                    key={key}
                    label={isRTL ? item.arb_name : this.Capitalize(item.name)}
                    value={item.id}
                  />
                );
              })}
            </Picker>
          ) : (
            <View style={{left: 20, top: 15, color: 'black'}}>
              <RNPickerSelect
                placeholder={{
                  label: strings('adNewAd.category'),
                  value: null,
                }}
                value={this.state.category}
                onValueChange={(itemValue, _index) =>
                  this.selectedCategory(itemValue)
                }
                style={{
                  color: 'black',
                  placeholderColor: 'black',
                  inputIOS: {color: 'black', placeholderColor: 'black'},
                }}
                hideIcon
                items={this.state.categoryList}
              />
            </View>
          )}
        </View>
        <View style={[styles.category]}>
          {Platform.OS === 'android' ? (
            <Picker
              style={{flex: 1, marginHorizontal: 10, color: 'black'}}
              selectedValue={this.state.subCategory}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({subCategory: itemValue})
              }>
              <Picker.Item label={strings('adNewAd.sub_category')} value="" />
              {this.state.subCategoryList.map((item, key) => (
                <Picker.Item
                  key={key}
                  label={isRTL ? item.arb_name : this.Capitalize(item.name)}
                  value={item.id}
                />
              ))}
            </Picker>
          ) : (
            <View style={{left: 20, top: 15}}>
              {this.state.subCategoryList.length ? (
                <RNPickerSelect
                  placeholder={{
                    label: strings('adNewAd.sub_category'),
                    value: null,
                  }}
                  value={this.state.subCategory}
                  onValueChange={(itemValue, index) =>
                    this.setState({subCategory: itemValue})
                  }
                  style={{
                    color: 'black',
                    placeholderColor: 'black',
                    inputIOS: {color: 'black', placeholderColor: 'black'},
                  }}
                  hideIcon
                  items={this.state.subCategoryList}
                />
              ) : (
                <Text>{strings('adNewAd.noCategory')}</Text>
              )}
            </View>
          )}
        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <View style={styles.Button}>
              <Text style={styles.text}>{isRTL ? 'الغاء' : 'Cancel'}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.applyPromotion.bind(this)}>
            <View style={styles.Button}>
              <Text style={styles.text}>{isRTL ? 'أرسل' : 'Apply'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddOffer);
