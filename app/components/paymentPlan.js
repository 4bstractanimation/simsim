import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
  AsyncStorage,
  // WebView,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Subscription';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import getCurrencyValue from './getCurrencyValue.js';
import Modal from 'react-native-modalbox';
import moment from 'moment';
import API_URL, {WEB_URL} from '../configuration/configuration';
import RNIap, {
  Product,
  ProductPurchase,
  acknowledgePurchaseAndroid,
  purchaseUpdatedListener,
  purchaseErrorListener,
  PurchaseError,
} from 'react-native-iap';

const items = Platform.select({
  ios: [
    'com.premium.3for1',
    'com.premium.3for6',
    'com.premium.3for12',
    'com.premium.7for1',
    'com.premium.7for6',
    'com.premium.7for12',
    'com.premium.10for1',
    'com.premium.10for6',
    'com.premium.10for12',
    'com.golden.3for1',
    'com.golden.3for6',
    'com.golden.7for1',
    'com.golden.10for1',
  ],
  android: [''],
});

class PaymentPlan extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      // console.warn('-->>',state.params.isRTL)
      return {
        title: state.params.isRTL === true ? 'خطة السداد' : 'Payment Plan',
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      plan: [],
      isLoading: true,
      token: '',
      currentCurrencyValue: 1,
      subscriptionPlanData: {},
      html: '',
      newSubscriptionPlan: '',
      isSubscribe: false,
      iOSProducts: [],
      pruchaseReceipt: '',
      isPurchased: false,
      platform: '',
    };
    this.getUser = this.getUser.bind(this);
  }

  async getUser() {
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    this.setState({token: token});
  }

  getSubscriptionPlanData = token => {
    fetch(API_URL + '/staticSubscriptionplan', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn('responseData new api -> ',responseData);
        this.setState({
          newSubscriptionPlan: responseData.subscriptionPlan,
          subscribedPlanId: responseData.subscriptionPlan.subscribedPlanId,
          isLoading: false,
        });
      });
  };

  purchaseUpdateSubscription = null;
  purchaseErrorSubscription = null;

  async componentDidMount() {
    await this.getUser();
    const {currencyCode} = this.props.country;
    const token = this.state.token;

    if (currencyCode !== 'SAR') {
      this.setState({
        currentCurrencyValue: await getCurrencyValue(currencyCode),
      });
    }
    this.getSubscriptionPlanData(token);

    const data = this.props.navigation.state.params.sellerSubscriptionPlan;
    // console.warn("TCL: componentDidMount -> data", data.platform)
    if (data) {
      this.setState({platform: data.platform});
    }

    if (Platform.OS == 'ios') {
      try {
        const iOSProducts = await RNIap.getProducts(items);
        // console.warn("TCL: componentDidMount -> products", iOSProducts );
        this.setState({iOSProducts});
      } catch (err) {
        console.warn(err);
        Alert.alert('Error', err);
      }

      this.purchaseUpdateSubscription = purchaseUpdatedListener(purchase => {
        // console.warn('purchaseUpdatedListener', purchase);
        const receipt = purchase.transactionReceipt;
        if (receipt) {
          if (Platform.OS === 'ios') {
            RNIap.finishTransactionIOS(purchase.transactionId);
          } else {
            // Retry / conclude the purchase is fraudulent, etc...
          }
        }
      });

      this.purchaseErrorSubscription = purchaseErrorListener(error => {
        this.setState({isPurchased: false});
        // console.warn('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      });
    }

    this.props.navigation.setParams({isRTL: this.props.isRTL});
    this.props.navigation.state.params.isRTL = this.props.isRTL;
  }

  componentWillUnmount() {
    if (this.purchaseUpdateSubscription) {
      this.purchaseUpdateSubscription.remove();
      this.purchaseUpdateSubscription = null;
    }
    if (this.purchaseErrorSubscription) {
      this.purchaseErrorSubscription.remove();
      this.purchaseErrorSubscription = null;
    }
  }

  showSubcriptionPlan(subscriptionPlan) {
    const {isRTL} = this.props;

    const currentPlatform = Platform.OS;
    //console.warn(sellerSubscriptionPlan)

    if (
      this.state.platform === '' ||
      currentPlatform === this.state.platform.toLowerCase()
    ) {
      // console.warn('SubscriptionPlan-->>',subscriptionPlan)
      // const { sellerSubscriptionPlan } = this.props.navigation.state.params;
      const {sellerSubscriptionPlan} = this.props.navigation.state.params; // Seller current subscription plan
      const {used_pin} = sellerSubscriptionPlan;

      const {newSubscriptionPlan} = this.state;
      const {subscribedPlanId, subscribedPlanName} = newSubscriptionPlan;
      const {
        id,
        subscription_plan_name,
        price,
        discounted_price,
        title,
        duration,
        pin,
        months,
        app_id,
      } = subscriptionPlan;
      //const [validity] = name.split(' ');
      // console.warn('sellerSubscriptionPlan->>',sellerSubscriptionPlan)
      // const { start_date, end_date, total_pin, subscription_plan } = sellerSubscriptionPlan;
      let startDate, endDate;
      const newDate = new Date();
      startDate = moment(newDate).format('YYYY-MM-DD');
      startDate = moment(newDate, 'YYYY-MM-DD')
        .add(1, 'days')
        .format('YYYY-MM-DD');
      endDate = moment(startDate)
        .add(parseInt(duration), 'month')
        .format('YYYY-MM-DD');

      if (used_pin > pin) {
        const EngMsg = `You are applying for ${pin} Sales Point and currently you have used ${used_pin} Sales Point, please remove extra Sales point first in order to downgrade the susbcription plan.`;
        const ArbMsg = `الاشتراك الجديد ${pin} نقاط بيع في حين المستخدم حاليا ${used_pin} نقاط￼￼ بيع. يرجى إزالة نقاط البيع الإضافية قبل تغيير الحساب`;
        const message = isRTL ? ArbMsg : EngMsg;
        Alert.alert('', message);
        return;
      }
      // if(subscription_plan && subscription_plan.toLowerCase() === 'free') {
      // 	const newDate = new Date();
      // 	startDate = moment(newDate).add(1, 'days').format('YYYY-MM-DD');
      // 	endDate = moment(newDate).add(1, 'days').add(parseInt(validity),'month').format('YYYY-MM-DD');
      // } else {
      // 	startDate = moment(end_date, "YYYY-MM-DD").add(1, 'days').format('YYYY-MM-DD');
      // 	endDate = moment(startDate, "YYYY-MM-DD").add(parseInt(validity),'month').format('YYYY-MM-DD');
      // }

      const subscriptionPlanData = {
        id,
        applyingPlan: subscription_plan_name,
        currentPlan: subscribedPlanName ? subscribedPlanName : 'Free',
        price,
        discounted_price,
        name: months,
        validity: duration,
        startDate,
        endDate,
        total_pin: pin,
        platform: Platform.OS,
        app_id,
      };
      //console.warn('subscriptionPlanData',subscriptionPlanData)
      this.setState({subscriptionPlanData});
      this.refs.modal1.open();
    } else if (currentPlatform !== this.state.platform.toLowerCase()) {
      const message = this.props.isRTL
        ? `تم عمل اشتراكك الحالي من جهاز ${this.state.platform} ، يرجى تغيير او الغاء الاشتراك من جهاز ${this.state.platform}`
        : `Your current subscription plan is from ${this.state.platform} device,kindly change or cancel your subscription from ${this.state.platform} device`;
      Alert.alert('', message);
      return;
    }
  }

  componentWillReceiveProps(nextProps) {
    // console.warn('nextProps', nextProps)
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  onClickCancel = () => {
    // this.setState({subscriptionPlanData: {}})
    this.refs.modal1.close();
  };

  updateSubscriptionPlanHandler = async () => {
    this.setState({isPurchased: true});
    //console.warn(this.state.subscriptionPlanData);
    const {isRTL} = this.props;
    const {
      id,
      applyingPlan,
      price,
      discounted_price,
      name,
      total_pin,
      startDate,
      endDate,
      currentPlan,
      validity,
      platform,
      app_id,
    } = this.state.subscriptionPlanData;

    // if(currentPlan && currentPlan.toLowerCase() === 'free') {
    const currentCurrencyValue = this.state.currentCurrencyValue;
    // const totalCost = price * currentCurrencyValue;
    const totalCost = discounted_price * currentCurrencyValue;
    const requestData = {
      ivp_amount: totalCost, //total amt
      ivp_desc: applyingPlan, //Platinum or Premium
      repeat_period: validity, // validity
      repeat_start: startDate
        .split('-')
        .reverse()
        .join(''), // start date
      xtra_amount: totalCost, // single pin price in old model
      xtra_package: applyingPlan, // Platinum or Premium
    };

    //console.warn('requestData-->>',requestData)

    if (Platform.OS === 'android') {
      fetch(API_URL + '/create-recurring-payment', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: this.state.token,
        },
        body: JSON.stringify(requestData),
      })
        .then(response => response.text())
        .then(responseData => {
          // console.warn('responseData',responseData);
          this.setState({html: responseData, isPurchased: false});
        });
    } else {
      //console.warn("TCL: Ios purchace -")
      RNIap.requestSubscription(app_id)
        .then(purchase => {
          //console.warn('purchase Ios resp ->', purchase);
          this.sendPurchaseData(purchase);
        })
        .catch(error => {
          console.warn(error.message);
        });
    }
    // } else {

    // 	fetch(API_URL+'/changeSubscriptionPlan', {
    // 		method: 'POST',
    // 		headers: {
    // 			Accept: 'application/json',
    // 			'Content-Type': 'application/json',
    // 			Authorization: this.state.token
    // 		},
    // 		body: JSON.stringify(this.state.subscriptionPlanData)
    // 	})
    // 	.then(response => response.json())
    // 	.then(responseData => {
    // 		// console.warn('--->>',responseData);
    // 		if (responseData.status == 200) {
    // 			const title = isRTL ? 'تم تحديث الاشتراك ' : 'Subscription Updated';
    // 			const message = isRTL ? 'تم تعديل اشتراكك بنجاح ' : 'Your subscription has been changed successfully'
    // 			Alert.alert(title, message);
    // 			this.props.navigation.state.params.customGoBackHandler();
    // 			this.props.navigation.goBack();
    // 		}
    // 	})
    // 	.catch(err => {
    // 		Alert.alert('Error',err.message);
    // 	});
    // }
  };

  sendPurchaseData = purchase => {
    //console.warn("TCL: sendPurchaseData -> ", purchase);

    const iOSTransactionData = {
      iosReceipt: purchase.transactionReceipt,
      transactionDate: purchase.originalTransactionDateIOS,
      transactionId: purchase.transactionId,
      productId: purchase.productId,
      planId: this.state.subscriptionPlanData.id,
      planType: 'Subscription Plan',
      start_date: this.state.subscriptionPlanData.startDate,
      end_date: this.state.subscriptionPlanData.endDate,
    };
    //console.warn("TCL: -> iOSTransactionData", iOSTransactionData)

    fetch(API_URL + '/paymentWithIos', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify(iOSTransactionData),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('payment api resp --->>',responseData);
        this.setState({isPurchased: false});
        this.onClickCancel();
        this.props.navigation.goBack();
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  };

  _onNavigationStateChange = webViewState => {
    const {isRTL} = this.props;
    if (webViewState.url === WEB_URL + '/thank-you') {
      this.props.navigation.goBack();
      const title = isRTL ? 'نجاح' : 'Success';
      const message = isRTL
        ? 'تم اعتماد التحويل بنجاح '
        : 'Transaction successfuly authorised';
      Alert.alert(title, message);
    } else if (webViewState.url === WEB_URL + '/transaction') {
      this.props.navigation.goBack();
      const title = isRTL ? 'فشل' : 'Failed';
      const message = isRTL
        ? 'لم يتم اعتماد التحويل'
        : 'Transaction could not authorised';
      Alert.alert(message);
    } else if (webViewState.url === WEB_URL) {
      this.props.navigation.goBack();
    }
  };

  changePlatinumToGold(planType, isRTL) {
    if (!planType) {
      return;
    }

    if (planType.toLowerCase() === 'platinum') {
      return isRTL ? 'ذهبي' : 'Golden';
    } else if (planType.toLowerCase() === 'premium') {
      return isRTL ? 'مميز' : 'Premium';
    }

    //return  isRTL ? 'علاوة' : planType;
  }

  render() {
    const isRTL = this.props.isRTL;
    const styles = StyleSheetFactory.getSheet(isRTL);
    const {navigate} = this.props.navigation;
    const plan = this.props.navigation.state.params.subscriptionPlan.planType[0]
      .subscription_name;
    // console.warn('new plan--->>',plan);
    const {currencyCode} = this.props.country;
    const currentCurrencyValue = this.state.currentCurrencyValue;
    const newPlan = this.state.newSubscriptionPlan
      ? plan.toLowerCase() === 'premium'
        ? this.state.newSubscriptionPlan.premium.plans
        : this.state.newSubscriptionPlan.platinum.plans
      : '';
    //const subscribedPlanId = this.state.subscribedPlanId;
    // const { total_pin } = this.props.navigation.sta te.params.sellerPinsDetail;
    //console.warn(this.state.subscribedPlanId)
    const subscriptionTitle = isRTL ? 'طلب مساعدة' : 'Information Request';
    const subscribedPlanId = this.state.subscribedPlanId;
    const {
      applyingPlan,
      price,
      discounted_price,
      name,
      total_pin,
      startDate,
      endDate,
    } = this.state.subscriptionPlanData;
    const totalCost = discounted_price * currentCurrencyValue;
    // console.warn('html-->>',this.state.html)

    if (this.state.html) {
      return (
        <WebView
          originWhitelist={['*']}
          javaScriptEnabled={true}
          source={{html: this.state.html, baseUrl: ''}}
          onNavigationStateChange={this._onNavigationStateChange}
        />
      );
    }

    // console.warn('data.id==subscribedPlanId',this.state.newSubscriptionPlan.subscribedPlanId)

    return (
      <View style={styles.container}>
        {
          // <View style={{flexDirection: 'row'}}>
          // 	<Text>Number of Pins</Text>
          // 	<TextInput
          // 		underlineColorAndroid='#FFF'
          // 		style={{ borderRadius:5, marginLeft:5, backgroundColor: '#FFF', padding:0, paddingHorizontal: 5, width: '7%'}}
          // 	/>
          // </View>
        }
        <ScrollView style={{marginTop: 15}}>
          {this.state.isLoading ? (
            <PreLoader />
          ) : newPlan.length ? (
            newPlan.map((data, i) => {
              {
                /* const enTitle = `${data.months} ${data.subscription_plan_name} for ${data.pin} Sales Points`;
							const arTitle = `${data.months} ${data.subscription_plan_name} ل ${data.pin} نقطة البيع`; */
              }
              const displayTitle = isRTL
                ? data.description_arb
                : data.description;
              //console.warn('-->>',subscribedPlanId, data.id)
              return (
                <TouchableOpacity
                  disabled={subscribedPlanId == data.id}
                  onPress={() => this.showSubcriptionPlan(data)}
                  key={i}
                  style={[
                    styles._messageBox,
                    {
                      backgroundColor:
                        subscribedPlanId == data.id ? '#e0e0d1' : '#fff',
                    },
                  ]}>
                  <View style={styles._textBox}>
                    <View
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={[styles._title, {color: '#000', fontSize: 18}]}>
                        {displayTitle}
                      </Text>
                    </View>
                    <Text style={[styles._date, {color: '#00ADDE'}]}>
                      {isRTL ? 'السعر' : 'Price'}{' '}
                      {(
                        parseFloat(data.discounted_price) * currentCurrencyValue
                      ).toFixed(2)}{' '}
                      {currencyCode}
                    </Text>
                    {/*
											<View
												style={
													styles._descriptionContainer
												}
											>
												<Text
													style={[styles._description,{fontSize:13,}]}
												>
												{data.title}{" "}{isRTL ? data.description_arb : data.description_detail}
												</Text>

											</View>
											*/}
                  </View>
                </TouchableOpacity>
              );
            })
          ) : (
            <NoDataFound />
          )}

          {!!newPlan.length && (
            <TouchableOpacity
              style={{
                borderRadius: 25,
                backgroundColor: '#00ADDE',
                padding: 10,
                paddingHorizontal: 5,
                margin: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() =>
                this.props.navigation.navigate('Help', {
                  title: subscriptionTitle,
                })
              }>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: '#FFF',
                  paddingHorizontal: 10,
                }}>
                {isRTL ? 'للمساعدة' : 'For help'}
              </Text>
            </TouchableOpacity>
          )}
        </ScrollView>
        <Modal
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: 300,
            width: 350,
            padding: 15,
          }}
          position={'center'}
          ref={'modal1'}
          onClosed={() => this.setState({subscriptionPlanData: {}})}>
          <Text
            style={{
              fontWeight: 'bold',
              color: '#000000',
              fontSize: 16,
              marginBottom: 10,
            }}>
            {isRTL ? 'تفاصيل الخطة' : 'Plan Details'}
          </Text>
          <View style={{}}>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL
                  ? `نوع الاشتراك: ${this.changePlatinumToGold(
                      applyingPlan,
                      isRTL,
                    )}`
                  : `Applying for: ${this.changePlatinumToGold(
                      applyingPlan,
                      isRTL,
                    )}`}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL
                  ? `تاريخ البداية: ${startDate}`
                  : `Start Date: ${startDate}`}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL
                  ? `تاريخ النهاية: ${endDate}`
                  : `Renewal Date: ${endDate}`}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL ? `خطة الاشتراك ${name}` : `Plan Validity: ${name} `}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL
                  ? 'عدد نقاط البيع:'
                  : 'Total Sales Point In Your Account:'}{' '}
                {total_pin}{' '}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{color: '#000000'}}>
                {isRTL ? 'تكلفة الاشتراك:' : 'Cost for total package:'}{' '}
                {totalCost * currentCurrencyValue} {currencyCode}{' '}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{fontWeight: 'bold', color: '#000000'}}>
                {isRTL
                  ? 'سيتم تجديد الاشتراك تلقائيًا'
                  : 'Subscription will be renewed automatically'}{' '}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{}}>_________________________________</Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <Text style={{fontWeight: 'bold', color: '#000000'}}>
                {isRTL ? 'تكلفة الاشتراك:' : 'Net Payable Amount:'}{' '}
                {totalCost * currentCurrencyValue} {currencyCode}{' '}
              </Text>
            </View>
            <View
              style={{
                flexDirection: isRTL ? 'row-reverse' : 'row',
                marginTop: 20,
                justifyContent: 'space-evenly',
              }}>
              <View
                style={{
                  flexDirection: isRTL ? 'row-reverse' : 'row',
                  marginTop: 20,
                  justifyContent: 'space-evenly',
                }}>
                <TouchableOpacity
                  onPress={this.updateSubscriptionPlanHandler}
                  style={{
                    borderRadius: 25,
                    backgroundColor: '#0099e6',
                    padding: 10,
                    paddingHorizontal: 15,
                  }}>
                  <Text style={{fontWeight: 'bold', color: '#FFF'}}>
                    {isRTL ? 'دفع' : 'Payment'}
                  </Text>
                  {this.state.isPurchased ? (
                    <ActivityIndicator size="small" color="red" />
                  ) : null}
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: isRTL ? 'row-reverse' : 'row',
                  marginTop: 20,
                  justifyContent: 'space-evenly',
                }}>
                <TouchableOpacity
                  onPress={this.onClickCancel}
                  style={{
                    borderRadius: 25,
                    backgroundColor: '#e62e00',
                    padding: 10,
                    paddingHorizontal: 15,
                  }}>
                  <Text style={{fontWeight: 'bold', color: '#FFF'}}>
                    {isRTL ? 'إلغاء' : 'Cancel'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    country: state.network.countryLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PaymentPlan);
