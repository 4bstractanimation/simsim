import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  Text,
  View,
  Image,
  Alert,
  AsyncStorage
} from 'react-native';

import ActionButton from 'react-native-action-button';
// import ListView from './ListView';
import Tab from './TabView';
import MapView from './maps/Maps';
import AddPin from './AddPin';
import moment from "moment";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
// import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class FloatingButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // pins: [],
      token:'',
      dob:"",
      remainingPin: '',
      seller_type: ''
    };
  }

  componentDidMount() {
    // console.warn('componentDidMount',this.props.loginData.sData)
    const token = this.props.loginData.sData;
    if(token) {
      this.getSellerSubscriptionPlan(token)
    }
    
   
  }

  componentWillReceiveProps(props) {
    // console.warn('componentWillReceiveProps::',props.loginData);
    // console.warn('this.props',this.props.loginData);
    if(props.loginData.sData !== this.props.loginData.sData) {
      if(props.loginData.sData) {
        this.setState({ token: props.loginData.sData });
        this.getSellerSubscriptionPlan(props.loginData.sData);
      } else {
        this.setState({token: '', seller_type: ''});
      }
    } else {
        if(props.loginData.sData) {
          this.setState({ token:props.loginData.sData })
        }
    }
  }

  getSellerSubscriptionPlan(token) {    
    
    if(!token) {
      return;
    }
    const url = API_URL+'/get-seller-subscription-plan';

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token
      }
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('get-seller-subscription-plan-->>',responseData.data);
        if( responseData && responseData.status == 200 ) {
          const { total_pin, used_pin, seller_type } = responseData.data;
          const remainingPin =  total_pin - used_pin;
          this.setState({remainingPin, seller_type, used_pin});
        }
        
      })
      .catch(err => {        
       Alert.alert('Error', err.message)
      });
  }

  userNotLoggedIn = () => {
    const { navigate } = this.props.navigation;
    const { isRTL } = this.props;
    const message = isRTL ? 'يجب تسجيل الدخول' : 'You need to login';
    const loginText = isRTL ? 'تسجيل الدخول' : 'Login';
    const signupText = isRTL ? 'تسجيل' : 'SignUp';
    const cancelText = isRTL ? 'لا' : 'Cancel';
    Alert.alert(
      '',message,
      [
        {text: loginText, onPress: () => navigate('Login')},
        {text: signupText, onPress: () => navigate('SignUp')},
        {text: cancelText},
      ],
      { cancelable: false }
    )
  }

  createPinHandler = () => {

    if(!this.props.loginData.sData) {
        this.userNotLoggedIn();
        return;
    }
    const { isRTL } = this.props;
    const { seller_type } = this.state;
    
    if(this.state.used_pin === 1 && seller_type && seller_type.toLowerCase() === 'individual') {
      const title = isRTL ? 'Access Denied' : 'Access Denied';
      const message = isRTL ? "يمكن للبائع الفردي انشاء نقطة بيع واحدة فقط" : "Individual Seller can only have 1 Sales Point";
      Alert.alert(title, message);
      return;
    }
    
     const dob = this.dobHandle();

     if(dob < 18) {
      const message = isRTL ? "يجب ان يكون عمر البائع فوق 18سنة لينشر منتجاته وخدماته" : "Seller needs to be 18+ to post products and services";
     Alert.alert('',message);
      return;
    }

    if(this.state.remainingPin === 0|| this.state.remainingPin<0) {
      const { navigate } = this.props.navigation;
      const title = isRTL ? "لا يوجد نقاط بيع متبقية" : "No remaining Sales Point!";
      const message = isRTL ? 'لقد استخدمت جميع نقاط البيع المخصصة. تحتاج إلى إضافة نقطة مبيعات إضافية' : 'You have used all allowed Sales Point, upgrade your account to add more sales point';
      const addText = isRTL ? 'اضف نقاط بيع إضافية' : 'Upgrade';
      const cancelText = isRTL ? 'الغاء' : 'Cancel';
     
      Alert.alert(title,message,
          [
            {text: addText, onPress: () => this.props.navigation.navigate('Subscription')},
            {text: cancelText},
          ],
          { cancelable: false }
        );

      return;
    }
    // console.warn('createPinHandler',this.state.remainingPin)
   if(this.state.remainingPin > 0 ) {
      AsyncStorage.getItem('isProfileCompleted').then( res => {
        
        const data = JSON.parse(res);
       
        if(data.isCompleted) {
          this.props.navigation.navigate('AddPin')
        } else {
          const message = isRTL ? 'يرجى اكمال حسابك الشخصي ' : 'Please complete your profile';
          const laterButtonText = isRTL ? "لاحقا" : 'Later';
          const okText = isRTL ? 'نعم' : 'OK'
          Alert.alert(
              '',
              message,
              [
                {
                  text: okText,
                  onPress: () => {
                    this.props.navigation.navigate('Profile');
                  }
                },
                {
                  text: laterButtonText,
                  // onPress: () => {
                  //   alert('First complete your profile');
                  // }
                }
              ],
              { cancelable: false }
          );
        }
      })
    }
  }

  dobHandle(){
    const dob = this.state.dob
    //console.warn("DOB ---> "+dob);

    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
        age--;
    }
    // console.warn("NEw age : "+ age);
    return age;
    
  }

  render() {
    // console.warn('render login data',this.props.loginData.sData, this.props.sellerType)
    const { navigate } = this.props.navigation;
    const token = this.state.token;
    const val = this.props.mapView ? 90 : 30;
   
   // let seller_type = this.state.seller_type;
   // console.warn("TCL: FloatingButton -> seller_type", this.props.sellerType);
   
    return (
      <ActionButton
        buttonColor="rgba(0,0,0,1)"
        size={50}
        position='right'
        buttonTextStyle={{ fontSize: 40 }}
        bgColor="rgba(243, 242, 242,0.8)"
        offsetX={val}
        // onPressIn={() => console.warn('......')}
      >
        {this.props.sellerType && this.props.sellerType.toLowerCase() === 'enterprise' ? (
          <ActionButton.Item
            buttonColor="#999999"
            title={ this.props.isRTL ? "إدارة الإعلانات التجارية" : "Manage Promotions"}
            textStyle={styles.buttonItemText}
            textContainerStyle={styles.buttonContainerStyle}
            size={45}
            onPress={() => navigate('ManageCommercial')}
          >
            <Image
              style={{ width: 60, height: 60, marginTop: 10 }}
              source={require('../icons/manage_commercial1x.png')}
            />
          </ActionButton.Item>
        )
        :''
        
        }
        <ActionButton.Item
          buttonColor="#1aba18"
          title={ this.props.isRTL ? "إضافة منبه عروض" : "Add Offer Alarm"}
          textStyle={styles.buttonItemText}
          textContainerStyle={styles.buttonContainerStyle}
          size={45}
          onPress={() => token ? navigate('Promotion') : this.userNotLoggedIn() }
        >
          <Image
            style={{ width: 60, height: 60, marginTop: 10 }}
            source={require('../icons/set_alert`x.png')}
          />
        </ActionButton.Item>
        <ActionButton.Item
          buttonColor="#33d6ff"
          title={ this.props.isRTL ? "إضافة نقطة بيع" : "Add Sales Point"}
          textStyle={styles.buttonItemText}
          textContainerStyle={styles.buttonContainerStyle}
          size={45}
          onPress={this.createPinHandler }
        >
          <Image
            style={{ width: 60, height: 60, marginTop: 10 }}
            source={require('../icons/add_pin.png')}
          />
        </ActionButton.Item>
      </ActionButton>
    );
  }
}

const styles = StyleSheet.create({
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white'
  },
  buttonItemText: {
    fontWeight: 'bold',
    paddingTop: 2
  },
  buttonContainerStyle: {
    borderRadius: 30,
    height: 30,
    borderWidth: 2
  }
});

function mapStateToProps(state) {
  // console.warn('FloatingButton-->>',state.loginReducer)
  return {
    isRTL: state.network.isRTL,
    loginData: state.loginReducer.loginUserData,
    sellerType : state.network.sellerType
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FloatingButton);
