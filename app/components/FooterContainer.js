import React, {Component} from 'react';
import Footer from './Footer';
import SocialSharing from './SocialSharing';
import {
  View,
  Text,
  Button,
  Dimensions,
  Image,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';

export default class FooterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
  }

  toggleVisible = () => {
    this.setState(prevState => {
      return {toggle: !prevState.toggle};
    });
  };
  render() {
    const toggle = this.state.toggle;
    return (
      <View style={{marginBottom: 50}}>
        <SocialSharing toggle={toggle} toggleVisible={this.toggleVisible} />
        <Footer
          toggleVisible={this.toggleVisible}
          {...this.props}
          footerLat={this.props.footerLat}
          footerLng={this.props.footerLng}
        />
      </View>
    );
  }
}
