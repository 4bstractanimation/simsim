import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
  ScrollView,
  // StyleSheet,
  Image,
  TouchableOpacity,
  // TouchableHighlight,
  // Modal,
  // Platform,
  // Button,
  AsyncStorage,
} from 'react-native';
import Toast from 'react-native-simple-toast';
// import FloatingButton from './FloatingButton';
// import bellIcon from './Navigation-Drawer';
import NotificationHeader from './NotificationHeader';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
// import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import * as pinActions from '../actions/pinAction';
import * as seachActions from '../actions/Search';
import StyleSheetFactory from './../styles/Pin';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import Share from 'react-native-share';
// import SocialSharing from './SocialSharing';
import API_URL, {WEB_URL} from '../configuration/configuration';
// import {StackActions, NavigationActions} from 'react-navigation';
// import Subscription from './Subscription';

class Pin extends Component {
  static navigationOptions = ({navigation}) => {
    let state = navigation.state;
    if (state.params !== undefined) {
      return {
        title: state.params.isRTL === true ? 'نقاط البيع' : 'Sales Point',
        headerBackTitle: null,
        headerRight: <NotificationHeader navigation={navigation} />,
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      pins: [],
      responseMsg: '',
      promptVisible: false,
      pinId: '',
      pinNumber: '',
      toggle: false,
      shareOptions: {},
      remainingPin: '',
      sellerSubscriptionPlan: {},
    };
  }

  toggleVisible = () => {
    this.setState(prevState => {
      return {toggle: !prevState.toggle};
    });
  };

  async getAllPin() {
    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
    const token = this.props.loginData.sData;
    console.log('toek', token);
    const isRTL = this.props.isRTL;
    fetch(API_URL + '/usersAllPin', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('all pins-->>', responseData.allPins)
        if (
          responseData.status == 654864 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          this.setState({
            isLoading: false,
            responseMsg: responseData.eMsg,
            pins: [],
          });
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          this.props.setPinData(responseData.allPins);
          this.setState({pins: responseData.allPins, isLoading: false});
        } else if (responseData.status == 654864) {
          this.props.setSellerType('');
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول لنقطة البيع'
            : 'Failed to get Sales Point';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  async componentDidMount() {
    this.props.setSelectedcategory({cat: '', id: ''});

    this.getSellerSubscriptionPlan();

    this.props.navigation.addListener('didFocus', async event => {
      this.getAllPin();
      this.getSellerSubscriptionPlan();
    });
  }

  getSellerSubscriptionPlan() {
    const token = this.props.loginData.sData;
    const url = API_URL + '/get-seller-subscription-plan';
    const isRTL = this.props.isRTL;
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('getSellerSubscriptionPlan-->>',responseData);
        if (responseData && responseData.status == 200) {
          const {total_pin, used_pin} = responseData.data;
          const remainingPin = total_pin - used_pin;
          this.setState({
            sellerSubscriptionPlan: responseData.data,
            remainingPin,
          });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  editpin(data) {
    const type = 'edit';
    this.props.navigation.navigate('AddPin', {data, type});
  }

  deletePin(pin) {
    const message = this.props.isRTL
      ? 'هل تريد مسح نقطة البيع هذه؟'
      : 'Do you want to delete this pin?';
    const yesText = this.props.isRTL ? 'نعم' : 'Yes';
    const noText = this.props.isRTL ? 'لا' : 'No';
    Alert.alert(
      '',
      message,
      [
        {text: noText, style: 'cancel'},
        {text: yesText, onPress: () => this.deletePinHandler(pin)},
      ],
      {cancelable: false},
    );
  }

  deletePinHandler(pinData) {
    const {pinId, pinNumber} = pinData;
    const token = this.props.loginData.sData;
    const isRTL = this.props.isRTL;
    fetch(API_URL + '/delete-pin', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        pinId: pinId,
        pin_no: pinNumber,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 236486
        ) {
          alert('fields required');
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          Alert.alert('data not saved');
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          // console.warn('deleted-->>', responseData);
          this.getAllPin();
          this.getSellerSubscriptionPlan();
          // Alert.alert('','Pin deleted successfully!');
          const message = isRTL
            ? 'تم حذف نقطة البيع بنجاح'
            : 'Sales Point deleted successfully';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        }
      });
  }

  shareOnSocial = item => {
    this.setState({toggle: true});
    const isRTL = this.props.isRTL;
    const {id, pin_name, user_id, description} = item;
    // const url =
    //   'https://www.simsim-markets.com/pin/' +
    //   pin_name.replace(/ +/g, '_') +
    //   '/' +
    //   user_id +
    //   '/' +
    //   id;
    const url = WEB_URL + '/pins/' + id;
    const titleDesc = pin_name + ':- ' + description;
    const shareOptions = {
      title: 'Share via',
      message: titleDesc,
      url: url,
    };
    // this.setState({shareOptions})
    Share.open(shareOptions, function(e) {
      console.warn(e);
    });
  };

  createPinHandler = () => {
    // console.warn('seller_type-->>',this.state.sellerSubscriptionPlan);
    const {used_pin, seller_type} = this.state.sellerSubscriptionPlan;
    const isRTL = this.props.isRTL;
    // console.warn(seller_type)
    if (
      used_pin === 1 &&
      seller_type &&
      seller_type.toLowerCase() === 'individual'
    ) {
      const message = isRTL
        ? 'يمكن للبائع الفردي انشاء نقطة بيع واحدة فقط '
        : 'Individual Seller can only have 1 Sales Point';
      Alert.alert('', message);
      return;
    }

    const dob = this.dobHandle();

    if (dob < 18) {
      const message = isRTL
        ? 'يجب ان يكون عمر البائع فوق 18سنة لينشر منتجاته وخدماته'
        : 'Seller needs to be 18+ to post products and services';
      Alert.alert('', message);
      return;
    }

    if (this.state.remainingPin <= 0) {
      // console.warn("TCL: createPinHandler -> remainingPin", this.state.remainingPin)
      const {navigate} = this.props.navigation;
      const title = isRTL
        ? 'لا يوجد نقاط بيع متبقية '
        : 'No remaining Sales Point!';
      const message = isRTL
        ? 'قمت باستخدام كامل نقاط البيع في حسابك. يمكنك إضافة نقاط بيع إضافية'
        : 'You have used all allowed Sales Point, upgrade your account to add more sales point.';
      const addText = isRTL ? 'اضف نقاط بيع إضافية ' : 'Upgrade';
      const cancelText = isRTL ? 'الغاء' : 'Cancel';

      Alert.alert(
        title,
        message,
        [
          {
            text: addText,
            onPress: () => this.props.navigation.replace('Subscription'),
          }, //navigate('Subscription')},
          {text: cancelText},
        ],
        {cancelable: false},
      );

      return;
    }

    if (this.state.remainingPin > 0) {
      AsyncStorage.getItem('isProfileCompleted').then(res => {
        const data = JSON.parse(res);
        console.warn('data', data);
        if (data) {
          if (data.isCompleted) {
            this.props.navigation.navigate('AddPin');
          } else {
            const message = isRTL
              ? 'يرجى اكمال حسابك الشخصي '
              : 'Please complete your profile';
            const laterButtonText = isRTL ? 'لاحقا' : 'Later';
            const okText = isRTL ? 'نعم' : 'OK';
            Alert.alert(
              '',
              message,
              [
                {
                  text: okText,
                  onPress: () => {
                    this.props.navigation.navigate('Profile');
                  },
                },
                {
                  text: laterButtonText,
                  // onPress: () => {
                  //   alert('First complete your profile');
                  // }
                },
              ],
              {cancelable: false},
            );
          }
        } else {
          const message = isRTL
            ? 'يرجى اكمال حسابك الشخصي '
            : 'Please complete your profile';
          const laterButtonText = isRTL ? 'لاحقا' : 'Later';
          const okText = isRTL ? 'نعم' : 'OK';
          Alert.alert(
            '',
            message,
            [
              {
                text: okText,
                onPress: () => {
                  this.props.navigation.navigate('Profile');
                },
              },
              {
                text: laterButtonText,
                // onPress: () => {
                //   alert('First complete your profile');
                // }
              },
            ],
            {cancelable: false},
          );
        }
      });
    }
  };

  dobHandle() {
    const dob = this.props.loginData.userData.dob;

    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    return age;
  }
  render() {
    // console.warn('remainingPin-->>',this.props.loginData)
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {state, navigate} = this.props.navigation;
    const Pins = this.state.pins;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            borderRadius: 15,
            padding: 8,
            backgroundColor: '#000',
            justifyContent: 'center',
            width: '30%',
            alignSelf: 'flex-end', //this.props.isRTL ? 'flex-start' : 'flex-end',
            marginBottom: 10,
          }}
          onPress={this.createPinHandler}>
          <Text style={{color: '#FFF', fontWeight: 'bold'}}>
            {this.props.isRTL ? 'انشاء نقطة بيع' : 'Create Pin'}
          </Text>
        </TouchableOpacity>
        <ScrollView>
          {this.state.isLoading ? (
            <PreLoader />
          ) : this.state.pins.length ? (
            this.state.pins.map((item, i) => {
              return (
                <TouchableOpacity
                  key={i}
                  onPress={() => navigate('SellerContainer', {item})}>
                  <View style={styles._messageBox} key={i}>
                    <View style={{flex: 1.2}}>
                      <Image
                        style={styles._image}
                        source={{uri: item.pin_image}}
                      />
                    </View>
                    <View style={styles._textBox}>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: this.props.isRTL
                            ? 'row-reverse'
                            : 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text
                          style={[
                            styles._title,
                            {textAlign: this.props.isRTL ? 'right' : null},
                          ]}>
                          {item.pin_name}
                        </Text>
                        <View
                          style={{
                            flexDirection: this.props.isRTL
                              ? 'row-reverse'
                              : 'row',
                            justifyContent: 'space-between',
                          }}>
                          <TouchableOpacity
                            style={{marginLeft: 5}}
                            onPress={() => this.shareOnSocial(item)}>
                            <View style={{borderWidth: 1, borderRadius: 25}}>
                              <Text style={{fontSize: 12, padding: 3}}>
                                {this.props.isRTL ? 'نشر' : 'Publish'}
                              </Text>
                            </View>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{marginLeft: 5}}
                            onPress={() => {
                              this.editpin(item);
                            }}>
                            <Image
                              style={styles.image}
                              source={require('./../icons/edit2x.png')}
                            />
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() =>
                              this.deletePin({
                                pinId: item.id,
                                pinNumber: item.pin_no,
                              })
                            }>
                            <Image
                              style={[styles.image, {marginLeft: 5}]}
                              source={require('./../icons/delete2x.png')}
                            />
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={styles._descriptionContainer}>
                        <Text
                          style={[
                            styles._description,
                            {textAlign: this.props.isRTL ? 'right' : null},
                          ]}>
                          {item.description}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })
          ) : (
            <NoDataFound message={this.state.responseMsg} />
          )}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    loginData: state.loginReducer.loginUserData,
  };
}

// function mapDispatchToProps(dispatch) {
// 	return bindActionCreators(seachActions, dispatch)
// }

export default connect(
  mapStateToProps,
  {...layoutActions, ...pinActions, ...seachActions},
)(Pin);
