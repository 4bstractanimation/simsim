import React, {Component} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import SearchInput, {createFilter} from 'react-native-search-filter';
import data from './data';
import database from '@react-native-firebase/database';

const KEYS_TO_FILTERS = ['title', 'description'];

class SearchFilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      searchText: '',
      arrayholder: data,
    };
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSearch(event) {
    this.setState({
      searchText: event,
    });
  }
  GetListViewItem(title) {
    Alert(title);
  }

  render() {
    const filteredData = this.state.arrayholder.filter(
      createFilter(this.state.searchText, KEYS_TO_FILTERS),
    );
    return (
      <View style={styles.container}>
        <SearchInput
          style={styles.searchText}
          onChangeText={text => this.handleSearch(text)}
          placeholder="Search Here"
        />

        <ScrollView>
          {filteredData.map((arr, i) => {
            return (
              <TouchableOpacity
                onPress={() => alert(arr.title)}
                key={i}
                style={styles._title}>
                <View>
                  <Text>{arr.title}</Text>
                  <Text>{arr.description}</Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  rowViewContainer: {
    fontSize: 17,
    padding: 10,
  },
  searchText: {
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    width: 335,
    height: 50,
    backgroundColor: 'white',
    marginTop: 20,
    textAlign: 'center',
  },
});

export default SearchFilter;
