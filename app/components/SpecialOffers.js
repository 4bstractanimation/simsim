import React from 'react';
import {
  Text,
  View,
  Alert,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import Modal from 'react-native-simple-modal';
import PreLoader from './PreLoader';
import NoDataFound from './NoDataFound';
import {connect} from 'react-redux';
import * as seachActions from '../actions/Search';
import {bindActionCreators} from 'redux';
import getCurrencyValue from './getCurrencyValue.js';
import StarRating from 'react-native-star-rating';
import API_URL from '../configuration/configuration';

const {width, height} = Dimensions.get('window');

class SpecialOffers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentCurrencyValue: 1,
    };
  }

  async componentDidMount() {
    const {currencyCode} = this.props.country;
    if (currencyCode !== 'SAR') {
      this.setState({
        currentCurrencyValue: await getCurrencyValue(currencyCode),
      });
    }
  }
  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  onAdClickHandler(item) {
    // console.warn('offer data-->>', item.ads_offers);

    if (item.ads_offers) {
      const {id, transaction_id} = item.ads_offers;
      // console.warn('transaction_id',transaction_id);
      const url = API_URL + '/update-offer-click-price';
      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          offerId: id,
          transactionId: transaction_id,
        }),
      })
        .then(response => response.json())
        .then(responseData => {
          // console.warn('update-offer-click-price res-->>', responseData)
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
    item.pinName = item.pins.pin_name;

    this.props.navigation.navigate('AdDetails', {item, pinDetails: item.pins});
  }

  render() {
    // console.warn('openModal-->>',this.props.navigation)
    const isRTL = this.props.isRTL;
    const {currencyCode} = this.props.country;
    const specialOfferData = this.props.specialOfferData;
    // console.warn('specialOfferData-->>',specialOfferData)
    const {currentCurrencyValue} = this.state;

    let offerList = [];
    if (specialOfferData.length && specialOfferData !== 'No Data') {
      offerList = specialOfferData.map((item, index) => {
        const isAdOffer = item.ads_have_offer; // Yes or No
        const offerDesc = item.offer_description;
        const discountAvailable = item.discount_avail;
        const discountPercentage = item.dis_percentage;
        const discountedPrice = item.discounted_price;
        let rating = item.rating;
        const pinName = item.pins.pin_name;
        return (
          <TouchableOpacity
            style={{borderBottomWidth: 1}}
            key={index}
            onPress={() => this.onAdClickHandler(item)}>
            <View style={styles.adOffer}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#cc0000',
                    textDecorationLine:
                      discountPercentage > '0' ? 'line-through' : '',
                  }}>
                  {parseFloat(item.add_price * currentCurrencyValue).toFixed(2)}{' '}
                  {currencyCode}
                </Text>
                {discountPercentage > 0 ? (
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text>{' / '}</Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#cc0000',
                      }}>
                      {parseFloat(
                        discountedPrice * currentCurrencyValue,
                      ).toFixed(2)}{' '}
                      {currencyCode}{' '}
                    </Text>
                    <Text>{'  '}</Text>
                    <Text style={{fontSize: 12, fontWeight: 'bold'}}>
                      {discountPercentage}% OFF
                    </Text>
                  </View>
                ) : null}
              </View>
              <Text
                style={{fontWeight: 'bold', fontSize: 16, color: '#cc0000'}}>
                {offerDesc}
              </Text>
            </View>
            <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
              <View style={{flex: 1.5}}>
                {item.ad_image && item.ad_image.length ? (
                  <Image
                    style={styles.image}
                    source={{uri: item.ad_image[0].image_name}}
                  />
                ) : (
                  <Image
                    style={styles.image}
                    source={require('../icons/blankImage.png')}
                  />
                )}
              </View>
              <View style={{flex: 2.6}}>
                <View style={{flexDirection: isRTL ? 'row-reverse' : 'row'}}>
                  <View style={{flex: 2}}>
                    <Text
                      style={[
                        styles.title,
                        {
                          fontSize: 16,
                          flexWrap: 'wrap',
                          fontWeight: 'bold',
                          textAlign: 'center',
                        },
                      ]}>
                      {this.Capitalize(item.product_name)}{' '}
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                      marginTop: 5,
                    }}>
                    {
                      //<Text style={[styles.price, {marginTop: 4}]}>{parseFloat(item.add_price*currentCurrencyValue).toFixed(2)} {currencyCode}</Text>
                    }
                    <Text style={[styles.price, {color: '#000000'}]}>
                      {' '}
                      {this.props.isRTL ? 'الحالة' : 'Condition: '}
                    </Text>
                    <Text style={styles.price}>{item.add_status}</Text>
                  </View>
                </View>
                <View style={{marginTop: 15}}>
                  <Text>
                    {this.props.isRTL ? ':تاجر' : 'Seller: '} {pinName}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        );
      });
    }
    return (
      <Modal
        open={this.props.modalOpen}
        closeOnTouchOutside={true}
        modalDidClose={() => this.props.openModal(false)}
        style={{alignItems: 'center', backgroundColor: '#000000'}}
        containerStyle={{marginTop: Platform.OS === 'ios' ? 70 : 60}}>
        <TouchableOpacity
          style={{alignItems: 'flex-end'}}
          onPress={() => {
            this.props.openModal(false);
          }}>
          <Image
            style={{width: 15, height: 15}}
            source={require('../icons/close.jpeg')}
          />
        </TouchableOpacity>

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>
            {isRTL ? 'عروض خاصة' : 'Special Offers'}
          </Text>
        </View>
        <View>
          {!specialOfferData.length ? (
            <PreLoader />
          ) : specialOfferData === 'No Data' ? (
            <Text>
              {isRTL ? 'لا يوجد عرض' : 'Currently, no special offers available'}
            </Text>
          ) : (
            <ScrollView style={styles.container}>{offerList}</ScrollView>
          )}
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // flex:1,
    marginTop: 10,
    marginBottom: 15,
  },
  offerList: {},
  adOffer: {
    marginTop: 10,
    marginBottom: 10,
  },
  image: {
    flex: 1,
    marginRight: 10,
    borderWidth: 2,
    width: width <= 320 ? 60 : 100,
    height: height <= 470 ? 60 : 100,
    resizeMode: 'contain',
  },
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    modalOpen: state.search.modalOpen,
    specialOfferData: state.search.specialOfferData,
    country: state.network.countryLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...seachActions}, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SpecialOffers);
