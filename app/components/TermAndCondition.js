import React, { Component } from 'react';
import Header from './Header';
import {Text, View, StyleSheet} from 'react-native';
import { WebView } from 'react-native-webview';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import PreLoader from './PreLoader';
import { WEB_URL } from '../configuration/configuration';

class Policy extends Component { 

	constructor(props) {
		super();
		this.state = {
			isLoading : true
		}
	}

  	static navigationOptions = ({ navigation }) => {
		let {state} = navigation;
		if (state.params !== undefined) {
			return {
    		title: (state.params.isRTL === true) ? 'الأحكام والشروط' : 'Terms and Conditions'
    }}
	}

	componentDidMount() {
		const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
    	this.props.navigation.setParams({ isRTL: nextProps.isRTL });
    	this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}

	handleLoading = () => {
		this.setState({isLoading: false})
	}

	getContent() {
		const isRTL = this.props.isRTL;
		console.warn("isRTL ->",isRTL);
		if(isRTL) {
			console.warn("isRTL arab ->",isRTL);
			return <WebView
				onLoad={this.handleLoading}
		        source={{uri: WEB_URL+'/termCondition/arb/app'}}			        
			/>	
		} else {
			console.warn("isRTL eng ->",isRTL);
			return <WebView
				onLoad={this.handleLoading}
		        source={{uri: WEB_URL+'/termCondition/eng/app'}}			        
			/>	
		}
	}

	render() {
		console.warn(" render isRTL ->",isRTL);
		return (			
				<View style={{ flex: 1, backgroundColor: '#FFF' }}>	
					{this.state.isLoading && <PreLoader />}
					{this.getContent()}					
			   </View>	
		)
	}
}

const styles = StyleSheet.create({
		container:{
			flex: 1,
			justifyContent: 'space-between',
			alignItems:'center',
			backgroundColor:'#ecf2f9'
		}
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Policy)
