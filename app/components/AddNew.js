import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
  AsyncStorage,
  Image,
  Platform,
  Picker,
  DatePickerIOS,
  KeyboardAvoidingView,
  Dimensions,
  Keyboard,
} from 'react-native';
import Header from './Header';
import ImagePicker from 'react-native-image-crop-picker';
import ModalDropdown from 'react-native-modal-dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import MultiSelect from 'react-native-multiple-select';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AddNew';
import NotificationHeader from './NotificationHeader';
import GetPackages from './GetPackages';
import PreLoader from './PreLoader';
import getCurrencyValue from './getCurrencyValue.js';
import DeviceInfo from 'react-native-device-info';
import RNFetchBlob from 'rn-fetch-blob';
import ImageResizer from 'react-native-image-resizer';
import getUserBalance from './getUserBalance';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

import RNIap, {
  Product,
  ProductPurchase,
  acknowledgePurchaseAndroid,
  purchaseUpdatedListener,
  purchaseErrorListener,
  PurchaseError,
} from 'react-native-iap';
import {PAY,CURRENCY} from "./Payment_Conf";

const items = Platform.select({
  ios: [
    'com.package.first',
    'com.package.second',
    'com.package.third',
    'com.package.fourth',
  ],
  android: [''],
});

const {width, height} = Dimensions.get('window');

class AddNew extends Component {
  static navigationOptions = ({navigation}) => {
    let state = navigation.state;
    if (state.params !== undefined) {
      return {
        title: state.params.isRTL === true ? 'اضف جديد' : 'Add New',
        headerRight: <NotificationHeader navigation={navigation} />,
      };
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      userId: null,
      token: '',
      pinId: '',
      pins: [],
      pinName: '',
      messageType: '',
      title: '',
      description: '',
      fileSource: '',
      photos: '',
      isDatePickerVisible: false,
      startDate: '',
      endDate: '',
      isendDatePickerVisible: false,
      campaign: '',
      highLightField: '',
      manageCom: [],
      packageIndex: 0,
      packageName: '',
      cost: '',
      selectedItems: [],
      packages: '',
      isLoading: false,
      balance: 0,
      payableAmount: 0,
      currentCurrencyValue: 1,
      showIosDate: false,
      isIosPicker: false,
      startDateIOS: new Date(),
      endDateIOS: new Date(),
      isEndIosPicker: false,
      showIosEndDate: false,
      promotionIndex: 0,
    };

    this.uploadImage = this.uploadImage.bind(this);
    this.selectedCategory = this.selectedCategory.bind(this);
    this.selectedPin = this.selectedPin.bind(this);
    this.package = '';
  }

  purchaseUpdateSubscription = null;
  purchaseErrorSubscription = null;

  async componentDidMount() {
    // this.props.navigation.addListener('didFocus', async event => {
    // 	this.props.navigation.goBack();
    // });
    const {setParams} = this.props.navigation;
    const {currencyCode} = this.props.country;
    setParams({isRTL: this.props.isRTL});

    //*************************************************************
    /**************	Apple in app purchage code start  *************/

    if (Platform.OS == 'ios') {
      try {
        const iOSProducts = await RNIap.getProducts(items);
        // console.warn("TCL: componentDidMount -> products", iOSProducts );
        this.setState({iOSProducts});
      } catch (err) {
        Alert.alert('purchase error', JSON.stringify(err));
      }

      this.purchaseUpdateSubscription = purchaseUpdatedListener(purchase => {
        // console.warn('purchaseUpdatedListener', purchase);
        const receipt = purchase.transactionReceipt;
        if (receipt) {
          if (Platform.OS === 'ios') {
            RNIap.finishTransactionIOS(purchase.transactionId);
          } else {
            // Retry / conclude the purchase is fraudulent, etc...
          }
        }
      });

      this.purchaseErrorSubscription = purchaseErrorListener(error => {
        this.setState({isPurchased: false});
        // console.warn('purchaseErrorListener', error);
        Alert.alert('purchase error', JSON.stringify(error));
      });
    }

    /********************** Apple in app purchage code end  ****************/
    //***********************************************************************

    const appName = DeviceInfo.getApplicationName();
    const deviceName = DeviceInfo.getDeviceName();
    const deviceId = DeviceInfo.getDeviceId();
    this.setState({appName, deviceId, deviceName});

    AsyncStorage.getItem('isProfileCompleted').then(json => {
      const profileData = JSON.parse(json);
      // console.warn('profileData-->>',profileData)
      this.setState({userProfile: profileData.userProfile});
    });

    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    this.setState({
      token: token,
      packages: await GetPackages(token),
      //currentCurrencyValue: await getCurrencyValue(currencyCode),
    });

    const userBalance = await getUserBalance(token);
    this.setState({balance: userBalance.balance});

    if (currencyCode !== 'SAR') {
      this.setState({
        currentCurrencyValue: await getCurrencyValue(currencyCode),
      });
    }
    this.getCommercialList();
    this.getUserAllPin();
  }

  getCommercialList() {
    AsyncStorage.getItem('user_data').then(data => {
      const userData = JSON.parse(data);
      const token = userData.sData;
      this.setState({token: token, userId: userData.userId});
      fetch(API_URL + '/commercial-list', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: this.state.token,
        },
      })
        .then(response => response.json())
        .then(responseData => {
          // console.warn('responseData', responseData)
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الوصول الى قائمة الإعلانات التجارية '
              : 'Failed to get commercial list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 805465 &&
            responseData.eData === null
          ) {
            // console.warn('comm--->>>',responseData)
            this.setState({
              manageCom: responseData.list,
              // packageName: responseData.list[0].packages,
              messageType: this.props.isRTL
                ? responseData.list[0].arb_name
                : responseData.list[0].commercial_name,
              promotionIndex: 0,
            });
            // setTimeout(() => {console.warn(this.state.dataSource.length);}, 300);
          }
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    });
  }
  getUserAllPin() {
    const add = 'yes';
    const {isRTL} = this.props;
    fetch(API_URL + '/usersAllPin?ad=yes', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('pins-->>',responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول لنقطة البيع'
            : 'Failed to get Sales Points';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          const items = [];
          responseData.allPins.map(item => {
            const data = (({id, pin_name}) => ({id, pin_name}))(item);
            items.push(data);
          });
          if (items.length) {
            this.setState({pins: items});
          }
        } else if (
          responseData.status == 654864 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          const message = isRTL
            ? 'لا يوجد نقاط بيع في حسابك '
            : 'There is no Sales Point in your account';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(err => {
        //console.warn(err);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  _showDateTimePicker = () => {
    // console.warn("show picker");
    this.setState({
      isDatePickerVisible: true,
      highLightField: 'startDate',
    });
  };
  _hideDateTimePicker = () =>
    this.setState({isDatePickerVisible: false, highLightField: ''});

  onFocus(status) {
    this.setState({highLightField: status});
  }

  _handleDatePicked = date => {
    //console.warn('TCL: date', date)

    const chosenDate = moment(date).format('YYYY-MM-DD');
    //console.warn('TCL: chosenDate', chosenDate)

    this.setState({startDate: chosenDate, highLightField: '', endDate: ''});
    this._hideDateTimePicker();
  };

  _showendDateTimePicker = () =>
    this.setState({
      isendDatePickerVisible: true,
      highLightField: 'endDate',
    });

  _hideendDateTimePicker = () =>
    this.setState({isendDatePickerVisible: false, highLightField: ''});

  _handleendDatePicked = date => {
    const chosenDate = moment(date).format('YYYY-MM-DD');
    this.setState({endDate: chosenDate, highLightField: ''});
    this._hideendDateTimePicker();
  };

  _handleSubmit = () => {
    const {currencyCode} = this.props.country;
    const userId = this.state.userId;
    const payableAmount = this.state.payableAmount;
    const {isRTL} = this.props;
    if (isNaN(payableAmount)) {
      const message = isRTL
        ? 'لم يتم إيجاد المبلغ'
        : 'Payable amount not found.';
      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
      return;
    }
    if (!currencyCode) {
      const message = isRTL
        ? 'لم يتم إيجاد العملة '
        : 'Currency code not found.';
      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
      return;
    }

    this.setState({isLoading: true});
    // const { navigate } = this.props.navigation;
    const commercial_name = this.state.messageType;
    const comm = this.state.manageCom.filter(
      item =>
        item.commercial_name === commercial_name ||
        item.arb_name === commercial_name,
    );
    // console.warn("TCL: _handleSubmit -> comm", comm);

    //console.warn('_handleSubmit....')
    // const myPromotion = this.state.messageType.split(' ');
    // const cost = myPromotion[myPromotion.length - 1];
    // var com_cost = cost.replace(/[()]/, '');
    // var com_id = myPromotion[0].replace(/[.]/, '') === "Toast" ? 1 : 2;

    const {id, cost} = comm[0]; // here find commertial Id and cost
    const promotionIndex = this.state.promotionIndex;
    const url = API_URL + '/create-new';
    //post the pin details
    const data = {
      pinIds: this.state.selectedItems,
      userId: this.state.userId,
      title: this.state.title,
      description: this.state.description,
      upload_media: this.state.fileSource,
      campaign_budget: this.state.campaign,
      commercial_id: id,
      commercial_name: this.state.manageCom[promotionIndex].commercial_name,
      start_date:
        Platform.OS === 'ios' ? this.state.startDateIOS : this.state.startDate,
      end_date:
        this.state.messageType.toLowerCase() == 'push notifications' ||
        this.state.messageType.trim() == 'تنبيهات الجوال'
          ? Platform.OS === 'ios'
            ? this.state.startDateIOS
            : this.state.startDate
          : Platform.OS === 'ios'
          ? this.state.endDateIOS
          : this.state.endDate,
      cost: cost,
    };
    //console.warn('TCL: data', data.start_date," ",data.end_date)
    // this.goForPayment('123456')
    //return;

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn('responseData-->>', responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 236486
        ) {
          const message = isRTL
            ? 'فشل في إضافة اعلان تجاري'
            : 'Failed to add commercial';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          this.setState({isLoading: false});
        } else if (
          responseData.status == 502368 &&
          responseData.eData.eCode == 684605
        ) {
          Alert.alert('Error', responseData.eData.errors.toString());
          this.setState({isLoading: false});
        } else if (
          responseData.status == 805465 &&
          responseData.eData === null
        ) {
          const orderId = responseData.orderId;
          const tId = responseData.tId;
          const cTid = responseData.cTid;
          // this.setState({
          // 	startDate: '',
          // 	endDate: '',
          // 	title: '',
          // 	description: '',
          // 	fileSource: '',
          // 	photos: '',
          // 	campaign: '',
          // 	cost: '',
          // 	selectedItems: []
          // });
          //Alert.alert('Success', 'Wait untill your request approves.');
          this.goForPayment(
            orderId,
            tId,
            cTid,
            currencyCode,
            payableAmount,
            userId,
          );
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  };

  getResponseFromPaymentGateway = () => {
    this.props.navigation.goBack();
  };

  updateWalletBalance = (
    deductedAmount,
    remainingBalance,
    paymentStatus,
    orderId,
    tId,
    cTid,
  ) => {
    const {isRTL} = this.props;

    if (deductedAmount === 0) {
      return;
    }

    fetch(API_URL + '/update-wallet-balance', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        type: 'Commercials',
        reason: 'Payment for applying commercial',
        deduct_amount: deductedAmount,
        remaining_balance: remainingBalance,
        status: paymentStatus,
        orderId,
        tId,
        cTid,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('responseData-->>',responseData)
        if (responseData.status == 200) {
          const message = isRTL
            ? 'تم ارسال طلب الإعلان التجاري بنجاح'
            : 'Commercial request submitted successfully!';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          this.setState({isLoading: false});
          this.props.navigation.goBack();
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  };

  goForPayment = (orderId, tId, cTid, currencyCode, payableAmount, userId) => {
    const {isRTL} = this.props;
    const {appName, deviceId, deviceName} = this.state;
    const {name, email} = this.state.userProfile;
    const {deductedAmount, remainingCommercialBalance} = this.state;

    // let paymentStatus = payableAmount === 0 ? "Wallet" : "PaymentGateway";
    let paymentStatus = 'Full';

    if (payableAmount === 0) {
      this.updateWalletBalance(
        deductedAmount,
        remainingCommercialBalance,
        paymentStatus,
        orderId,
        tId,
        cTid,
      );
      /** Here no need to go to payment gateway because payable amout is zero. */
      return 0;
    }

    if (!email || !name) {
      const message = isRTL
        ? 'يرجى اكمال ملفك الشخصي'
        : 'Please complete your profile first';
      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

      return;
    }

    if (Platform.OS == 'ios') {
      const appleInAppKey = this.state.appleInAppKey; // This is apple in app purchage key

      RNIap.requestPurchase(appleInAppKey)
        .then(purchase => {
          //console.warn('purchase Ios resp ->', purchase);
          this.sendPurchaseData(purchase, orderId, tId, cTid, payableAmount);
        })
        .catch(error => {
          // console.warn(error.message);
          Alert.alert('Error', error.message);
        });
    } else {
      const url = 'https://secure.innovatepayments.com/gateway/mobile.xml';
      const test = PAY;
      const currency_test = CURRENCY;
      const data1 =
        '<?xml version="1.0" encoding="UTF-8"?>\
			<mobile>\
			   	<store>20793</store>\
			 	<key>KnXhH-xHFZV#3FNf</key> \
			   	<device>\
				    <type>' +
        deviceName +
        '</type>\
				    <id>' +
        deviceId +
        '</id>\
				    <agent></agent>\
				    <accept></accept>\
			  	</device>\
			   	<app>\
				    <name>' +
        appName +
        '</name>\
				    <version>beta</version>\
				     <user>' +
        name +
        '</user>\
				    <id>' +
        userId +
        '</id>\
			  	</app>\
			  <tran>\
			  <test>'+test+'</test>\
			  <type>PAYPAGE</type>\
			  <cartid>' +
        orderId +
        '</cartid>\
			  <description>Transaction description</description>\
			   <currency>' +
        currency_test +
        '</currency>\
			    <amount>' +
        payableAmount +
        '</amount>\
			  </tran>\
			  <billing>\
			    <email>' +
        email +
        '</email>\
			  </billing>\
			</mobile>';

      fetch(url, {
        method: 'POST',

        headers: {
          Accept: 'application/xml',
          'Content-Type': 'text',
        },
        body: data1,
      })
        .then(response => response.text())
        .then(responseData => {
          // console.warn('xml response-->>',responseData)
          //console.warn('xml response-->>',responseData.match('<start>(.*)</start>')[1])
          const start = responseData.match('<start>(.*)</start>')[1];
          const close = responseData.match('<close>(.*)</close>')[1];
          const abort = responseData.match('<abort>(.*)</abort>')[1];
          const code = responseData.match('<code>(.*)</code>')[1];
          const urls = {start, close, abort, code};

          this.props.navigation.navigate('Payment', {
            urls,
            orderId,
            tId,
            cTid,
            deductedAmount,
            remainingCommercialBalance,
            paymentStatus,
            type: 'commercial',
            token: this.state.token,
            onGoBack: () => this.getResponseFromPaymentGateway(),
          });
        })
        .catch(err => {
          Alert.alert('Error', err.message);
        });
    }
  };

  sendPurchaseData = (purchase, orderId, tId, cTid, payableAmount) => {
    const iOSTransactionData = {
      iosReceipt: purchase.transactionReceipt,
      transactionDate: purchase.originalTransactionDateIOS,
      transactionId: purchase.transactionId,
      productId: purchase.productId,
      planId: this.state.appleInAppKey,
      planType: 'Commercial Request',
      orderId: orderId,
      discounted_price: payableAmount,
      cTid,
      tId,
    };

    fetch(API_URL + '/apple-promotion-payment', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify(iOSTransactionData),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn('payment api resp --->>',responseData);
        this.props.navigation.goBack();
        // this.setState({isPurchased:false});
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  };

  uploadImage() {
    ImagePicker.openPicker({
      includeBase64: true,
      compressImageQuality: 0.2,
    }).then(r => {
      const str = r.path;
      var filename = str.replace(/^.*[\\\/]/, '');
      let img = r.data;

      this.setState({
        fileSource: img,
        photos: filename,
      });
    });
  }

  async convertIntoBase64(fileUri) {
    return RNFetchBlob.fs.readFile(fileUri, 'base64').then(async data => {
      return data;
    });
  }

  selectedCategory(index, commercialName) {
    // console.warn("msgtype-->>",index, commercialName,);
    //const messageType = commercialName.split('(');
    //console.warn("msgtype", messageType);
    this.setState({messageType: commercialName, promotionIndex: index});
  }
  selectedPin(value) {
    const name = value.pin_name;
    this.setState({
      pinName: name,
      pinId: value.id,
    });
  }

  onSelectedItemsChange(selectedItems) {
    this.setState({selectedItems});
  }

  selectPackage = (index, value) => {
    const cost = this.state.packages[index].package_cost;

    const appleInAppKey = this.state.packages[index].key;

    let selectPackgae = cost;
    let commercialBalance = this.state.balance;

    let payableAmount;
    let remainingCommercialBalance;
    let deductedAmount;
    //console.warn(commercialBalance, selectPackgae)
    if (commercialBalance >= selectPackgae) {
      payableAmount = 0;
      deductedAmount = selectPackgae;
      remainingCommercialBalance = commercialBalance - selectPackgae;
    } else {
      payableAmount = selectPackgae;
      deductedAmount = 0;
      remainingCommercialBalance = commercialBalance;
    }
    // console.warn(payableAmount, commercialBalance)
    //this.setState({selectPackgae: packageValue, packageIndex, payableAmount})
    this.setState({
      campaign: cost,
      payableAmount,
      deductedAmount,
      remainingCommercialBalance,
      appleInAppKey,
    });
  };

  setDateIOS = newDate => {
    this.setState({startDateIOS: newDate});
  };

  setEndDateIOS = newDate => {
    this.setState({endDateIOS: newDate});
  };

  showIosDatePicker = () => {
    this.setState({isIosPicker: true, showIosDate: true});
  };

  hideIosDatePicekr = () => {
    this.setState({isIosPicker: false});
  };

  showIosEndDatePicker = () => {
    this.setState({isEndIosPicker: true, showIosEndDate: true});
  };

  hideIosEndDatePicekr = () => {
    this.setState({isEndIosPicker: false});
  };

  render() {
    const isRTL = this.props.isRTL;
    const {currencyCode} = this.props.country;
    const commercialName = this.state.messageType;
    // console.warn('promotionIndex-->>', this.state.promotionIndex);
    const promotionIndex = this.state.promotionIndex;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {selectedItems, currentCurrencyValue, balance} = this.state;
    //let items = [];
    let costPerClick = 0;
    const usebalance = balance
      ? (balance * currentCurrencyValue).toFixed(2)
      : 0;
    console.warn('usebalance: ', usebalance);
    const data =
      this.state.manageCom.length &&
      this.state.manageCom.map((item, index) => {
        if (
          commercialName == item.commercial_name ||
          commercialName == item.arb_name
        )
          costPerClick = item.cost;
        return isRTL ? item.arb_name : item.commercial_name;
      });
    //	console.warn("TCL: render -> data", data);

    //	const dataRTL = this.props.isRTL ? data === 'Toast Message' ? 'رسالة ترويجية' : 'تنبيهات للجوال' : null;
    const clicks = this.props.isRTL ? 'نقرات' : 'Clicks';
    const packageName =
      (this.state.packages.length &&
        this.state.packages.map(
          item =>
            `${Math.ceil(item.package_cost) / costPerClick} ${clicks} (${(
              item.package_cost * currentCurrencyValue
            ).toFixed(2)} ${currencyCode})`,
        )) ||
      [];
    // console.warn('packageName-->>', packageName)
    return (
      <ScrollView
        style={{padding: 10, backgroundColor: '#ecf2f9'}}
        keyboardShouldPersistTaps="handled">
        <View
          style={[
            styles.chooseMessage,
            {
              flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
              justifyContent: 'space-between',
            },
          ]}>
          {!!data.length && (
            <ModalDropdown
              defaultValue={data[this.state.packageIndex]}
              options={data}
              adjustFrame={() => ({
                width: '80%',
                height: 110,
                right: 30,
                left: 20,
                top: 100,
              })}
              dropdownTextStyle={{
                fontSize: 17,
                marginHorizontal: 25,
                fontWeight: 'bold',
              }}
              textStyle={{
                fontSize: 17,
                marginTop: Platform.OS === 'ios' ? 14 : 18,
                marginHorizontal: 25,
                color: '#7A7A7A',
                fontWeight: 'bold',
              }}
              dropdownStyle={{
                height: 120,
                width: '85%',
                marginHorizontal: '5%',
                paddingTop: 5,

                marginTop: Platform.OS === 'ios' ? 15 : 12,
                // borderBottomLeftRadius: 25,
                // borderBottomRightRadius: 25,
                borderRadius: 25,
                //borderColor: '#7A7A7A'
              }}
              showsVerticalScrollIndicator={false}
              onSelect={this.selectedCategory}
            />
          )}
        </View>

        {/* {!!data.length && (
						<Image
							style={{
								marginRight: this.props.isRTL ? 0 : 20,
								marginTop: 20,
								marginLeft: this.props.isRTL ? 20 : 0
							}}
							source={require('./../icons/down_arrow2x.png')}
						/>
					)} */}

        <View style={styles.tostMsg}>
          {commercialName.trim() === 'Toast Message' ||
          commercialName.trim() === 'رسائل ترويجية' ? (
            <View style={styles.tostMsg}>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '1. ' +
                    'عند فتح المستخدم للتطبيق او موقع الانترنت، سيتم اخذالمستخدم الى أحد نقاط البيع المحددة وإظهار رسالة ترويجية فوقها'
                  : '1. A message will appear over you sales point when user opens the app or website to promote your sales point and only successful clicks will be counted.'}
              </Text>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '2. ' +
                    'يتم احتساب الخصم في حال قام المستخدم بنق الرسالة الترويجية ولا يتم الخصم في حال تم العرض فقط من دون نقر المستخدم'
                  : '2. Click rate will apply only if user clicked your toast message'}
              </Text>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '3. ' + 'تكلفة النقرة '
                  : '3. Cost per click -'}{' '}
                {`${(costPerClick * currentCurrencyValue).toFixed(
                  2,
                )} ${currencyCode}`}
              </Text>
            </View>
          ) : null}

          {commercialName.trim() === 'Push Notifications' ||
          commercialName.trim() === 'تنبيهات الجوال' ? (
            <View style={styles.tostMsg}>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '1. ' +
                    'سيتم ارسال تنبيهات على جوالات المستخدمين في مدينتك￼بحسب عدد نقرات المحددة المطلوبة '
                  : '1.  Will send notification to all users within your city to promoted your sales point'}
              </Text>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '2. ' + 'يتم الخصم في حال قام المستخدم بنقر التنبيه فقط'
                  : '2.  Rate will only apply on successful clicks'}
              </Text>
              <Text style={{textAlign: this.props.isRTL ? 'right' : null}}>
                {this.props.isRTL
                  ? '3. ' + 'تكلفة النقرة'
                  : '3. Cost per click'}{' '}
                -{' '}
                {`${(costPerClick * currentCurrencyValue).toFixed(
                  2,
                )} ${currencyCode}`}
              </Text>
            </View>
          ) : null}
        </View>
        {Platform.OS === 'ios' ? (
          <View style={[styles._textbox, {}]}>
            <TouchableOpacity
              onPress={this.showIosDatePicker}
              style={{
                flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                justifyContent: 'space-between',
              }}>
              <View
                style={[
                  styles._textbox,
                  this.props.isRTL ? {flexDirection: 'row-reverse'} : null,
                  {
                    marginTop: 0,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  },
                ]}>
                {this.state.showIosDate ? (
                  <Text
                    style={[
                      styles._textboxText,
                      {marginTop: 2, color: 'grey'},
                    ]}>
                    {this.state.startDateIOS.toDateString()}
                  </Text>
                ) : (
                  <Text
                    style={[
                      styles._textboxText,
                      {marginTop: 2, color: 'grey'},
                    ]}>
                    {strings('addNew.start_date')}
                  </Text>
                )}
                <Image
                  style={{
                    marginRight: this.props.isRTL ? 15 : 15,
                    marginVertical: 8,
                    marginLeft: this.props.isRTL ? 15 : 0,
                    resizeMode: 'contain',
                    width: 20,
                  }}
                  source={require('./../icons/calender2x.png')}
                />
              </View>
            </TouchableOpacity>
            {this.state.isIosPicker && (
              <View
                style={{
                  backgroundColor: 'white',
                  marginTop: 1,
                  borderRadius: 20,
                }}>
                <DatePickerIOS
                  date={this.state.startDateIOS}
                  onDateChange={this.setDateIOS}
                  minimumDate={new Date()}
                  mode="date"
                />
                <TouchableOpacity onPress={this.hideIosDatePicekr}>
                  <View style={{justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: 25,
                        fontWeight: 'bold',
                        marginHorizontal: '45%',
                        marginBottom: 5,
                      }}>
                      OK
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        ) : (
          <View
            style={[
              styles._textbox,

              this.state.highLightField === 'startDate'
                ? {borderWidth: 2, borderColor: '#71c1f2'}
                : {},
            ]}>
            <TouchableOpacity
              onPress={this._showDateTimePicker}
              style={{
                flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                justifyContent: 'space-between',
              }}>
              {this.state.startDate ? (
                <Text style={[styles._textboxText, {color: 'grey'}]}>
                  {this.state.startDate}
                </Text>
              ) : (
                <Text style={[styles._textboxText, {color: 'grey'}]}>
                  {strings('addNew.start_date')}
                </Text>
              )}
              <Image
                style={{
                  marginRight: this.props.isRTL ? 0 : 15,
                  marginVertical: 8,
                  marginLeft: this.props.isRTL ? 15 : 0,
                  resizeMode: 'contain',
                  width: 20,
                }}
                source={require('./../icons/calender2x.png')}
              />
            </TouchableOpacity>
            <DateTimePicker
              format="YYYY/MM/DD"
              isVisible={this.state.isDatePickerVisible}
              onConfirm={this._handleDatePicked.bind(this)}
              onCancel={this._hideDateTimePicker.bind(this)}
              minimumDate={new Date()}
            />
          </View>
        )}

        {this.state.manageCom.length &&
        this.state.manageCom[promotionIndex].commercial_name.toLowerCase() !=
          'push notifications' ? (
          Platform.OS === 'ios' ? (
            <View
              style={[
                styles._textbox,
                this.state.isIosPicker ? {marginTop: 260} : {marginTop: 10},
                {},
              ]}>
              <TouchableOpacity
                onPress={this.showIosEndDatePicker}
                style={{
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  justifyContent: 'space-between',
                }}>
                <View
                  style={[
                    styles._textbox,
                    this.props.isRTL ? {flexDirection: 'row-reverse'} : null,
                    {
                      marginTop: 0,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    },
                  ]}>
                  {this.state.showIosEndDate ? (
                    <Text
                      style={[
                        styles._textboxText,
                        {marginTop: 2, color: 'grey'},
                      ]}>
                      {this.state.endDateIOS.toDateString()}
                    </Text>
                  ) : (
                    <Text
                      style={[
                        styles._textboxText,
                        {marginTop: 2, color: 'grey'},
                      ]}>
                      {strings('addNew.end_date')}
                    </Text>
                  )}
                  <Image
                    style={{
                      marginRight: this.props.isRTL ? 15 : 15,
                      marginVertical: 8,
                      marginLeft: this.props.isRTL ? 15 : 0,
                      resizeMode: 'contain',
                      width: 20,
                    }}
                    source={require('./../icons/calender2x.png')}
                  />
                </View>
              </TouchableOpacity>
              {this.state.isEndIosPicker && (
                <View
                  style={{
                    backgroundColor: 'white',
                    marginTop: 1,
                    borderRadius: 20,
                  }}>
                  <DatePickerIOS
                    date={this.state.endDateIOS}
                    onDateChange={this.setEndDateIOS}
                    minimumDate={new Date()}
                    mode="date"
                  />
                  <TouchableOpacity onPress={this.hideIosEndDatePicekr}>
                    <View style={{justifyContent: 'center'}}>
                      <Text
                        style={{
                          fontSize: 25,
                          fontWeight: 'bold',
                          marginHorizontal: '45%',
                          marginBottom: 5,
                        }}>
                        OK
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          ) : (
            <View
              style={[
                styles._textbox,
                this.state.isIosPicker ? {marginTop: 260} : {marginTop: 10},
                this.state.highLightField === 'endDate'
                  ? {borderWidth: 2, borderColor: '#71c1f2'}
                  : {},
              ]}>
              <TouchableOpacity
                onPress={this._showendDateTimePicker}
                style={{
                  flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                  justifyContent: 'space-between',
                }}>
                {this.state.endDate ? (
                  <Text style={styles._textboxText}>{this.state.endDate}</Text>
                ) : (
                  <Text style={styles._textboxText}>
                    {strings('addNew.end_date')}
                  </Text>
                )}
                <Image
                  style={{
                    marginRight: this.props.isRTL ? 0 : 15,
                    marginVertical: 8,
                    resizeMode: 'contain',
                    width: 20,
                    marginLeft: this.props.isRTL ? 15 : 0,
                  }}
                  source={require('./../icons/calender2x.png')}
                />
              </TouchableOpacity>
              <DateTimePicker
                format="YYYY/MM/DD"
                isVisible={this.state.isendDatePickerVisible}
                onConfirm={this._handleendDatePicked.bind(this)}
                onCancel={this._hideendDateTimePicker.bind(this)}
                minimumDate={moment(
                  this.state.startDate,
                  'YYYY-MM-DD',
                ).toDate()}
              />
            </View>
          )
        ) : null}

        <View
          style={[
            styles._textbox,
            this.state.highLightField === 'title'
              ? {borderWidth: 2, borderColor: '#71c1f2'}
              : {},
            // this.state.isEndIosPicker ? {marginTop:260} : {marginTop:10},
            commercialName.toLowerCase().trim() === 'push notifications' ||
            commercialName.trim() === 'تنبيهات الجوال'
              ? this.state.isIosPicker
                ? {marginTop: 260}
                : {marginTop: 10}
              : this.state.isEndIosPicker
              ? {marginTop: 260}
              : {marginTop: 10},
          ]}>
          <TextInput
            style={styles._textboxText}
            onChangeText={text => this.setState({title: text})}
            style={styles._textboxText}
            onFocus={() => this.onFocus('title')}
            onBlur={() => this.setState({highLightField: ''})}
            underlineColorAndroid="white"
            keyboardType={'default'}
            placeholderTextColor="#7A7A7A"
            placeholder={strings('addNew.title')}
          />
        </View>
        {commercialName.trim() === 'Toast Message' ||
        commercialName.trim() === 'رسائل ترويجية' ? (
          <View
            style={[
              styles._descriptionbox,
              this.state.highLightField === 'description'
                ? {borderWidth: 2, borderColor: '#71c1f2'}
                : {},
            ]}>
            <TextInput
              style={styles._textboxText}
              onChangeText={text => this.setState({description: text})}
              multiline={true}
              onFocus={() => this.onFocus('description')}
              onBlur={() => this.setState({highLightField: ''})}
              underlineColorAndroid="white"
              numberOfLines={2}
              textAlignVertical="top"
              keyboardType={'default'}
              placeholderTextColor="#7A7A7A"
              placeholder={strings('addNew.description')}
              maxLength={100}
              blurOnSubmit={true}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
              returnKeyType="done"
            />
          </View>
        ) : null}
        {commercialName.trim() === 'Toast Message' ||
        commercialName.trim() === 'رسائل ترويجية' ? (
          <View style={[styles._textbox]}>
            <TouchableOpacity
              onPress={this.uploadImage}
              style={{
                flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                justifyContent: 'space-between',
              }}>
              {this.state.photos ? (
                <TextInput
                  ellipsizeMode="tail"
                  underlineColorAndroid="white"
                  numberOfLines={1}
                  style={[styles._textboxText, {flex: 1, color: 'grey'}]}>
                  {this.state.photos}
                </TextInput>
              ) : (
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={1}
                  style={[styles._textboxText, {flex: 1, color: 'grey'}]}>
                  {this.props.isRTL ? 'تحميل الصور' : 'Upload Media'}
                </Text>
              )}

              <Image
                style={{
                  marginRight: this.props.isRTL ? 0 : 15,
                  marginVertical: 8,
                  marginLeft: this.props.isRTL ? 15 : 0,
                  resizeMode: 'contain',
                  width: 20,
                }}
                source={require('./../icons/upload2x.png')}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        <View style={{marginTop: 10, width: '100%', borderRadius: 25}}>
          <MultiSelect
            style={{borderColor: 'red', width: 3, borderRadius: 25}}
            hidetags
            items={this.state.pins}
            uniqueKey="id"
            ref={component => {
              this.multiSelect = component;
            }}
            onSelectedItemsChange={this.onSelectedItemsChange.bind(this)}
            selectedItems={selectedItems}
            displayKey="pin_name"
            returnKeyType="done"
            selectText={
              this.props.isRTL
                ? 'اختيار نقاط البيع'
                : 'First attached pin name, Second attached pin name'
            }
            searchInputPlaceholderText="Pick Your Pin"
            fontSize={15}
            tagRemoveIconColor="#00ADDE"
            tagBorderColor="#00ADDE"
            tagTextColor="#00ADDE"
            selectedItemTextColor="#CCC"
            selectedItemIconColor="#CCC"
            itemTextColor="#000"
            searchInputStyle={{
              flexDirection: 'row',
              borderRadius: 25,
              justifyContent: 'space-between',
            }}
            submitButtonColor="#00ADDE"
            submitButtonText={strings('addNew.submit')}
          />
        </View>

        <View
          style={[
            styles.chooseMessage,
            {
              flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
              justifyContent: 'space-between',
            },
          ]}>
          <ModalDropdown
            defaultValue={
              this.props.isRTL
                ? 'عدد النقرات المطلوبة'
                : 'Target Number of user clicks'
            }
            options={packageName}
            onSelect={this.selectPackage}
            adjustFrame={() => ({
              width: '80%',
              height: 140,
              right: 30,
              left: 20,
              top: height / 2,
            })}
            dropdownTextStyle={{
              fontSize: 17,
              marginHorizontal: 25,
              fontWeight: 'bold',
            }}
            textStyle={{
              fontSize: 17,
              marginTop: 12,
              marginHorizontal: 25,
              color: '#7A7A7A',
              fontWeight: 'bold',
              textAlign: 'center',
            }}
            dropdownStyle={{
              height: 125,
              width: '83%',
              borderRadius: 25,
              left: 50,
              right: 150,
              marginHorizontal: '5%',
              //	marginRight: this.props.isRTL ? "50%" : 0,
              marginBottom: 50,
              paddingBottom: 10,
            }}
            showsVerticalScrollIndicator={false}
          />
        </View>

        <View
          style={{
            flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={styles._textboxText}>
            {strings('addNew.balance')}: {usebalance} {currencyCode}
          </Text>
        </View>
        <View style={{flexDirection: this.props.isRTL ? 'row-reverse' : 'row'}}>
          {this.state.campaign ? (
            <View
              style={{flexDirection: this.props.isRTL ? 'row-reverse' : 'row'}}>
              <Text style={styles._textboxText}>
                {' '}
                {this.props.isRTL ? 'المبلغ المستحق' : 'Payable Amount'} :{' '}
                {(this.state.payableAmount * currentCurrencyValue).toFixed(2)}{' '}
                {currencyCode}
              </Text>
            </View>
          ) : null}
        </View>

        {this.state.isLoading ? (
          <View style={{marginBottom: 100}}>
            <PreLoader />
          </View>
        ) : (
          <TouchableOpacity
            style={[styles._button, {}]}
            onPress={this._handleSubmit}>
            <Text style={styles._buttonText}>{strings('addNew.submit')}</Text>
          </TouchableOpacity>
        )}
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    country: state.network.countryLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddNew);
