import React, { Component } from 'react';
import Header from './Header';
import {
	Text,
	View,
	Image,
	ScrollView,
	TextInput,
	StyleSheet,
	Dimensions,
	Alert,
	TouchableOpacity,
	Picker,
	Keyboard,
	Platform,
} from 'react-native';
import ResetPassword from './ResetPassword';
import Otp from './Otp';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/ForgotPassword';
import countriesCode from './CountriesCode';
import RNPickerSelect from 'react-native-picker-select';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class ForgotPassword extends Component {
	static navigationOptions = ({ navigation }) => {
		let { state } = navigation;
		if (state.params !== undefined) {
			return {
				title:
					state.params.isRTL === true
						? 'هل نسيت كلمة المرور'
						: 'Forgot Password'
			};
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			mobile: '',
			code: '+966',
			error: false
		};
	}

	componentDidMount() {
		const { setParams } = this.props.navigation;
		setParams({ isRTL: this.props.isRTL });
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
			this.props.navigation.setParams({ isRTL: nextProps.isRTL });
			this.props.navigation.state.params.isRTL = nextProps.isRTL;
		}
	}

	handleSubmit() {
		const { navigate } = this.props.navigation;
		//console.log(this.state.mobile);
		Keyboard.dismiss();
		const { isRTL } = this.props;
		if(this.state.mobile.charAt(0) == 0){
			this.setState({error:true})
			return;
		}
		fetch(API_URL+'/forgotpassword_2', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				countryCode: this.state.code,
				value: this.state.mobile
			})
		})
			.then(response => response.json())
			.then(responseData => {
				if (
					responseData.status == 502368 &&
					responseData.eData.eCode == 684605
				) {
					const message = isRTL ? 'يرجى ادخال رقم الجوال' : 'Please enter the mobile number'
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);

				} else if (
					responseData.status == 502368 &&
					responseData.eData.eCode == 686858
				) {
					
					const message = isRTL ? 'رقم الجوال غير مسجل' : 'Mobile is not registered';
					Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
				} else if (
					responseData.status == 805465 &&
					responseData.eData === null
				) {
					const mobileNo = this.state.mobile;
					const cCode = this.state.code;
					navigate('Otp', { mobileNo, cCode });
				}
			})
			.catch(err => {
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
	}
	render() {
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		for (let i = 0; i < countriesCode.length; i++) {
			countriesCode[i].value = countriesCode[i].dial_code;
			countriesCode[i].label = countriesCode[i].dial_code+'-'+countriesCode[i].name;
			countriesCode[i].color = '#000000';
			}
		return (
			<ScrollView
				keyboardShouldPersistTaps="always"
				style={{
					padding: 20,
					backgroundColor: '#F1F6F7',
					marginVertical: 10
				}}
			>
				<View style={{ alignItems: 'center' }}>
					<Image
						style={styles.logo}
						source={require('../icons/logo1x.png')}
					/>
				</View>
				<View style={{ flexDirection: 'row' }}>
					{/* <View style={[styles._textbox, { width: '50%' }]}>
						<Picker
							selectedValue={this.state.code}
							style={{ height: 50, width: 155 }}
							onValueChange={(itemValue, itemIndex) =>
								this.setState({ code: itemValue })
							}
						>
							<Picker.Item label="Country Code" value="" />
							{countriesCode.map((item, key) => (
								<Picker.Item
									key={key}
									label={item.dial_code+'-'+item.name}
									value={item.dial_code}
								/>
							))}
						</Picker>
					</View> */}

					<View style={styles.ccode}>
							{ Platform.OS === 'android' ?
								<Picker

									selectedValue={this.state.code}
									onValueChange={(itemValue, itemIndex) =>
										this.setState({ code: itemValue })
									}
								>
									<Picker.Item label="Country Code" />
									{countriesCode.map((item, key) => (
										<Picker.Item
											key={key}
											label={item.dial_code+'-'+item.name}
											value={item.dial_code}
										/>
									))}
								</Picker>
								: <View style={{left: 10, top: 12,marginVertical:6}}>
									<RNPickerSelect
										placeholder={{
											label: 'Select CountryCode',
											value: null
										}}
										value= {
											this.state.code
										}		
										onValueChange={(itemValue, index) =>
											this.setState({ code: itemValue })
										}
										style={{
											placeholderColor: 'grey'
										}}
										hideIcon
										items={countriesCode}
									/>
								</View>
							}
							</View>
					<View style={[styles._textbox, { flex: 1 }]}>
						<TextInput
							style={styles._textboxText}
							underlineColorAndroid="white"
							maxLength={10}
							onChangeText={text =>
								this.setState({ mobile: text })
							}
							keyboardType={'numeric'}
							placeholderTextColor="#545454"
							placeholder={strings('login.mobile')}
							returnKeyType="go"
						/>
					</View>
				</View>
					{this.state.error && <Text style={{color:"red"}}>Write your mobile number without zero at begining, example: 50xxxxx77</Text>}
				<TouchableOpacity
					style={styles._button}
					onPress={this.handleSubmit.bind(this)}
				>
					<Text style={styles._buttonText}>
						{strings('adNewAd.submit')}
					</Text>
				</TouchableOpacity>
			</ScrollView>
		);
	}
}

function mapStateToProps(state) {
	return {
		isRTL: state.network.isRTL
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(layoutActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
