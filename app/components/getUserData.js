import { AsyncStorage } from 'react-native';

export const getUserData = async function() {
		const json = await AsyncStorage.getItem('user_data');
		const userData = JSON.parse(json);

		return userData;
}
