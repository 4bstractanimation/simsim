import React, {Component} from 'react';
import {
  View,
  StatusBar,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
// import Footer from './Footer';
import Comments from './Comments';
import commentlist from './commentlist';
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/FontAwesome';
import Slideshow from './react-native-slideshow';
import Modal from 'react-native-simple-modal';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/AdDetails';
import FooterContainer from './FooterContainer';
import NotificationHeader from './NotificationHeader';
import getCurrencyValue from './getCurrencyValue.js';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';
import {AppEventsLogger} from 'react-native-fbsdk';
import database from '@react-native-firebase/database';

const {width, height} = Dimensions.get('window');

class AdDetails extends Component {
  constructor(props) {
    database()
      .ref(
        'ADS/AD/' + props.navigation.state.params.item.product_name + '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
    database()
      .ref(
        'ADS/AD/' +
          props.navigation.state.params.item.product_name +
          '/android',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });

    database()
      .ref(
        'ADS/AD/' +
          props.navigation.state.params.item.product_name +
          '/date/' +
          new Date().toDateString() +
          '/total',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
    database()
      .ref(
        'ADS/AD/' +
          props.navigation.state.params.item.product_name +
          '/date/' +
          new Date().toDateString() +
          '/android',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });

    database()
      .ref('D_ADS/D_AD/' + new Date().toDateString() + '/android')
      .transaction(total => {
        return (total || 0) + 1;
      });

    database()
      .ref('D_ADS/D_AD/' + new Date().toDateString() + '/total')
      .transaction(total => {
        return (total || 0) + 1;
      });

    super();
    this.state = {
      review: false,
      abuse: false,
      ad_details: '',
      reviewDescription: '',
      starCount: 0,
      allreviews: [],
      token: '',
      userId: '',
      isLoading: true,
      JSON_from_server: [],
      fetching_Status: false,
      lastPage: false,
      star5: '',
      star4: '',
      star3: '',
      star2: '',
      star1: '',
      country: '',
      currentCurrencyValue: 1,
      item: '',
      adsImage: [],
      modalOpen: false,
      footerLat: '',
      footerLng: '',
    };
    this.page = 0;
    this.addFavourite = this.addFavourite.bind(this);
    this.removefavourite = this.removefavourite.bind(this);
  }

  // static navigationOptions = ({navigation}) => ({

  //     title: navigation.state.params.item.pinName || navigation.state.params.item.pin_name || navigation.state.params.item.pins.pin_name,
  //     headerRight: <NotificationHeader navigation={navigation}/>
  // });

  static navigationOptions = ({navigation}) => ({
    headerTitle: (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 20,
            marginTop: 10,
            marginRight: 10,
          }}>
          {navigation.state.params.item.pinName ||
            navigation.state.params.item.pin_name ||
            navigation.state.params.item.pins.pin_name}
        </Text>

        <TouchableOpacity
          onPress={function() {
            function userNotLoggedIn() {
              const {navigate} = navigation;
              Alert.alert(
                '',
                'You need to login',
                [
                  {text: 'Login', onPress: () => navigate('Login')},
                  {text: 'SignUp', onPress: () => navigate('SignUp')},
                  {text: 'Cancel'},
                ],
                {cancelable: false},
              );
            }
            if (navigation.state.params.token) {
              navigation.state.params.handleSave();
            } else {
              userNotLoggedIn();
            }
          }}>
          {navigation.state.params.isfavourite ? (
            <Image
              resizeMode="cover"
              style={{
                width: 30,
                height: 50,
                resizeMode: 'contain',
              }}
              source={require('./../icons/favourite2x.png')}
            />
          ) : (
            <Image
              resizeMode="cover"
              style={{
                width: 30,
                height: 50,
                resizeMode: 'contain',
              }}
              source={require('./../icons/beforefavourite.png')}
            />
          )}
        </TouchableOpacity>
      </View>
    ),
    headerRight: <NotificationHeader navigation={navigation} />,
  });

  addFavourite() {
    const {isRTL} = this.props;
    if (this.state.token) {
      fetch(API_URL + '/add-favorite', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: this.state.token,
        },
        body: JSON.stringify({
          pinId: this.props.navigation.state.params.pinDetails.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          // console.warn("TCL: addFavourite -> responseData", responseData)
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الإضافة لقائمة المفضلة  '
              : 'Failed to add in favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            this.setState({isfavourite: true});
            const message = isRTL
              ? 'تم الإضافة بنجاح لقائمة المفضلة '
              : 'Added in favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
            this.props.navigation.setParams({
              handleSave: this.removefavourite,
              isfavourite: true,
            });
          }
        })
        .catch(err => {
          const errorText = isRTL ? 'خطأ' : 'Error';
          Alert.alert(errorText, err.message);
        });
    }
  }

  async removefavourite() {
    const token = this.state.token;
    const {isRTL} = this.props;
    if (token) {
      fetch(API_URL + '/remove-favorite', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          pinId: this.props.navigation.state.params.pinDetails.id,
          userId: this.state.userId,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          //console.warn('res-->',responseData);
          if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            const message = isRTL
              ? 'فشل في الازالة من قائمة المفضلة '
              : 'Failed to remove from favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            const message = isRTL
              ? ' تمت الازالة من قائمة المفضلة '
              : 'Removed from favorite list';
            Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
            this.setState({isfavourite: false});
            this.props.navigation.setParams({
              handleSave: this.addFavourite,
              isfavourite: false,
            });
          }
        })
        .catch(err => {
          const errorText = isRTL ? 'خطأ' : 'Error';
          Alert.alert(errorText, err.message);
        });
    }
  }

  async componentDidMount() {
    const {currencyCode} = this.props.country;
    this.getAdDetails();
    if (currencyCode !== 'SAR') {
      this.setState({
        currentCurrencyValue: await getCurrencyValue(currencyCode),
      });
    }
    let isfavourite;
    if (this.props.navigation.state.params.pinDetails) {
      isfavourite = Boolean(
        this.props.navigation.state.params.pinDetails.isfavourite,
      );
    }

    if (isfavourite) {
      //console.warn('isfavourite-->>',isfavourite)
      this.props.navigation.setParams({
        handleSave: this.removefavourite,
        isfavourite: true,
      });
    } else {
      this.props.navigation.setParams({
        handleSave: this.addFavourite,
        isfavourite: false,
      });
    }

    const json = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(json);
    // console.warn(userData)
    if (userData) {
      const id = userData.userId;
      const token = userData.sData;
      this.setState({token: token, userId: id});
      this.props.navigation.setParams({token: userData.sData});
      // this.setState({token:userData.sData,userId:userData.userId})
    }

    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick = () => {
    this.props.navigation.goBack();
    return true;
  };

  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  fetch_more_data_from_server = () => {
    this.page = this.page + 1;
    const data = this.props.navigation.state;
    this.setState({fetching_Status: true}, () => {
      fetch(API_URL + '/adDetail?page=' + this.page, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          pinId: data.params.item.pin_id,
          adId: data.params.item.id,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          //console.warn('ad details-->>', responseData)
          this.setState({
            JSON_from_server: [
              ...this.state.JSON_from_server,
              ...responseData.comments.data,
            ],
            fetching_Status: false,
          });
          if (
            responseData.comments.current_page ==
            responseData.comments.last_page
          ) {
            this.setState({lastPage: true});
          }
        });
    });
  };

  Capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  Render_Footer = () => {
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.viewMore}
        onPress={this.fetch_more_data_from_server}>
        <Text style={styles.viewMoreText}>View More</Text>

        {this.state.fetching_Status ? (
          <ActivityIndicator color="#fff" style={{marginLeft: 6}} />
        ) : null}
      </TouchableOpacity>
    );
  };

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#eff0f2',
          marginVertical: 15,
        }}
      />
    );
  };

  _handleSubmit(data) {
    const token = this.state.token;
    const id = this.state.userId;
    const {isRTL} = this.props;
    this.setState({isLoading: true});
    const reviewdata = {
      pinId: data.params.item.pin_id,
      userId: id,
      ad_id: data.params.item.id,
      rating: this.state.starCount,
      review: this.state.reviewDescription,
    };
    // console.warn('rating data-->>>',reviewdata);
    if (this.state.starCount) {
      fetch(API_URL + '/ads-review', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify(reviewdata),
      })
        .then(response => response.json())
        .then(async responseData => {
          this.setState({
            review: false,
            abuse: false,
            reviewDescription: '',
            isLoading: false,
          });
          this.getAdDetails(false);
          if (responseData.status == 654864 && responseData.eData === null) {
            Alert.alert('', responseData.eMsg);
          }
        });
    } else {
      this.setState({isLoading: false});

      const message = isRTL ? 'التقييم مطلوب' : 'Rating is required!';
      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
    }
  }

  getAdDetails(review = true) {
    const data = this.props.navigation.state;
    // console.warn("TCL: getAdDetails -> data", data);
    const {isRTL} = this.props;
    if (review) {
      this.page = this.page + 1;
    }
    fetch(API_URL + '/adDetail?page=' + this.page, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pinId: data.params.item.pin_id,
        adId: data.params.item.id,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn('ad details -->>',responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          this.setState({isLoading: false, responseMsg: responseData.eMsg});
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          if (this.state.JSON_from_server.length < 2) {
            this.setState({
              JSON_from_server: responseData.comments.data,
              isLoading: false,
              star5: responseData.star5[0].totalratingCount,
              star4: responseData.star4[0].totalratingCount,
              star3: responseData.star3[0].totalratingCount,
              star2: responseData.star2[0].totalratingCount,
              star1: responseData.star1[0].totalratingCount,
              item: responseData.AdByid,
              footerLat: responseData.lat,
              footerLng: responseData.lng,
              adsImage: responseData.adsImage,
            });
          } else {
            this.setState({
              JSON_from_server: [
                ...this.state.JSON_from_server,
                ...responseData.comments.data,
              ],
              isLoading: false,
              star5: responseData.star5[0].totalratingCount,
              star4: responseData.star4[0].totalratingCount,
              star3: responseData.star3[0].totalratingCount,
              star2: responseData.star2[0].totalratingCount,
              star1: responseData.star1[0].totalratingCount,
              footerLat: responseData.lat,
              footerLng: responseData.lng,
              item: responseData.AdByid,
              // adsImage: responseData.adsImage
            });
          }

          if (
            responseData.comments.current_page ==
            responseData.comments.last_page
          ) {
            this.setState({lastPage: true});
          }
        } else if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول إلى تفاصيل الإعلان '
            : 'Failed to get Ad details';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  reportAbuse(data) {
    // console.warn('report....', data.params.item.id)
    const token = this.state.token;
    const id = this.state.userId;
    const {isRTL} = this.props;
    if (this.state.reviewDescription.trim()) {
      fetch(API_URL + '/report-abuse', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
        body: JSON.stringify({
          userId: id, // this is login user id
          to_user_id: data.params.item.user_id, //this user_id is a seller id
          pinId: data.params.item.pin_id,
          addId: data.params.item.id,
          description: this.state.reviewDescription,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          const message = isRTL
            ? 'تم ارسال رسالتك بنجاح '
            : 'Your messages has been sent successfully';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          this.setState({review: false, abuse: false, reviewDescription: ''});
        });
    } else {
      const message = isRTL ? 'الوصف مطلوب' : 'Description is required!';
      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
    }
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  showSpecialOfferListMoldal = () => {
    this.setState({modalOpen: true});
  };

  modalDidClose = () => {
    this.setState({modalOpen: false});
  };

  isAdHasOfferHandler(adData) {
    const {start_date, end_date} = adData;

    if (!start_date || !end_date) {
      return;
    }

    const startTime = new Date(start_date).setMilliseconds(0);
    const endDate = new Date(end_date);
    const endTime = endDate.setDate(endDate.getDate() + 1);
    const today = Date.now();

    return today >= startTime && today < endTime;
  }

  render() {
    // console.warn('currentCurrencyValue', this.state.currentCurrencyValue)

    const {star1, star2, star3, star4, star5} = this.state;
    const totalStar = star1 + star2 + star3 + star4 + star5;

    const {item} = this.props.navigation.state.params;
    const isOfferAvl = this.isAdHasOfferHandler(item);
    const {currencyCode} = this.props.country;
    const {currentCurrencyValue} = this.state;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {state} = this.props.navigation;
    const details = this.state.item;
    let rating = details.rating ? details.rating.toFixed(1) : '';
    const {
      id,
      product_name,
      pin_id,
      pinName,
      description,
    } = this.props.navigation.state.params.item; // this is pin info

    var imgUrls = this.state.adsImage.length
      ? this.state.adsImage.map(image => {
          return {url: image.image_name};
        })
      : [];
    try {
      AppEventsLogger.logEvent('Ad Clicked');
    } catch (e) {}

    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={[styles.content]}>
            <View>
              <TouchableOpacity onPress={this.showSpecialOfferListMoldal}>
                {this.state.adsImage.length ? (
                  <Slideshow
                    height={220}
                    arrowSize={25}
                    indicatorSelectedColor={'grey'}
                    containerStyle={{marginBottom: 15}}
                    dataSource={imgUrls}
                  />
                ) : (
                  <ActivityIndicator
                    size="large"
                    color="#0000ff"
                    style={{marginVertical: 25}}
                  />
                )}
              </TouchableOpacity>
            </View>
            <View
              style={[styles.contentItem, {justifyContent: 'space-between'}]}>
              <View
                style={{
                  flex: 1.5,
                  alignItems: this.props.isRTL ? 'flex-end' : 'flex-start',
                }}>
                <Text style={styles.textHeading}>
                  {strings('adDetail.product')}
                </Text>
                <Text style={styles.textColor}>{details.product_name}</Text>
                <Text
                  style={[
                    styles.textHeading,
                    {fontWeight: 'normal', fontSize: 12, color: '#00AADB'},
                  ]}>
                  {details.ad_number}
                </Text>
              </View>
              <View style={{flex: 1.1, marginLeft: 10}}>
                <Text style={styles.textHeading}>
                  {strings('adDetail.price')}
                </Text>
                <Text
                  style={[
                    styles.textColor,
                    {textDecorationLine: isOfferAvl ? 'line-through' : 'none'},
                  ]}>
                  {(
                    parseFloat(details.add_price) * currentCurrencyValue
                  ).toFixed(2)}
                </Text>
                <Text
                  style={[styles.textColor, {color: '#e60000', fontSize: 16}]}>
                  {isOfferAvl
                    ? (item.discounted_price * currentCurrencyValue).toFixed(2)
                    : ''}{' '}
                  {currencyCode}
                </Text>
              </View>
              {details.add_status ? (
                <View style={{flex: 0.8, marginLeft: 10}}>
                  <Text style={styles.textHeading}>
                    {strings('adDetail.condition')}
                  </Text>
                  <Text style={[styles.textColor, {color: '#00AADB'}]}>
                    {details.add_status}
                  </Text>
                </View>
              ) : null}
            </View>
            {details.description ? (
              <View style={styles.contentItem}>
                <View
                  style={{
                    flex: 1,
                    alignItems: this.props.isRTL ? 'flex-end' : 'flex-start',
                  }}>
                  <Text style={styles.textHeading}>
                    {strings('adDetail.description')}
                  </Text>
                  <Text style={styles.textColor}>{details.description}</Text>
                </View>
              </View>
            ) : null}
            {details.notes ? (
              <View style={styles.contentItem}>
                <View
                  style={{
                    flex: 1,
                    alignItems: this.props.isRTL ? 'flex-end' : 'flex-start',
                  }}>
                  <Text style={styles.textHeading}>
                    {strings('adDetail.notes')}
                  </Text>

                  <View style={{flexDirection: 'row'}}>
                    <Text style={styles.textColor}> {details.notes}</Text>
                  </View>
                </View>
              </View>
            ) : null}
          </View>
          <View style={styles.content}>
            <View style={styles.contentItem}>
              <View
                style={{
                  flex: 1,
                  alignItems: this.props.isRTL ? 'flex-end' : 'flex-start',
                }}>
                <Text style={styles.textHeading}>
                  {strings('adDetail.rates')}
                </Text>
              </View>
            </View>
            <View style={styles.contentItem}>
              <View style={styles.avrRating}>
                <Text style={styles.avrRatingText}>{rating}</Text>
                <StarRating
                  disabled={false}
                  maxStars={5}
                  rating={details.rating}
                  starSize={13}
                  emptyStarColor="#cacdd1"
                  fullStarColor="green"
                  emptyStar="star"
                  containerStyle={{paddingTop: 3}}
                  starStyle={{marginLeft: 5}}
                />
                <View style={styles.totalRating}>
                  <Icon
                    name="user"
                    size={10}
                    color="#4c4d4f"
                    style={[styles.icons, {marginLeft: 10}]}
                  />
                  <Text style={styles.iconsText}>
                    {details.totalUserRating}
                  </Text>
                </View>
              </View>
              <View style={{flex: 2}}>
                <RatingBar
                  starCounter=" 5 "
                  totalStar={totalStar}
                  ratingValue={this.state.star5}
                  isRTL={this.props.isRTL}
                />
                <RatingBar
                  starCounter=" 4 "
                  totalStar={totalStar}
                  ratingValue={this.state.star4}
                  isRTL={this.props.isRTL}
                />
                <RatingBar
                  starCounter=" 3 "
                  totalStar={totalStar}
                  ratingValue={this.state.star3}
                  isRTL={this.props.isRTL}
                />
                <RatingBar
                  starCounter=" 2 "
                  totalStar={totalStar}
                  ratingValue={this.state.star2}
                  bgColor={'#d8bc08'}
                  isRTL={this.props.isRTL}
                />
                <RatingBar
                  starCounter=" 1 "
                  totalStar={totalStar}
                  ratingValue={this.state.star1}
                  bgColor="red"
                  isRTL={this.props.isRTL}
                />
              </View>
            </View>
            <View style={{flex: 1, width: '100%'}}>
              {this.state.isLoading ? (
                <ActivityIndicator size="large" />
              ) : (
                <FlatList
                  keyExtractor={(item, index) => index}
                  data={this.state.JSON_from_server}
                  ItemSeparatorComponent={this.FlatListItemSeparator}
                  renderItem={({item, index}) => {
                    return (
                      <View style={styles.comment}>
                        <View
                          style={[
                            styles.avrRating,
                            {flexDirection: 'row', alignItems: 'center'},
                          ]}>
                          <Text style={styles.userName}>
                            {' '}
                            {item.review_by_user}{' '}
                          </Text>
                          <StarRating
                            disabled={true}
                            maxStars={5}
                            rating={parseFloat(item.rating)}
                            // halfStarEnabled={true}
                            starSize={10}
                            emptyStarColor="#cacdd1"
                            fullStarColor="green"
                            emptyStar="star"
                            containerStyle={{marginLeft: 10}}
                          />
                        </View>
                        <Text style={styles.flatList_items}>
                          {' '}
                          {item.review}{' '}
                        </Text>
                      </View>
                    );
                  }}
                  ListFooterComponent={
                    this.state.JSON_from_server.length && !this.state.lastPage
                      ? this.Render_Footer
                      : null
                  }
                />
              )}
            </View>
          </View>
          {this.state.review || this.state.abuse ? (
            <View style={styles.review}>
              {this.state.review && (
                <View style={styles.reviewRating}>
                  <Text
                    style={{
                      fontSize: 15,
                      fontFamily: 'System',
                      fontWeight: '400',
                    }}>
                    {strings('adDetail.giveRatingText')}
                  </Text>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={this.state.starCount}
                    // halfStarEnabled={true}
                    starSize={30}
                    selectedStar={rating => this.onStarRatingPress(rating)}
                    emptyStarColor="#cacdd1"
                    fullStarColor="green"
                    emptyStar="star"
                    containerStyle={{
                      alignItems: 'flex-start',
                      paddingTop: 3,
                      height: 33,
                    }}
                    starStyle={{marginLeft: 5}}
                    starPadding={10}
                  />
                </View>
              )}
              <View style={styles._descriptionbox}>
                <TextInput
                  style={[styles._textboxText]}
                  multiline={true}
                  underlineColorAndroid="white"
                  onChangeText={text =>
                    this.setState({reviewDescription: text})
                  }
                  numberOfLines={4}
                  keyboardType={'default'}
                  textAlignVertical="top"
                  placeholderTextColor="#7A7A7A"
                  placeholder={strings('adDetail.reviewDescription')}
                />
              </View>
              <TouchableOpacity
                style={[
                  styles._textbox,
                  {backgroundColor: '#00ADDE', marginBottom: 90},
                ]}
                onPress={() =>
                  this.state.review
                    ? this._handleSubmit(state)
                    : this.reportAbuse(state)
                }>
                <Text
                  style={{
                    textAlign: 'center',
                    marginVertical: 15,
                    fontWeight: 'bold',
                    color: '#ffff',
                    fontSize: 15,
                  }}>
                  {strings('adDetail.submit')}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}

          <View style={styles.buttonBox}>
            <TouchableOpacity
              style={[styles.button, {backgroundColor: '#29a3db'}]}
              onPress={() => {
                if (this.state.token) {
                  this.setState({review: true});
                } else {
                  this.props.navigation.navigate('Login');
                }
              }}>
              <Text style={styles.buttonText}>
                {strings('adDetail.review')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, {marginLeft: 10}]}
              onPress={() => {
                if (this.state.token) {
                  this.setState({abuse: true});
                } else {
                  this.props.navigation.navigate('Login');
                }
              }}>
              <Text style={styles.buttonText}>
                {strings('adDetail.report')}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View style={{flex: 0, marginBottom: 0}}>
          <FooterContainer
            {...this.props}
            footerLat={this.state.footerLat}
            footerLng={this.state.footerLng}
            sharingInfo={{
              sharingObject: 'ad',
              id,
              product_name,
              pin_id,
              pinName,
              description,
            }}
          />
        </View>
        <Modal
          disableOnBackPress={true}
          closeOnTouchOutside={true}
          open={this.state.modalOpen}
          modalDidClose={this.modalDidClose}
          style={{alignItems: 'center', width: '100%', height: '100%'}}
          modalStyle={{
            backgroundColor: 'transparent',
            height: '97%',
            justifyContent: 'center',
            borderRadius: 5,
          }}
          overlayStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0.75)',
            flex: 1,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
            <Text
              onPress={this.modalDidClose}
              style={{
                textAlign: 'right',
                fontWeight: 'bold',
                fontSize: 20,
                color: 'white',
                marginBottom: 0,
                marginTop: -25,
                marginRight: -10,
                paddingBottom: 0,
              }}>
              {' '}
              X{' '}
            </Text>
          </View>

          <Slideshow
            height={400}
            arrowSize={25}
            indicatorSelectedColor={'grey'}
            containerStyle={{
              backgroundColor: 'transparent',
              marginBottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
            }}
            dataSource={imgUrls}
          />

          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text
              style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20}}>
              {' '}
            </Text>
          </View>
        </Modal>
      </View>
    );
  }
}

const RatingBar = props => {
  const totalStar = props.totalStar;
  const styles = StyleSheetFactory.getSheet(props.isRTL);
  const ratingValue = props.ratingValue ? props.ratingValue : 0;
  const finalRating = (ratingValue * 100) / totalStar;
  const bgColor = props.bgColor ? props.bgColor : '#33a037';
  return (
    <View
      style={{
        flexDirection: props.isRTL ? 'row-reverse' : 'row',
        marginBottom: 1,
      }}>
      <Icon name="star" size={10} color="#a5a8aa" style={{marginTop: 5}} />
      <Text>{props.starCounter} </Text>
      <View style={styles.ratingBar}>
        <View
          style={[
            styles.ratingColorBar,
            {width: `${finalRating}%`},
            {backgroundColor: bgColor},
          ]}
        />
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    country: state.network.countryLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AdDetails);
