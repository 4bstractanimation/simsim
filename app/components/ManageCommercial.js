import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableNativeFeedback,
  Modal,
  Platform,
  Button,
  AsyncStorage,
  ActivityIndicator,
} from 'react-native';

import Header from './Header';
import ModalDropdown from 'react-native-modal-dropdown';
import AddNew from './AddNew';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/ManageCommercial';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import API_URL from '../configuration/configuration';

class ManageCommercial extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      return {
        title:
          state.params.isRTL === true
            ? ' إدارة العروض الترويجية'
            : 'Manage Commercials',
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      manageCom: [],
      play: false,
      title: '',
      commercialId: null,
      selectedIndex: [],
      userID: null,
      token: '',
      loader: false,
      itemId: '',
      specialOfferList: [],
    };
    this.balance = '';
  }

  async getCommercialList(token) {
    // const getUserData = await AsyncStorage.getItem('user_data');
    // const userData = JSON.parse(getUserData);
    // const token = userData.sData;
    const {isRTL} = this.props;
    fetch(API_URL + '/manage-commercial', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn('manage-commercial-->>', responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في تكرار الإعلان التجاري'
            : 'Failed to repeat commercial list';
          Toast.show(message);
        } else if (
          responseData.status == 805465 &&
          responseData.eData === null
        ) {
          this.setState({
            manageCom: responseData.list,
            isLoading: false,
            loader: false,
            itemId: '',
          });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });

    {
      /*	Get special ofer list that seller applied */
    }
    fetch(API_URL + '/getAdsDetailWithOffers', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn('special offer list-->>', responseData.data)
        if (responseData.status === 200) {
          this.setState({
            specialOfferList: responseData.data,
            isLoading: false,
            loader: false,
            itemId: '',
          });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  async componentDidMount() {
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    const userID = userData.userId;

    this.setState({token: token, userID: userID});

    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
    this.getCommercialList(token);
    this.props.navigation.addListener('didFocus', async event => {
      this.getCommercialList(token);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  async pause(item) {
    const {isRTL} = this.props;
    // console.warn('commercial-->>',item)
    this.setState({loader: true, itemId: item.id});

    let data;
    //const getUserData = await AsyncStorage.getItem('user_data');
    //const userData = JSON.parse(getUserData);
    //const token = userData.sData;
    //const userID = userData.userId;

    if (item.running_status === 'Play') {
      data = {
        running_status: 'Pause',
        commercialId: item.commercial_id,
        sellerId: this.state.userID,
        title: item.title,
        start_date: item.start_date,
      };
      //this.setState({ play: false, });
    } else {
      data = {
        running_status: 'Play',
        commercialId: item.commercial_id,
        sellerId: this.state.userID,
        title: item.title,
        start_date: item.start_date,
      };
      //this.setState({ play: true, });
    }
    fetch(API_URL + '/update-running-status', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn("Hello res --> "+JSON.stringify(responseData));
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'arb missing'
            : 'Failed to update running status';
          Alert.alert('', message);
        } else if (
          responseData.status == 805465 &&
          responseData.eData === null
        ) {
          this.getCommercialList(this.state.token);
          //this.setState({loader:false})
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });

    //this.setState({loader:false})
  }

  async repeatPromo() {
    const {isRTL} = this.props;
    if (this.state.manageCom.length === 0) {
      const message = isRTL
        ? 'يرجى إضافة العرض للتكرار'
        : 'Please add Promotion to repeat';
      Toast.show(message);
      return;
    }

    if (this.state.commercialId === null) {
      const message = isRTL
        ? 'يرجى متابعة الضغط لاختيار العرض'
        : 'Please long press to select the promotion';
      Toast.show(message);
    }

    // const getUserData = await AsyncStorage.getItem('user_data');
    // const userData = JSON.parse(getUserData);
    // const token = userData.sData;
    // const userID = userData.userId;
    fetch(API_URL + '/repeat-commercial', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        commercialId: this.state.commercialId,
        userId: this.state.userID,
        title: this.state.title,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في تكرار الإعلان التجاري '
            : 'Failed to repeat commercial';
          Toast.show(message);
        } else if (
          responseData.status == 805465 &&
          responseData.eData === null
        ) {
          this.getCommercialList(this.state.token);
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  SampleFunction = item => {
    const {isRTL} = this.props;
    this.setState({commercialId: item.commercial_id, title: item.title});
    const title = isRTL ? 'كرر العرض' : 'repeat promotion';
    const message = isRTL
      ? 'اذا اردت تكرار هذا العرض ، اضغط على زر الإعادة '
      : 'If You want to repeat this promotion, Click on Repeat Button';
    Alert.alert(title, message);
  };

  getStatus(status) {
    switch (status) {
      case 'Yes':
        return this.props.isRTL ? 'وافق' : 'Approved';
      case 'No':
        return this.props.isRTL ? 'قيد الانتظار' : 'Pending';
      case 'Done':
        return this.props.isRTL ? 'انتهى' : 'Finished';
      default:
        return status;
    }
  }

  updateSpecialOfferRunningStatus(offerItem) {
    // console.warn('offerData-->>',offerItem);
    const {offerId, transaction_id} = offerItem.offerData;
    this.setState({loader: true, itemId: offerId});

    let running_status = offerItem.offerData.reunning_status;
    if (running_status == 'Play') {
      running_status = 'Pause';
    } else {
      running_status = 'Play';
    }

    fetch(API_URL + '/specialOfferPlayPause', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({offerId, transaction_id, status: running_status}),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn("specialOfferPlayPause --> ",responseData);
        if (responseData.status === 200) {
          this.getCommercialList(this.state.token);
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  deleteCommercial(commercialData) {
    const {id} = commercialData;
    const {isRTL} = this.props;
    fetch(API_URL + '/delete-commercial-request', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({id}),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn("specialOfferPlayPause --> ",responseData);
        if (responseData.status === 200) {
          this.getCommercialList(this.state.token);
          const message = isRTL
            ? 'arb missing'
            : 'Commercial Deleted successfully';
          Toast.show(message);
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  deleteOfferHandler(offerData) {
    const {transaction_id} = offerData.offerData;
    const {isRTL} = this.props;
    fetch(API_URL + '/delete-specialoffer-request', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({transaction_id}),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn("specialOfferPlayPause --> ",responseData);
        if (responseData.status === 200) {
          this.getCommercialList(this.state.token);
          const message = isRTL
            ? 'arb missing'
            : 'Commercial Deleted successfully';
          Toast.show(message);
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  /**
	 showOfferStatus method used to display the following offer status:
	 1. Active: if offer running
	 2. Finished: if passed end date ot budget is finished
	 3. Pause: If seller clicked on pause button
	*/
  showOfferStatus(status, expire, budget) {
    const isRTL = this.props.isRTL;
    if (expire || budget == 0) {
      return isRTL ? 'انتهى' : 'Finished';
    }

    if (status === 'Play') {
      return isRTL ? 'نشط' : 'Active';
    }

    return isRTL ? 'متوقف' : status;
  }

  getArabicCommercialText(value) {
    if (!value) {
      return;
    }

    if (this.props.isRTL) {
      if (value.toLowerCase().trim() === 'push notifications') {
        return 'تنبيهات الجوال';
      } else {
        return 'رسائل ترويجية';
      }
    }

    return value;
  }

  render() {
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
            marginVertical: 10,
            marginHorizontal: 10,
          }}>
          {/* <TouchableOpacity onPress={this.repeatPromo.bind(this)}>
						<View style={styles.button}>
							<Text style={styles.button_text}>
								{strings('manageCommercial.repeat')}
							</Text>
						</View>
					</TouchableOpacity> */}
          <TouchableOpacity onPress={() => navigate('AddNew')}>
            <View style={[styles.button, {backgroundColor: '#00ADDE'}]}>
              <Text style={[styles.button_text, {color: '#FFFFFF'}]}>
                {strings('manageCommercial.add')}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <ScrollView style={{marginHorizontal: 10}}>
          {this.state.isLoading ? (
            <PreLoader />
          ) : this.state.manageCom.length ? (
            this.state.manageCom.map((item, i) => {
              //console.warn("TCL: render -> item", item);
              const count = item.total;
              const val = count ? count.split('') : [];
              const startDate = moment(item.start_date).format('DD MMM YYYY');
              const endDate = moment(item.end_date).format('DD MMM YYYY');
              const status = this.getStatus(item.admin_approval);
              const pinsName =
                item.commercials_pin &&
                item.commercials_pin.map(pin => pin.pins.pin_name).join(', ');
              return (
                <TouchableOpacity
                  //onLongPress={this.SampleFunction.bind(
                  //	this,
                  //	item)}

                  activeOpacity={0.6}
                  key={i}>
                  <View style={[styles._messageBox]}>
                    <View style={styles._textBox}>
                      <View
                        style={{
                          flexDirection: this.props.isRTL
                            ? 'row-reverse'
                            : 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={styles._title}>
                          {this.getArabicCommercialText(item.commercial_name)}
                        </Text>
                        {item.admin_approval &&
                          item.admin_approval.toLowerCase() === 'yes' &&
                          item.commercial_name !== 'Special Offers' &&
                          item.remaining_balance != 0 && (
                            <TouchableOpacity
                              onPress={this.pause.bind(this, item)}>
                              {this.state.loader &&
                              item.id === this.state.itemId ? (
                                <ActivityIndicator size="small" color="grey" />
                              ) : (
                                <Text style={styles.pause}>
                                  {item.running_status === 'Play'
                                    ? this.props.isRTL
                                      ? 'ايقاف'
                                      : 'Pause'
                                    : this.props.isRTL
                                    ? 'تشغيل'
                                    : 'Play'}
                                </Text>
                              )}
                            </TouchableOpacity>
                          )}

                        {/* Delete button code
													((item.admin_approval && item.admin_approval.toLowerCase() === "done") || (item.remaining_balance == 0 )) &&
													<TouchableOpacity onPress={this.deleteCommercial.bind(this, item)}>
														<Image style={[styles.icons, { marginHorizontal: 2, width:30, height:30 }]} source={require('./../icons/delete2x.png')} />
													</TouchableOpacity>
													*/}
                      </View>
                      <View
                        style={[
                          styles._descriptionContainer,
                          {flexDirection: 'column'},
                        ]}>
                        <Text
                          style={[styles._description, {fontWeight: 'bold'}]}>
                          {item.title}
                        </Text>

                        <Text style={styles._description}>
                          {this.props.isRTL ? '￼الرصيد : ' : 'Balance:'}{' '}
                          {item.budget_package}
                        </Text>

                        <Text style={styles._description}>
                          {this.props.isRTL
                            ? 'تاريخ البداية : '
                            : 'Starting Date: '}{' '}
                          {startDate}
                        </Text>

                        <Text style={styles._description}>
                          {this.props.isRTL ? 'تاريخ الانتهاء : ' : 'End Date:'}{' '}
                          {endDate}
                        </Text>

                        <Text style={styles._description}>
                          {this.props.isRTL
                            ? ' عدد النقرات الناجحة : '
                            : 'Successful Clicks:'}{' '}
                          {item.total_send_commercial}
                        </Text>
                        <Text style={styles._description}>
                          {this.props.isRTL ? 'تكلفة النقرة : ' : 'Click Rate:'}{' '}
                          {item.commercial_cost}
                        </Text>

                        {/* <Text style={styles._description}>
                          {this.props.isRTL
                            ? 'الرصيد المتبقي : '
                            : 'Remaining Balance:'}{' '}
                          {item.balance_cost} */}
                          {/* {item.remaining_balance} */}
                        {/* </Text> */}

                        <Text style={styles._description}>
                          {this.props.isRTL ? ' نقطة البيع : ' : 'Pins:'}{' '}
                          {pinsName}
                        </Text>

                        <Text style={styles._description}>
                          {this.props.isRTL ? 'الحالة : ' : 'Status : '}{' '}
                          {status}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: this.props.isRTL
                            ? 'row-reverse'
                            : 'row',
                          justifyContent: 'flex-end',
                        }}>
                        {val.map((visitor, i) => {
                          return (
                            <View style={styles.counterContainer} key={i}>
                              <Text style={styles.counterText}>{visitor}</Text>
                            </View>
                          );
                        })}
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })
          ) : null}
          {/* Here Showing special offer list */}
          <View>
            {this.state.specialOfferList.length
              ? this.state.specialOfferList.map((offer, index) => {
                  const toDay = Date.now();
                  const endDate = new Date(offer.offerData.end_date);
                  const endDateValue = endDate.setDate(endDate.getDate() + 1);
                  const isOfferExpired = toDay >= endDateValue;
                  const budgetRemaining = offer.offerData.remaining_balance;
                  //console.warn('todaye-->>',toDay, endDateValue)
                  // console.warn('toDay com-->>',isOfferExpired)
                  return (
                    <View key={index} style={[styles._messageBox]}>
                      <View style={styles._textBox}>
                        <View style={styles._descriptionContainer}>
                          <Text
                            style={[styles._description, {fontWeight: 'bold'}]}>
                            {this.props.isRTL ? 'عروض خاصة' : 'Special Offer'}
                          </Text>
                          {isOfferExpired || budgetRemaining <= 0 ? null : (
                            <TouchableOpacity
                              onPress={this.updateSpecialOfferRunningStatus.bind(
                                this,
                                offer,
                              )}>
                              {this.state.loader &&
                              offer.offerData.offerId === this.state.itemId ? (
                                <ActivityIndicator size="small" color="grey" />
                              ) : (
                                <Text style={styles.pause}>
                                  {offer.offerData.reunning_status === 'Play'
                                    ? this.props.isRTL
                                      ? 'ايقاف'
                                      : 'Pause'
                                    : this.props.isRTL
                                    ? 'تشغيل'
                                    : 'Play'}
                                </Text>
                              )}
                            </TouchableOpacity>
                          )}
                        </View>

                        <View
                          style={[
                            styles._descriptionContainer,
                            {flexDirection: 'column'},
                          ]}>
                          <Text style={styles._description}>
                            {this.props.isRTL
                              ? 'تاريخ البداية'
                              : 'Starting Date'}
                            : {offer.offerData.start_date}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL ? 'تاريخ النهاية' : 'End Date'}:{' '}
                            {offer.offerData.end_date}
                          </Text>

                          <Text style={styles._description}>
                            {this.props.isRTL ? '￼الرصيد : ' : 'Balance:'}{' '}
                            {offer.offerData.package_value}
                          </Text>

                          <Text style={styles._description}>
                            {this.props.isRTL ? 'الاعلانات' : 'Ads'}:{' '}
                            {offer.adsData.join(', ')}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL ? 'نقطة البيع' : 'Sales Point'}:{' '}
                            {offer.pinData}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL ? 'تكلفة النقرة' : 'Click Rate'}:{' '}
                            {offer.offerData.click_rate}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL
                              ? 'عدد النقرات الناجحة'
                              : 'Successful Clicks'}
                            : {offer.offerData.total_clicks}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL
                              ? 'الميزانية المتبقية'
                              : 'Budget Remaining'}
                            : {offer.offerData.remaining_balance}
                          </Text>
                          <Text style={styles._description}>
                            {this.props.isRTL ? 'الحالة' : 'Status'}:{' '}
                            {this.showOfferStatus(
                              offer.offerData.reunning_status,
                              isOfferExpired,
                              budgetRemaining,
                            )}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                })
              : null}
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ManageCommercial);
