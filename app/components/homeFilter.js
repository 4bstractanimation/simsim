import React, { Component } from 'react';
import CheckBox from 'react-native-check-box';
import {Modal, TextInput, View,ScrollView, StyleSheet, Dimensions, TouchableOpacity, Text, AsyncStorage, Alert} from 'react-native';
import DeviceInfo from 'react-native-device-info'; 

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as searchActions from '../actions/Search';

class Filter extends Component {
	constructor(props){
		super(props);
		this.state = {
			modalVisible: false,
			individual: false,
			enterprise:false,
			free:false,
			premium: false,
			platinum: false,
			nearest: false,
			delivery:false,
			rating:false,
			new: false,
			used: false,
			all: false,
			region: {},
			token: '',
			userId: ''
		}
	}

	setModalVisible(visible) {
    	this.setState({modalVisible: visible});
  	}

	componentWillReceiveProps(nextProps) {
  		if(nextProps.modalVisible != this.state.modalVisible) {
  			this.setState({modalVisible : nextProps.modalVisible})
  		}
  	}

	onClickIndi(){
		this.setState({individual: !this.state.individual});
	}
	onClickEnter(){
		this.setState({enterprise: !this.state.enterprise});
	}
	onClickFree(){
		this.setState({free: !this.state.free})
	}
	 onClickPr(){
		this.setState({premium: !this.state.premium});
	}
	 onClickPl(){
	 	this.setState({platinum: !this.state.platinum});
    }
	onClickDl(){
		this.setState({delivery: !this.state.delivery, all: false});
	}
	onClickRat(){
		this.setState({rating: !this.state.rating, all: false});
	}
	onClickNear(){
		this.setState({nearest: !this.state.nearest, all: false})
	}

	onClickAll() {
		if(this.state.all) {
			this.setState({
				all: !this.state.all,
				rating: false,
				nearest: false,
				new: false,
				used: false,

			})
		} else {
			this.setState({
				all: true,
				rating: true,
				nearest: true,
				new: true,
				used: true,

			})
		}
	}

	componentDidMount() {
		AsyncStorage.getItem('user_data').then(json => {
			if (json) {
			  const userData = JSON.parse(json);
			  this.setState({ token: userData.sData, userId: userData.userId });
			}
		  });
		AsyncStorage.getItem('region').then( json => {
        const region = JSON.parse(json);
       // console.warn(region)
        const lat  = region.coords.latitude;
        const lng = region.coords.longitude;
        this.setState({region})
    })
	}

	async searchFilter() {
		let obj={};
		var free = this.state.free ? 'Free' : null;
		var premium = this.state.premium ? 'Premium' : null;
		var platinum = this.state.platinum ? 'Platinum' : null;
		var delivery = this.state.delivery ? 'yes':'';
		var individual = this.state.individual ? 'individual': null;
		var enterprise = this.state.enterprise ? 'enterprise':null;
		var rating = this.state.rating ? 'yes':'';
		var seller_type = [individual, enterprise];
		const newest = this.state.new? 'New': null;
		const used = this.state.used ? 'Used': null;
        console.warn("TCL: Filter -> searchFilter ->", used , newest);

		var type;
		type = seller_type.filter(item => item != null)
		if(type.length == 0 ) {
			type = '';
		}

		var subscription = [free, premium, platinum];

		var subscription_plan;		
		subscription_plan = subscription.filter(item => item != null)
		if(subscription_plan.length == 0 ) {
			subscription_plan = '';
		}

		var product_status = [used, newest];
		var status = product_status.filter(item => item != null)
		if(status.length === 0 ) {
			status ='';
		}
		
		const lat = (this.props.location !== undefined && this.props.location.latitude !== undefined) ? this.props.location.latitude : '';
		const long = (this.props.location !== undefined && this.props.location.longitude !== undefined) ? this.props.location.longitude : '';
		const deviceId = await DeviceInfo.getDeviceId();
		
		const { latitude, longitude } = this.state.region.coords;

        this.props.searchWithLocationAPI(this.state.userId, latitude , longitude, deviceId,'',null,null,null,type,subscription_plan,
        	delivery,rating, status),
        this.props.setModalStatus(false);
	}

	render() {
		const isRTL = this.props.isRTL;
		return(
			<View>
			<Modal
		        animationType={'slide'}
		        transparent={true}
		        visible={this.state.modalVisible}
		        onRequestClose={() => {
		          this.props.setModalStatus(false);
		      }}>
		      <View style={{flex:1, backgroundColor:'#ecf2f9', padding: 10}}>

		     <ScrollView>
				<View style={{backgroundColor:'white', height:50, marginTop:5}}> 
			      	<Text style={{color:'grey', fontSize:17,margin:12, fontWeight:'bold'}}>{isRTL? "حالة المنتج" : "Product Status"}</Text>
			    </View>
				<CheckBox
				     style={{flex: 1, padding: 10}}
				     onClick={() => this.setState({new: !this.state.new, all: false})}
				     isChecked={this.state.new}
				     leftText={isRTL? "الجديد" : 'New'}
				     leftTextStyle={{fontSize:17}}
				 />
				 <CheckBox
				     style={{flex: 1, padding: 10}}
				     onClick={() => this.setState({used: !this.state.used, all: false})}
				     isChecked={this.state.used}
				     leftText={isRTL? "مـسـتـعـمـل" : 'Used'}
				     leftTextStyle={{fontSize:17}}
				 />
				 
		      <View style={{backgroundColor:'white', height:50, marginTop:5}}> 
		      	<Text style={{color:'grey', fontSize:17,margin:12, fontWeight:'bold'}}>{isRTL? "نوع النشاط" : "Seller Type"}</Text>
		      </View>
				<CheckBox
				     style={{flex: 1, padding: 10}}
				     onClick={this.onClickIndi.bind(this)}
				     isChecked={this.state.individual}
				     leftText={isRTL? "فردي" : 'Individual'}
				     leftTextStyle={{fontSize:17}}
				 />
				 <CheckBox
				     style={{flex: 1, padding: 10}}
				     onClick={()=>this.onClickEnter(this)}
				     isChecked={this.state.enterprise}
				     leftText={isRTL? "منشأة" : 'Enterprises'}
				     leftTextStyle={{fontSize:17}}
				 />
				<View style={{backgroundColor:'white', height:50}}> 
		      		<Text style={{color:'grey', fontSize:17,margin:12, fontWeight:'bold'}}>{isRTL? "التوصيل" : 'Delivery'}</Text>
		      	</View>
				 <CheckBox
				     style={{flex: 1, padding: 10}}
				     onClick={()=>this.onClickDl(this)}
				     isChecked={this.state.delivery}
				     leftText={isRTL? "التوصيل" : 'Delivery'}
				     leftTextStyle={{fontSize:17}}
				 />
			 	</ScrollView>
			 	<View style={styles.buttonBox}>
			 	<TouchableOpacity 
						style={styles._button}
						onPress={this.searchFilter.bind(this)}
					>
						<Text style={styles._buttonText} >{isRTL? "موافق" : "Apply"}</Text>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => this.props.setModalStatus(false)}
						style={styles._button} 
					>
						<Text style={styles._buttonText} >{isRTL? "الغاء" : "Cancel"}</Text>
					</TouchableOpacity>
			 </View>
			 </View>
			</Modal>
			</View>
		)
	}
}

const styles = StyleSheet.create ({
	container:{
		flexDirection: 'row',
		justifyContent: 'space-between',
		padding: 5,
		marginHorizontal:10,
		marginBottom:5,
		height: 55,
		alignItems: 'center',
		position: 'absolute',
		left: 0,
		right: 0,
		backgroundColor: 'white',
		flex:1,
	},
	icon:{
		width: 25,
		height: 25,
	},
	text:{
		color:'grey',
		marginTop: 5,
		paddingRight:10 
	},
	_descriptionbox:{
		flexDirection:'column',
		borderRadius:25,
	    backgroundColor:'white',
	    marginTop:40,
	    height: 200,
	    marginHorizontal: 10,
	    borderColor: 'black',
	    borderWidth: 2
	},
	adnum:{
		flex:1,
		fontSize:15,
		marginLeft: 10,
		marginRight: 0,
		paddingTop: 15,
		fontWeight: 'bold',
		color:'#7A7A7A'
	},
	_button:{
		alignItems: 'center',
		backgroundColor: '#737477',
		paddingVertical:10,
		paddingHorizontal:30,
		borderRadius:30,
		marginRight: 7,
		marginLeft: 7
	},
	_buttonText: {
		color:'#ffffff', 
		fontWeight: 'bold',
	},
	buttonBox: {
		margin:10, 
		justifyContent: 'center', 
		flexDirection:'row',
	}
})

function mapStateToProps(state) {
  return {
    location: state.search.location,
    status: state.search.status,
     isRTL: state.network.isRTL
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...searchActions}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Filter)
