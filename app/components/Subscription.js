import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  TextInput,
  Platform,
  ActivityIndicator,
} from 'react-native';
import Data from './data';
import TransactionHistory from './TransactionHistory';
import moment from 'moment';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Subscription';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
// import Prompt from 'react-native-prompt';
import Modal from 'react-native-modalbox';
import getCurrencyCode from './Currency.js';
import getCurrencyValue from './getCurrencyValue.js';
import getUserBalance from './getUserBalance';
import DeviceInfo from 'react-native-device-info';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';
import {PAY, CURRENCY} from "./Payment_Conf";


class Subscription extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      return {
        title: state.params.isRTL === true ? 'إدارة الاشتراك' : 'Subscriptions',
      };
    }
  };

  constructor() {
    super();
    this.state = {
      isLoading: true,
      isPlanLoading: false,
      planId: '',
      userPlan: '',
      subscription: [],
      renewalDate: '',
      token: '',
      isDisabled: false,
      country: '',
      extraPinCost: 0,
      totalPinCost: 0,
      totaPin: '',
      userPlanId: '',
      remainingPin: '',
      subscriptionDate: '',
      sellerSubscriptionPlan: {},
      userBalance: {},
      totalRemoveSalesPoint: '',
      removeExtraSalesPointLoader: false,
      reasonForCancel: '',
    };
    this.getTotalPinCost = this.getTotalPinCost.bind(this);
    // this.buyExtraSalesPointHandler = this.buyExtraSalesPointHandler.bind(this);
  }

  async getSubscription() {
    const {currencyCode} = this.props.country;
    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    const isRTL = this.props.isRTL;
    this.getSellerSubscriptionPlan(token);

    //getting current currency value from api
    if (currencyCode != 'SAR')
      currencyValue = await getCurrencyValue(currencyCode);

    this.setState({
      token: token,
      userData: userData,
    });

    fetch(API_URL + '/subscription-plan', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        //  console.warn('subscription-plan-->>',responseData.subscriptionData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول لنقطة البيع'
            : 'Failed to get Subscription plan';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          // console.warn('subscription--->>',responseData)
          this.setState({
            subscription: responseData.subscriptionData,
            isLoading: false,
          });
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
    const userBalance = await getUserBalance(token);

    AsyncStorage.getItem('isProfileCompleted').then(json => {
      const profileData = JSON.parse(json);
      // console.warn('profileData-->>',profileData)
      this.setState({userProfile: profileData.userProfile});
    });
    this.setState({userBalance});
  }

  //*****************************
  setSalesPointCost(sellerSubscriptionPlan) {
    const {
      renew_price_per_pin,
      subscription_plan,
      subscription_plan_type_id,
    } = sellerSubscriptionPlan;
    // let renewPinPrice = 0;
    // if(renew_price_per_pin !== 0 ) {
    //   renewPinPrice = renew_price_per_pin;
    // }
    const startDate = moment(sellerSubscriptionPlan.start_date);
    const endDate = moment(sellerSubscriptionPlan.end_date);
    const totalDays = endDate.diff(startDate, 'days');
    const fullPinCost = parseFloat(sellerSubscriptionPlan.price_per_pin);
    const currentDate = moment().format('YYYY-MM-DD');
    const remainingDays = endDate.diff(currentDate, 'days');
    const extraPinCost = (fullPinCost * remainingDays) / totalDays;

    // this.setState({fullPinCost: fullPinCost + renewPinPrice,
    //   extraPinCost: renewPinPrice + parseFloat(extraPinCost.toFixed(2)),
    //   userPlan: subscription_plan,
    //   userPlanId: subscription_plan_type_id
    // });

    this.setState({
      fullPinCost: fullPinCost,
      extraPinCost: parseFloat(extraPinCost.toFixed(2)),
      userPlan: subscription_plan,
      userPlanId: subscription_plan_type_id,
    });
  }

  setCountryDetail() {
    const country = this.props.country;
    const currency = getCurrencyCode(country.countryCode);
    country.currency = currency;
    this.setState({country});
  }

  getTotalPinCost(pin) {
    const totaPin = pin === '' ? 0 : pin;
    if (parseInt(totaPin)) {
      this.setState({
        totalPinCost: parseFloat(
          parseInt(totaPin) * this.state.extraPinCost,
        ).toFixed(2),
        totaPin: parseInt(totaPin),
      });
    } else if (totaPin === 0) {
      this.setState({totalPinCost: 0, totaPin: ''});
    }
  }

  componentDidMount() {
    const appName = DeviceInfo.getApplicationName();
    const deviceName = DeviceInfo.getDeviceName();
    const deviceId = DeviceInfo.getDeviceId();
    this.setState({appName, deviceId, deviceName});
    this.setCountryDetail();
    this.checkProfileCompleted();
    // this.getSubscription();
    this.props.navigation.addListener('didFocus', async event => {
      this.getSubscription();
    });
  }

  // componentWillUnmount() {
  //   this.props.navigation.removeListener('didFocus')
  // }
  customGoBackHandler = () => {
    // console.warn('customGoBackHandler....');
    this.getSubscription();
  };

  getSellerSubscriptionPlan(token) {
    const url = API_URL + '/get-seller-subscription-plan';

    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('get-seller-subscription-plan-->>',responseData);
        if (responseData && responseData.status == 200) {
          this.setState({sellerSubscriptionPlan: responseData.data});
          //this.setSalesPointCost(responseData.data);
        }
      })
      .catch(err => {
        Alert.alert('Error', err.message);
      });
  }

  checkProfileCompleted() {
    AsyncStorage.getItem('isProfileCompleted').then(json => {
      const res = JSON.parse(json);
      if (res) {
        this.setState({isProfileCompleted: res.isCompleted});
      }
    });
  }

  completeProfileHandler() {
    const {isRTL} = this.props;
    const message = isRTL
      ? 'يرجى اكمال حسابك الشخصي'
      : 'Please complete your profile';
    const yesText = this.props.isRTL ? 'نعم' : 'Yes';
    const laterButtonText = isRTL ? 'لاحقا' : 'Later';
    Alert.alert(
      '',
      message,
      [
        {
          text: yesText,
          onPress: () => {
            this.props.navigation.navigate('Profile');
          },
        },
        {
          text: laterButtonText,
        },
      ],
      {cancelable: false},
    );
    return;
  }

  getPlan(subscriptionPlan) {
    const {
      seller_type,
      created_ads,
      subscription_plan,
    } = this.state.sellerSubscriptionPlan;
    if (seller_type && seller_type.toLowerCase() === 'individual') {
      const isRTL = this.props.isRTL;
      const EngMessage =
        'Individual seller can not apply for the subscription plan';
      const ArbMessage = 'الاشتراك وترقية الحساب غير متاحة للبائع الفردي ';
      const message = isRTL ? ArbMessage : EngMessage;
      Alert.alert('', message);
      return;
    }

    const isProfileCompleted = this.state.isProfileCompleted;
    if (!isProfileCompleted) {
      this.completeProfileHandler();
      return;
    }

    const {navigate} = this.props.navigation;
    const {id, name, no_of_ads} = subscriptionPlan;

    const isRTL = this.props.isRTL;
    const msg = this.props.isRTL
      ? 'يرجى انشاء نقطة بيع قبل التقديم على خطة اشتراك'
      : 'Please create Sales Point before applying for subscription plan.';
    const msg2 = this.props.isRTL
      ? 'البائع الفردي لا يمكنه التقديم على خطة اشتراك ذهبي'
      : 'Individual seller can not apply for Golden subscription plan';
    const msg3 = this.props.isRTL
      ? `حاليا في حساب ${created_ads} اعلان. يمكنك إبقاء ${no_of_ads} إعلانات فقط ، يرجى إزالة الإعلانات ￼الضافية قبل تنزيل خطة اشتراكك￼ `
      : `Currently in your account ${created_ads} ads. You can keep only ${no_of_ads} ads, so kindly remove the extra ads before downgrading your subscription plan.`;

    if (!seller_type) {
      const yes = this.props.isRTL ? 'أنشئ نقطة بيع ' : 'Create Sales Point';
      const no = this.props.isRTL ? 'ليس الآن' : 'Not Now';
      Alert.alert(
        '',
        msg,
        [
          {text: yes, onPress: () => this.props.navigation.navigate('AddPin')},
          {text: no, style: 'cancel'},
        ],
        {cancelable: false},
      );
      return;
    }

    if (
      name.toLowerCase() === 'platinum' &&
      seller_type.toLowerCase() === 'individual'
    ) {
      Alert.alert('', msg2);
      return;
    }

    //console.warn(no_of_ads, created_ads, name, subscription_plan)
    // if(subscription_plan &&  subscription_plan.toLowerCase() === 'platinum' && name.toLowerCase() === 'premium' && no_of_ads < created_ads) {
    //     Alert.alert('',msg3);
    //     return;
    // }

    const url = API_URL + '/get-subscription-plan-type';
    this.setState({planId: id, isPlanLoading: true});
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        subscriptionId: id,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'فشل في الوصول لنقطة البيع'
            : 'Failed to get the subscription plan type';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          this.setState({planId: '', isPlanLoading: false});
          navigate('PaymentPlan', {
            subscriptionPlan: responseData,
            sellerSubscriptionPlan: this.state.sellerSubscriptionPlan,
            customGoBackHandler: this.customGoBackHandler,
          });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  onClickCancel = () => {
    this.setState({totalPinCost: 0, totaPin: ''});
    this.refs.modal1.close();
  };

  buyExtraSalesPointHandler() {
    const {
      token,
      userPlan,
      totaPin,
      extraPinCost,
      userPlanId,
      totalPinCost,
    } = this.state;
    const {currencyCode} = this.props.country;
    const {userId} = this.state.userData;
    const error = this.props.isRTL ? 'خطأ' : 'Error';
    const msg1 = this.props.isRTL
      ? 'لم يتم إيجاد كود العملة'
      : 'Currency code not found.';
    const msg2 = this.props.isRTL
      ? 'لم يتم إيجاد رقم المستخدم'
      : 'User Id not found';
    const msg3 = this.props.isRTL
      ? 'المخدم لا يتجاوب'
      : 'Server not responding';
    const msg4 = this.props.isRTL
      ? 'يرجى ادخال نقطة البيع'
      : 'Please enter sales points';
    if (!currencyCode) {
      Alert.alert(error, msg1);
      return;
    }

    if (!userId) {
      Alert.alert(error, msg2);
      return;
    }

    const requestData = {
      subscription_name: this.state.userPlan,
      no_of_extra_pin: this.state.totaPin,
      extra_pin_cost: this.state.extraPinCost,
      sub_plan_type_id: this.state.userPlanId,
      total_cost: this.state.totalPinCost,
    };

    if (totalPinCost && totaPin) {
      // console.warn('totalPinCost',totalPinCost)
      fetch(API_URL + '/buy-extra-pins', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: this.state.token,
        },
        body: JSON.stringify({
          amount: totalPinCost,
          pins: totaPin,
        }),
      })
        .then(response => response.json())
        .then(async responseData => {
          // console.warn('buy-extra-pins response-->>',responseData)
          if (responseData.status === 200) {
            const orderId = responseData.orderId;
            const payableAmount = this.state.totalPinCost;
            this.goForPayment(
              orderId,
              orderId,
              orderId,
              currencyCode,
              payableAmount,
              userId,
              requestData,
            );
            this.onClickCancel();
          } else {
            Alert.alert(error, msg3);
          }
        })
        .catch(err => {
          Alert.alert(error, err.message);
        });
    } else {
      Alert.alert(error, msg4);
    }
  }

  goForPayment = (
    orderId,
    tId,
    cTid,
    currencyCode,
    payableAmount,
    userId,
    requestData,
  ) => {
    const {appName, deviceId, deviceName} = this.state;
    const {name, email} = this.state.userProfile;
    const msg = this.props.isRTL
      ? ' يرجى اكمال ملفك الشخصي اولا'
      : 'Please complete your profile first';
    const error = this.props.isRTL ? 'خطأ' : 'Error';
    if (!email || !name) {
      Alert.alert('', msg);
      return;
    }
    const test = PAY;
    const currency_test = CURRENCY;
    const url = 'https://secure.innovatepayments.com/gateway/mobile.xml';
    const data =
      '<?xml version="1.0" encoding="UTF-8"?>\
    <mobile>\
        <store>20793</store>\
        <key>KnXhH-xHFZV#3FNf</key> \
        <device>\
          <type>' +
      deviceName +
      '</type>\
          <id>' +
      deviceId +
      '</id>\
          <agent></agent>\
          <accept></accept>\
        </device>\
        <app>\
          <name>' +
      appName +
      '</name>\
          <version>beta</version>\
          <user>' +
      name +
      '</user>\
          <id>' +
      userId +
      '</id>\
        </app>\
      <tran>\
        <test>'+test+'</test>\
        <type>PAYPAGE</type>\
        <cartid>' +
      orderId +
      '</cartid>\
        <description>Transaction description</description>\
        <currency>' +
      currency_test +
      '</currency>\
        <amount>' +
      payableAmount +
      '</amount>\
      </tran>\
      <billing>\
        <email>' +
      email +
      '</email>\
      </billing>\
    </mobile>';
    fetch(url, {
      method: 'POST',

      headers: {
        Accept: 'application/xml',
        'Content-Type': 'text',
      },
      body: data,
    })
      .then(response => response.text())
      .then(responseData => {
        // console.warn('xml response-->>',responseData)
        //console.warn('xml response-->>',responseData.match('<start>(.*)</start>')[1])
        const start = responseData.match('<start>(.*)</start>')[1];
        const close = responseData.match('<close>(.*)</close>')[1];
        const abort = responseData.match('<abort>(.*)</abort>')[1];
        const code = responseData.match('<code>(.*)</code>')[1];
        const urls = {start, close, abort, code};

        this.props.navigation.navigate('Payment', {
          urls,
          requestData,
          screen: 'buyExtraSalesPoint',
          type: 'buyExtraSalesPoint',
          orderId,
          tId,
          cTid,
          token: this.state.token,
          onGoBack: () => this.customGoBackHandler(),
        });
      })
      .catch(err => {
        Alert.alert(error, err);
      });
  };

  confirmCancelSubscriptionPLan = () => {
    const {
      total_pin,
      created_ads,
      used_pin,
      platform,
    } = this.state.sellerSubscriptionPlan;

    if (Platform.OS !== platform.toLowerCase()) {
      const message = `Your have subscribed your subscription plan from ${platform}, kindly cancel your plan from ${platform} device`;
      Alert.alert('', message);
      return;
    }
    const {no_of_pin, no_of_ads} = this.state.subscription[2];
    //console.warn(no_of_pin)

    const msg = this.props.isRTL
      ? `حاليا في حسابك ${used_pin} نقطة بيع  و ${created_ads}  اعلان لتنزيل حسابك الى المجاني يمكنك إبقاء ${no_of_pin}نقطة بيع و ${no_of_ads} اعلان يرجى إزالة نقاط البيع والاعلانات الزائدة قبل الغاء اشتراكك`
      : `Currently you have used ${used_pin} Sales Point and ${created_ads} ads. You can keep ${no_of_pin} Sales Point and ${no_of_ads} ads in each Sales Point. Kindly remove extra Sales Point and Ads before cancellation.`;
    const msg2 = this.props.isRTL
      ? 'هل تريد الغاء اشتراكك الحالي؟'
      : 'Do you want to cancel your Subscription ?';
    const yes = this.props.isRTL ? 'نعم' : 'Yes';
    const no = this.props.isRTL ? 'لا' : 'No';
    if (used_pin > no_of_pin || created_ads > no_of_ads) {
      Alert.alert('', msg);
      return;
    }

    Alert.alert(
      msg2,
      '',
      [
        {text: yes, onPress: () => this.refs.cancellationReasonModal.open()},
        {text: no, style: 'cancel'},
      ],
      {cancelable: false},
    );
  };

  submitCancellationReasonHandler = () => {
    const msg = this.props.isRTL ? 'بنجاح' : 'Success';
    const error = this.props.isRTL ? 'خطأ' : 'Error';
    const reason = this.state.reasonForCancel;
    const token = this.state.token;
    fetch(API_URL + '/cancel-subscription', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({reason}),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('cancel-subscription-->>',responseData)
        if (responseData.status == 200) {
          endDate = this.state.sellerSubscriptionPlan.end_date;
          const text = this.props.isRTL
            ? `تم الغاء اشتراكك الحالي بنجاح وسيتم تحويل كل نقاط بيعك الى نقاط بيع مجانيه بعد انتهاء اشتراكك الحالي ${endDate}`
            : `Your subscription plan has been cancelled successfully and all your sales points will turn to Free after ${endDate}`;
          Alert.alert(msg, text);
          this.refs.cancellationReasonModal.close();
          this.getSellerSubscriptionPlan(token);
          if (Platform.OS === 'ios') {
            const enMsg = 'Cancel the Subscription Plan from iTunes as well';
            const arMsg = 'قم بالغاء الاشتراك من متجر ابل ايضا';
            const message = this.props.isRTL ? arMsg : enMsg;
            Alert.alert('', message);
          }
        } else {
          Alert.alert(error, responseData.data);
        }
      })
      .catch(err => {
        Alert.alert(error, err.message);
      });
  };

  showSubscriptionPLan = () => {
    this.refs.subscriptionModal.open();
  };

  cancelSubscriptionModal = () => {
    this.refs.subscriptionModal.close();
  };

  cancelRemoveExtraSalesPointModal = () => {
    this.setState({
      totalRemoveSalesPoint: '',
      removeExtraSalesPointLoader: false,
      reasonForCancel: '',
    });
    // this.refs.removeExtraSalesPointModal.close();
    this.refs.cancellationReasonModal.close();
  };

  setTotalRemoveSalesPoint = totalRemoveSalesPoint => {
    if (parseInt(totalRemoveSalesPoint)) {
      this.setState({totalRemoveSalesPoint: totalRemoveSalesPoint});
    } else {
      this.setState({totalRemoveSalesPoint: ''});
    }
  };

  removeExtraSalesPointHandler = () => {
    const {total_pin, used_pin} = this.state.sellerSubscriptionPlan;
    const remainingPin = total_pin - used_pin;
    const totalRemoveSalesPoint = this.state.totalRemoveSalesPoint;
    const error = this.props.isRTL ? 'خطأ' : 'Error';
    const msg = this.props.isRTL
      ? 'يرجى ادخال نقطة البيع'
      : 'Please enter the Sales Point';
    const limit = this.props.isRTL
      ? 'خطأ : تجاوز للحد '
      : 'Error: Limit exceeded';
    const msg2 = this.props.isRTL
      ? `يمكنك إزالة بحد اقصى ${remainingPin}  نقاط بيع ، اذا رغبت بإزالة المزيد فيجب مسح نقاط البيع من￼ ￼قائمة نقاط بيعك وجعلها فارغة`
      : `You can remove maximum ${remainingPin} Sales Point. If you want to remove more, then you have to delete Sales Point from your Sales Point list.`;
    if (totalRemoveSalesPoint === '' || parseInt(totalRemoveSalesPoint) === 0) {
      Alert.alert(error, msg);
      return;
    }

    if (remainingPin < totalRemoveSalesPoint) {
      Alert.alert(limit, msg2);
      return;
    }

    this.setState({removeExtraSalesPointLoader: true});
    fetch(API_URL + '/delete-extra-sale-point', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        deleted_pins: totalRemoveSalesPoint,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        if (responseData.status == 200) {
          endDate = this.state.sellerSubscriptionPlan.end_date;
          const msg = this.props.isRTL ? 'بنجاح' : 'Success';
          const text = this.props.isRTL
            ? `تم إزالة ${totalRemoveSalesPoint} نقاط بيع بنجاح`
            : `You have removed ${totalRemoveSalesPoint} extra sales point successfully`;
          Alert.alert(msg, text);
          this.cancelRemoveExtraSalesPointModal();
          this.getSellerSubscriptionPlan(this.state.token);
        } else {
          Alert.alert(error, responseData.data);
        }
      })
      .catch(err => {
        Alert.alert(error, err.message);
      });
  };

  changePlatinumToGold(planType) {
    if (!planType) {
      return;
    }

    if (planType.toLowerCase() === 'platinum') {
      return this.props.isRTL ? 'ذهبي' : 'Golden';
    } else if (planType.toLowerCase() === 'premium') {
      return this.props.isRTL ? 'مميز' : planType;
    } else {
      return this.props.isRTL ? 'مجاني' : planType;
    }
  }

  render() {
    // console.warn('countryLocation-->>',this.state.subscription[2], this.state.sellerSubscriptionPlan)
    // const { renewalDate, subscriptionDate } = this.state;
    const {
      total_pin,
      used_pin,
      subscription_plan,
      start_date,
      end_date,
      subscription_plan_type_id,
      platform,
      month,
      pins,
      title,
      plan_price,
      cancel_status,
      seller_type,
    } = this.state.sellerSubscriptionPlan;
    // console.warn('cancelled_status',cancelled_status)
    const remainingPin = total_pin - used_pin;
    const isRTL = this.props.isRTL;
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {navigate} = this.props.navigation;
    const cancelStatusEN = `You have cancelled your current subscription plan and your account will turn to free after ${end_date}`;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.showSubscriptionPLan}
          style={styles.planButton}>
          <Text style={{color: '#00ADDE'}}>
            {this.props.isRTL ? 'الخطة الحالية' : 'Current Plan'}
          </Text>
        </TouchableOpacity>
        {cancel_status == '1' && (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              margin: 10,
            }}>
            <Text style={{color: 'red'}}>{cancelStatusEN}</Text>
          </View>
        )}
        <ScrollView>
          {this.state.isLoading ? (
            <PreLoader />
          ) : this.state.subscription.length ? (
            this.state.subscription.map((data, i) => {
              if (data.name.toLowerCase() === 'free') {
                return null;
              }
              return (
                <TouchableOpacity
                  style={styles._messageBox}
                  key={i}
                  onPress={() => this.getPlan(data)}>
                  <View style={styles._textBox}>
                    <View
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={[styles._title, {color: '#000'}]}>
                        {this.props.isRTL
                          ? data.name === 'Premium'
                            ? 'تميز'
                            : 'ذهبي'
                          : this.changePlatinumToGold(data.name)}
                      </Text>
                      {this.state.isPlanLoading &&
                      this.state.planId === data.id ? (
                        <ActivityIndicator size="small" color="#3399ff" />
                      ) : null}
                      <Text style={[styles._date, {color: '#00ADDE'}]}>
                        {strings('subscription.subscribe')}
                      </Text>
                    </View>
                    <View style={styles._descriptionContainer}>
                      <Text style={styles._description}>
                        {this.props.isRTL
                          ? data.arabic_description
                          : data.description}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })
          ) : (
            <NoDataFound />
          )}

          <View
            style={{marginTop: 10, alignItems: 'flex-end', marginRight: 30}}>
            <Text style={{fontWeight: 'bold'}}>
              {isRTL ? 'اجمالي نقاط البيع:' : 'Total Sales Point:'}{' '}
              {total_pin ? total_pin : 0}
            </Text>
          </View>
          <View
            style={{marginTop: 10, alignItems: 'flex-end', marginRight: 30}}>
            <Text style={{fontWeight: 'bold'}}>
              {isRTL ? 'نقاط البيع المستخدمة:' : 'Used Sales Point:'}{' '}
              {used_pin ? used_pin : 0}
            </Text>
          </View>
          <View
            style={{marginTop: 10, alignItems: 'flex-end', marginRight: 30}}>
            <Text style={{fontWeight: 'bold'}}>
              {isRTL ? 'نقاط البيع المتبقية:' : 'Remaining Sales Point:'}{' '}
              {remainingPin ? remainingPin : 0}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 30,
            }}>
            {/* {subscription_plan && subscription_plan.toLowerCase() !== 'free' && (
              <TouchableOpacity onPress={() => navigate('TransactionHistory')}>
                <Text style={styles.txnText}>
                  {strings('subscription.transaction')}
                </Text>
              </TouchableOpacity>
            )} */}
            <TouchableOpacity onPress={() => navigate('TransactionHistory')}>
                <Text style={styles.txnText}>
                  {strings('subscription.transaction')}
                </Text>
              </TouchableOpacity>
          </View>

          {subscription_plan &&
          subscription_plan.toLowerCase() !== 'free' &&
          cancel_status == '0' ? (
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 20,
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  alignItems: 'center',
                  borderBottomLeftRadius: 25,
                  borderBottomRightRadius: 25,
                  borderTopLeftRadius: 25,
                  borderTopRightRadius: 25,
                  width: '35%',
                  height: Platform.OS === 'ios' ? 40 : 30,
                  marginTop: 5,
                  alignItems: 'center',
                  marginLeft: 25,
                }}
                onPress={this.confirmCancelSubscriptionPLan}>
                <Text
                  style={{
                    fontSize: 12,
                    fontWeight: 'bold',
                    marginTop: 5,
                    alignItems: 'center',
                    color: '#e60000',
                    marginTop: Platform.OS === 'ios' ? 11 : 5,
                  }}>
                  {this.props.isRTL ? 'الغاء الاشتراك' : 'Cancel Subscription'}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </ScrollView>

        <Modal
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: 300,
            width: 250,
            padding: 15,
          }}
          position={'center'}
          ref={'subscriptionModal'}
          isDisabled={this.state.isDisabled}>
          <Text style={{fontWeight: 'bold', fontSize: 18, marginBottom: 15}}>
            {isRTL ? 'تفاصيل الاشتراك' : 'Subscription Detail'}
          </Text>
          <View style={{}}>
            <View>
              <Text style={{fontWeight: 'bold'}}>
                {this.props.isRTL ? 'الاشتراك الحالي' : 'Current Plan'} :{' '}
                {this.changePlatinumToGold(subscription_plan)}
              </Text>
              {subscription_plan !== 'Free' && (
                <View>
                  <Text>
                    {isRTL ? 'مدة الاشتراك:' : 'Duration: '} {month}{' '}
                  </Text>
                  <Text>
                    {isRTL ? 'تاريخ البداية:' : 'Start Date: '} {start_date}{' '}
                  </Text>
                  <Text>
                    {isRTL ? 'تاريخ النهاية:' : 'End Date: '} {end_date}{' '}
                  </Text>
                  <Text>
                    {isRTL ? 'عدد نقاط البيع:' : 'Total Pins: '} {total_pin}{' '}
                  </Text>
                  <Text>
                    {isRTL ? 'مبلغ الاشتراك:' : 'Price: '} {plan_price}{' '}
                  </Text>
                  <Text>
                    {isRTL ? 'وصف الاشتراك:' : 'Description: '} {title}{' '}
                  </Text>
                </View>
              )}
            </View>
            {/* { renew_plan !== '' ?
                <View>
                  <Text></Text>
                  <Text style={{fontWeight:'bold'}}>{isRTL ? 'خطة الاشتراك القادمة:' : 'Next Plan:'} {this.changePlatinumToGold(renew_plan)} </Text>
                  <Text>{isRTL ? 'تاريخ البداية:' : 'Renew Start Date:'} {renew_start_date} </Text>
                  <Text>{isRTL ? 'تاريخ النهاية:' : 'Renew End Date:'} {renew_end_date} </Text>
                </View>
              : null
              } */}
            <View
              style={{
                flexDirection: isRTL ? 'row-reverse' : 'row',
                marginTop: 20,
                justifyContent: 'space-evenly',
              }}>
              <TouchableOpacity
                onPress={this.cancelSubscriptionModal}
                style={{
                  borderRadius: 25,
                  backgroundColor: '#0099e6',
                  padding: 10,
                  paddingHorizontal: 15,
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#FFF',
                    paddingHorizontal: 20,
                  }}>
                  {isRTL ? 'إلغاء' : 'OK'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal
          style={{
            justifyContent: 'center',
            // alignItems: 'center',
            height: 300,
            width: 260,
            padding: 15,
          }}
          position={'center'}
          ref={'cancellationReasonModal'}
          isDisabled={false}>
          <Text style={{fontWeight: 'bold', fontSize: 18}}>
            {isRTL ? 'سبب الغاء الحساب' : 'Reason For Cancellation'}
          </Text>
          {Platform.OS === 'ios' ? (
            <View
              style={{
                borderBottomWidth: 2,
                marginVertical: 10,
                width: 220,
                borderBottomColor: 'grey',
              }}>
              <TextInput
                multiline={true}
                numberOfLines={4}
                style={{width: 200, fontSize: 15, marginVertical: 10}}
                value={this.state.reasonForCancel}
                //placeholder={isRTL ? 'أدخل لا من نقطة البيع' : "Type reason here(optional)"}
                onChangeText={resion =>
                  this.setState({reasonForCancel: resion})
                }
                textAlignVertical="top"
              />
            </View>
          ) : (
            <TextInput
              style={{width: 200}}
              multiline={true}
              numberOfLines={4}
              value={this.state.reasonForCancel}
              placeholder={
                isRTL ? 'أدخل لا من نقطة البيع' : 'Type reason here(optional)'
              }
              onChangeText={resion => this.setState({reasonForCancel: resion})}
              textAlignVertical="top"
            />
          )}
          <View
            style={{
              flexDirection: isRTL ? 'row-reverse' : 'row',
              marginTop: 20,
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              disabled={this.state.removeExtraSalesPointLoader}
              onPress={this.submitCancellationReasonHandler}
              style={{
                flexDirection: 'row',
                borderRadius: 25,
                backgroundColor: '#0099e6',
                padding: 10,
                paddingHorizontal: 15,
                margin: 5,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: '#FFF',
                  paddingHorizontal: 10,
                }}>
                {isRTL ? 'تأكيد' : 'Submit'}
              </Text>
              {this.state.removeExtraSalesPointLoader ? (
                <ActivityIndicator size="small" color="#000000" />
              ) : null}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={this.cancelRemoveExtraSalesPointModal}
              style={{
                borderRadius: 25,
                backgroundColor: '#e62e00',
                padding: 10,
                paddingHorizontal: 15,
                margin: 5,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: '#FFF',
                  paddingHorizontal: 10,
                }}>
                {isRTL ? 'إلغاء' : 'Cancel'}
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
    country: state.network.countryLocation,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Subscription);

const styles = StyleSheet.create({
  _deleteButton: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  _buttonText: {
    fontSize: 17,
    fontWeight: 'bold',
    marginTop: 13,
    alignItems: 'center',
  },
  _button: {
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    width: '48%',
    height: 50,
    marginTop: 20,
    alignItems: 'center',
  },
  _deletebuttonText: {
    color: '#e60000',
  },
});
