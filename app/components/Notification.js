import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  AsyncStorage,
  Button,
  Platform,
  Dimensions,
  Alert,
  FlatList,
} from 'react-native';
import SearchInput, {createFilter} from 'react-native-search-filter';
const {width, height} = Dimensions.get('window');
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import * as notificationAction from '../actions/notificationAction';
import StyleSheetFactory from './../styles/Notification';
import moment from 'moment';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import API_URL from '../configuration/configuration';

const KEYS_TO_FILTERS = ['title', 'description'];

class Notification extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;
    if (state.params !== undefined) {
      return {
        title: state.params.isRTL === true ? 'تنبيهات' : 'Notifications',
      };
    }
  };

  constructor() {
    super();
    this.state = {
      isLoading: true,
      searchText: '',
      selectedMessageNotification: false,
      notification: [],
    };
    this.handleSearch = this.handleSearch.bind(this);
  }
  componentDidMount() {
    const {setParams} = this.props.navigation;
    setParams({isRTL: this.props.isRTL});
    this.getData();
    // console.warn(this.props);
    // console.warn("componentDidMount");
  }

  getData() {
    AsyncStorage.getItem('user_data').then(data => {
      const userData = JSON.parse(data);
      const token = userData.sData;
      this.setState({token: token, userID: userData.userId});
      fetch(API_URL + '/get-notification-list', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: token,
        },
      })
        .then(response => response.json())
        .then(responseData => {
          // console.warn('Notifications-->>',responseData.notification_list);
          // if (
          //   responseData.status == 654864 &&
          //   responseData.eData.eCode == 545864
          // ) {
          //   alert('Failed to get pin');
          // } else
          if (
            responseData.status == 654864 &&
            responseData.sData == null &&
            responseData.eData == null
          ) {
            this.setState({isLoading: false, notification: []});
          } else if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            this.setState({
              notification: responseData.notification_list,
              isLoading: false,
            });
          } else if (
            responseData.status == 654864 &&
            responseData.eData.eCode == 545864
          ) {
            alert('Data not found!');
          }
        })
        .catch(err => {
          console.warn(err);
        });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isRTL != this.props.isRTL) {
      this.props.navigation.setParams({isRTL: nextProps.isRTL});
      this.props.navigation.state.params.isRTL = nextProps.isRTL;
    }
  }

  handleSearch(event) {
    this.setState({searchText: event});
  }

  onPressClear = () => {
    if (!this.state.notification.length) {
      Alert.alert('', 'There is no data');
      return;
    }
    const {isRTL} = this.props;
    const message = isRTL
      ? 'هل تود مسح كل التنبيهات؟ '
      : 'Do you want to clear all notification?';
    const yesText = this.props.isRTL ? 'نعم' : 'Yes';
    const noText = this.props.isRTL ? 'لا' : 'No';
    Alert.alert(
      '',
      message,
      [
        {text: noText, style: 'cancel'},
        {text: yesText, onPress: () => this.onPressClearNotification()},
      ],
      {cancelable: false},
    );
  };

  onPressClearNotification() {
    const {isRTL} = this.props;
    fetch(API_URL + '/delete-all-notification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        userId: this.state.userID,
      }),
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn(responseData);
        if (
          responseData.status === 516324 &&
          responseData.sData === null &&
          responseData.eData === null
        ) {
          const message = isRTL
            ? 'تم مسح كل التنبيهات بنجاح'
            : 'Notification deleted successfully';
          Alert.alert('', message);
          this.getData();
          this.props.resetNotificationCount();
        }
        // if (responseData.list.length && toastId != responseData.list[0].toastId)
        // this.setState({ toastMessage: responseData.list });
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  handleNotification(notification) {
    fetch(API_URL + '/read-unread-notification', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      },
      body: JSON.stringify({
        notiId: notification.id,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        this.getData();
        this.props.resetNotificationCount();
        // if (responseData.list.length && toastId != responseData.list[0].toastId)
        // this.setState({ toastMessage: responseData.list });
        // this.props.navigation.navigate('ChatMessages',{arr:{to_user_name:'XYZ'}})
      })
      .catch(err => {
        console.log(err);
      });
  }

  navigateNotification = item => {
    console.warn("notification : ",item)
    const {target_screen, screen_value} = item;

    let screenValue = JSON.parse(screen_value);
    console.warn('notif screenValue-->>',screenValue, target_screen)

    if (!screenValue) {
      screenValue = {};
    }

    switch (target_screen) {
      case 'pin':
        const {pinid, pin_name, user_id} = screenValue;
        this.props.navigation.navigate('SellerContainer', {
          item: {id: pinid, pin_name, user_id},
        });
        break;
      case 'pin approved':
        this.props.navigation.navigate('Pin');
        break;
      case 'publish':
        this.props.navigation.navigate('Pin');
        break;
      case 'message':
        this.props.navigation.navigate('Message');
        break;
      case 'news':
        // const { pinid, pin_name, user_id } = screenValue;
        this.props.navigation.navigate('SellerContainer', {
          item: {
            id: screenValue.pinid,
            pin_name: screenValue.pin_name,
            user_id: screenValue.user_id,
          },
        });
        break;
      case 'commercials':
        this.props.navigation.navigate('ManageCommercial');
        break;
      case 'plan':
        this.props.navigation.navigate('Subscription');
        break;
      case 'AdDetails':
        this.props.navigation.navigate('AdDetails', {
          item: {
            id: screenValue.adId,
            pin_id: screenValue.pinid,
            pin_name: screenValue.pin_name,
            user_id: screenValue.user_id,
          },
        });
        break;
      case 'Ads': // for offer alert
        this.props.navigation.navigate('SellerContainer', {
          item: {
            id: screenValue.pinid,
            pin_name: screenValue.pin_name,
            user_id: screenValue.user_id,
          },
        });
        break;
    }
  };

  render() {
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const isRTL = this.props.isRTL;
    const filteredData = this.state.notification.filter(
      createFilter(this.state.searchText, KEYS_TO_FILTERS),
    );
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 0,
            flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
            justifyContent: 'space-between',
            marginVertical: 15,
          }}>
          <View
            style={[
              styles._searchText,
              {
                flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              },
            ]}>
            <View
              style={[
                styles._textbox,
                {
                  width: '85%',
                  height: Platform.OS === 'ios' ? '60%' : '100%',
                  marginTop: Platform.OS === 'ios' ? 15 : 3,
                  flex: 2,
                },
              ]}>
              <SearchInput
                onChangeText={text => this.handleSearch(text)}
                style={styles.searchInput}
                placeholder={isRTL ? 'بحث' : 'Search'}
              />
            </View>
            <TouchableOpacity>
              <Image
                style={{
                  width: 20,
                  marginHorizontal: 5,
                  resizeMode: 'contain',
                  marginRight: 10,
                }}
                source={require('./../icons/search3x.png')}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.clearButton}
            onPress={this.onPressClear.bind(this)}>
            <Text style={{fontSize: 16}}>
              {isRTL ? 'مسح الكل' : 'Clear all'}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flex: 1}}>
          {this.state.isLoading ? (
            <PreLoader />
          ) : this.state.notification.length == 0 ? (
            <NoDataFound />
          ) : (
            this.state.notification && (
              <FlatList
                data={filteredData}
                keyExtractor={item => item.id.toString()}
                renderItem={({item}) => {
                  const dateTime = item.created_at;
                  var date = moment(dateTime).format('DD MMM');
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.handleNotification(item);

                        // this.props.navigation.navigate("ListView")
                        this.navigateNotification(item);
                      }}
                      key={item.id}>
                      <View
                        style={[
                          styles._messageBox,
                          item.id == 1
                            ? {
                                borderTopLeftRadius: 25,
                                borderTopRightRadius: 25,
                              }
                            : null,
                          {backgroundColor: item.read ? '#FFF' : '#edeeef'},
                        ]}>
                        <View style={styles._textBox}>
                          <View
                            style={{
                              flexDirection: this.props.isRTL
                                ? 'row-reverse'
                                : 'row',
                              justifyContent: 'space-between',
                            }}>
                            <View style={{width: '85%'}}>
                              <Text
                                style={[
                                  styles._title,
                                  {textAlign: isRTL ? 'right' : null},
                                ]}>
                                {' '}
                                {isRTL ? item.arb_title : item.title}
                              </Text>
                            </View>
                            <Text style={styles._date}>{date}</Text>
                          </View>
                          <View style={styles._descriptionContainer}>
                            <Text
                              style={[
                                styles._description,
                                {textAlign: isRTL ? 'right' : null},
                              ]}>
                              {isRTL
                                ? item.arb_notification
                                : item.notification}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            )
          )}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {...layoutActions, ...notificationAction},
    dispatch,
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
