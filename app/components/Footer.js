import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  StyleSheet,
  AsyncStorage,
  Linking,
  Platform,
  Image,
} from 'react-native';
// import SocialSharing from './SocialSharing';
import Communications from 'react-native-communications';
import MessageModal from './MessageModal';
import {connect} from 'react-redux';
import Share from 'react-native-share';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';

import API_URL, {WEB_URL} from '../configuration/configuration';
import database from '@react-native-firebase/database';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      call: false,
      message: false,
      share: false,
      direction: false,
      modalVisible: false,
      token: '',
      toggle: true,
    };
    this.setModalStatus = this.setModalStatus.bind(this);
    this.daylist = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday ',
      'Thursday',
      'Friday',
      'Saturday',
    ];
  }

  toggleVisible = () => {
    this.setState({toggle: !this.state.toggle});
  };

  async componentDidMount() {
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    if (userData) {
      this.setState({token: userData.sData});
    }
  }

  showDirection() {
    let type = 'directions_clicks';
    this.successCalls(type);
    let lat;
    let lng;
    if (this.props.footerLat && this.props.footerLng) {
      lat = this.props.footerLat;
      lng = this.props.footerLng;
    } else {
      lat = this.props.navigation.state.params.item.lat;
      lng = this.props.navigation.state.params.item.lng;
    }
    let fun = Platform.select({
      ios: () => {
        Linking.openURL('http://maps.apple.com/maps?daddr=' + lat + ',' + lng);
      },
      android: () => {
        Linking.openURL(
          'http://maps.google.com/maps?daddr=' + lat + ',' + lng,
        ).catch(err => console.error('An error occurred', err));
      },
    });
    database()
      .ref(
        'SHOPS/SHOP/' +
          this.props.navigation.state.params.item.pin_name +
          '/directions',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });

    // DATE UPDATE

    database()
      .ref(
        'SHOPS/SHOP/' +
          this.props.navigation.state.params.item.pin_name +
          '/date/' +
          new Date().toDateString() +
          '/directions',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });
    //Daily update
    database()
      .ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/directions')
      .transaction(total => {
        return (total || 0) + 1;
      });

    fun();
  }

  userNotLoggedIn = () => {
    const {navigate} = this.props.navigation;
    Alert.alert(
      '',
      'You need to login',
      [
        {text: 'Login', onPress: () => navigate('Login')},
        {text: 'SignUp', onPress: () => navigate('SignUp')},
        {text: 'Cancel'},
      ],
      {cancelable: false},
    );
  };
  setModalStatus(status) {
    if (status) {
      let type = 'message_clicks';
      this.successCalls(type);
    }

    this.setState({modalVisible: status});
  }

  socialShare() {
    //console.warn('--->>',this.props.sharingInfo);
    /**
			url should follow
			for pin-->> "https://simsimmarket/pin/pin_name/user_id/pin_id"
	  		for ad-->> "https://simsimmarket/ad/ad_name/pin_id/pinName/ad_id"
      */
    let type = 'sharing_clicks';
    this.successCalls(type);
    const ob = this.props.sharingInfo;
    console.warn('aman', ob.sharingObject);
    let url, titleDesc;
    if (ob.sharingObject == 'pin') {
      const {id, pin_name, user_id, description} = ob;

      // url =
      //   'https://simsim-markets.com/pins/' +
      //   pin_name.replace(/ +/g, '_') +
      //   '/' +
      //   user_id +
      //   '/' +
      //   id;
      url = WEB_URL + '/pins/' + id;
      titleDesc = pin_name + ':- ' + description;
      const shareOptions = {
        title: 'SimSim Markets',
        message: titleDesc,
        url: url,
        subject: 'This is subject',
      };
      Share.open(shareOptions, function(e) {
        console.warn(e);
      });

      database()
        .ref('SHOPS/SHOP/' + pin_name + '/sharing')
        .transaction(total => {
          return (total || 0) + 1;
        });

      // DATE UPDATE

      database()
        .ref(
          'SHOPS/SHOP/' +
            pin_name +
            '/date/' +
            new Date().toDateString() +
            '/sharing',
        )
        .transaction(total => {
          return (total || 0) + 1;
        });

      // Daily update
      database()
        .ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/sharing')
        .transaction(total => {
          return (total || 0) + 1;
        });
    } else if (ob.sharingObject == 'ad') {
      const {id, product_name, pin_id, pinName, description} = ob;
      console.warn(id, product_name, pin_id, pinName);

      titleDesc = product_name + ' - ' + description;
      //   url = `https://www.simsim-markets.com/ads/${product_name.replace(
      //     / +/g,
      //     '_',
      //   )}/${pin_id}/${pinName.replace(/ +/g, '_')}/${id}`;
      // }
      url = WEB_URL + '/ads/' + id;
      const shareOptions = {
        title: 'SimSim Markets',
        message: titleDesc,
        url: url,
        subject: 'This is subject',
      };
      Share.open(shareOptions, function(e) {
        console.warn(e);
      });
    }
  }
  successCalls(types) {
    const pinId = this.props.navigation.state.params.item.id;
    fetch(API_URL + '/successCalls', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({pinId: pinId, type: types}),
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn('successCalls responseData-->>', responseData);
      });
  }
  isLogin = () => {
    if (this.state.token) {
      this.setModalStatus(true);
    } else {
      this.userNotLoggedIn();
    }
  };
  callHandler(contact) {
    let type = 'call_clicks';
    this.successCalls(type);
    const {
      working_from_wn,
      timing_from_wd,
      timing_to_wd,
      timing_from_wn,
      timing_to_wn,
    } =
      this.props.navigation.state.params.pinDetails ||
      this.props.navigation.state.params.item;
    //console.warn('timing-->',working_from_wn, timing_from_wd, timing_to_wd, timing_from_wn,timing_to_wn)
    const today = new Date();
    const getDayIndex = today.getDay();
    const day = this.daylist[getDayIndex];
    let weekEnd, isWeekEnd;
    if (working_from_wn) {
      weekEnd = working_from_wn.split(',');
      isWeekEnd = weekEnd.includes(day);
    }
    database()
      .ref(
        'SHOPS/SHOP/' +
          this.props.navigation.state.params.item.pin_name +
          '/calls',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });

    // DATE UPDATE

    database()
      .ref(
        'SHOPS/SHOP/' +
          this.props.navigation.state.params.item.pin_name +
          '/date/' +
          new Date().toDateString() +
          '/calls',
      )
      .transaction(total => {
        return (total || 0) + 1;
      });

    if (isWeekEnd) {
      if (timing_from_wn === 'Closed' || timing_to_wn === 'Closed') {
        const message = this.props.isRTL
          ? 'مغلق في نهاية الأسبوع'
          : 'Closed on weekend';
        Alert.alert(message);
      } else {
        var currentTime = moment();
        const startTime = moment(timing_from_wn, 'h:mm a');
        const endTime = moment(timing_to_wn, 'h:mm a');

        if (startTime.hour() >= 12 && endTime.hour() <= 12) {
          endTime.add(1, 'days'); // handle spanning days
        }
        var isBetween = currentTime.isBetween(startTime, endTime);

        if (isBetween) {
          Communications.phonecall(contact, true);
        } else {
          const msg = this.props.isRTL
            ? 'خارج وقت العمل'
            : "It's out of working time.";
          const text = this.props.isRTL
            ? 'هل تود ارسال رسالة للبائع؟'
            : 'Do you want to message him';
          const yes = this.props.isRTL ? 'نعم' : 'Yes';
          const no = this.props.isRTL ? 'لا' : 'No';
          Alert.alert(
            msg,
            text,
            [
              {text: no, style: 'cancel'},
              {text: yes, onPress: () => this.isLogin()},
            ],
            {cancelable: false},
          );
        }
      }
    } else {
      var currentTime = moment();
      const startTime = moment(timing_from_wd, 'h:mm a');
      const endTime = moment(timing_to_wd, 'h:mm a');

      if (startTime.hour() >= 12 && endTime.hour() <= 12) {
        endTime.add(1, 'days'); // handle spanning days
      }

      var isBetween = currentTime.isBetween(startTime, endTime);

      if (isBetween) {
        Communications.phonecall(contact, true);
      } else {
        const msg = this.props.isRTL
          ? 'خارج وقت العمل'
          : "It's out of working time.";
        const text = this.props.isRTL
          ? 'هل تود ارسال رسالة للبائع؟'
          : 'Do you want to message him';
        const yes = this.props.isRTL ? 'نعم' : 'Yes';
        const no = this.props.isRTL ? 'لا' : 'No';
        Alert.alert(
          msg,
          text,
          [
            {text: no, style: 'cancel'},
            {text: yes, onPress: () => this.isLogin()},
          ],
          {cancelable: false},
        );
      }
    }
  }

  render() {
    // console.warn('pin details-->>',this.props.navigation.state.params.item)
    const token = this.state.token;
    const isRTL = this.props.isRTL;
    const pinId = this.props.navigation.state.params.item.id;
    const SellerProps =
      this.props.navigation != undefined &&
      this.props.navigation.state != undefined &&
      this.props.navigation.state.params != undefined
        ? this.props.navigation.state.params
        : null;

    const {contact_visibility, contact_no, countryCode} =
      this.props.navigation.state.params.pinDetails ||
      this.props.navigation.state.params.item;
    const newContact = countryCode + contact_no;
    console.warn(
      'contact visiblity',
      this.props.navigation.state.params.pinDetails,
    );
    console.warn('contact visiblity1', this.props.navigation.state.params.item);
    return (
      <View style={styles.container}>
        {contact_visibility === 'yes' || contact_visibility === 'Yes' ? (
          <View
            style={{
              flex: 2.5,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{marginLeft: 2}}
              onPress={() => {
                this.setState({
                  call: true,
                  message: false,
                  share: false,
                  direction: false,
                });
                this.callHandler(newContact);
              }}>
              {this.state.call ? (
                <Icon name="phone-square" size={32} color="#1aa3ff" />
              ) : (
                <Icon name="phone-square" size={32} color="#1aa3ff" />
              )}
              <Text style={this.state.call ? styles.activeText : styles.text}>
                {isRTL ? 'اتصال' : 'Call'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
        <View
          style={{flex: 2.5, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            style={{justifyContent: 'center', alignItems: 'center'}}
            onPress={() => {
              if (token) {
                this.setState({
                  call: false,
                  message: true,
                  share: false,
                  direction: false,
                });
                this.setModalStatus(true);
              } else {
                this.userNotLoggedIn();
              }
            }}>
            {this.state.message ? (
              <Icon name="envelope" size={32} color="#1aa3ff" />
            ) : (
              <Icon name="envelope" size={32} color="#1aa3ff" />
            )}

            <Text style={this.state.message ? styles.activeText : styles.text}>
              {isRTL ? 'رسالة' : 'Message'}
            </Text>
          </TouchableOpacity>
        </View>

        <View
          style={{flex: 2.5, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            style={{justifyContent: 'center', alignItems: 'center'}}
            onPress={() => {
              this.setState({
                call: false,
                message: false,
                share: true,
                direction: false,
              });
              // this.props.toggleVisible()
              this.socialShare();
            }}>
            {this.state.share ? (
              <Icon name="share-alt-square" size={32} color="#1aa3ff" />
            ) : (
              <Icon name="share-alt-square" size={32} color="#1aa3ff" />
            )}

            <Text style={this.state.share ? styles.activeText : styles.text}>
              {isRTL ? 'مشاركة' : 'Share'}
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{flex: 2.5, justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            style={{justifyContent: 'center', alignItems: 'center'}}
            onPress={() => {
              this.setState({
                call: false,
                message: false,
                share: false,
                direction: true,
              });
              this.showDirection.bind(this)();
            }}>
            {this.state.direction ? (
              <Icon name="location-arrow" size={32} color="#1aa3ff" />
            ) : (
              <Icon name="location-arrow" size={32} color="#1aa3ff" />
            )}
            <Text
              style={this.state.direction ? styles.activeText : styles.text}>
              {isRTL ? 'اتجاهات' : 'Direction'}
            </Text>
          </TouchableOpacity>
        </View>
        <MessageModal
          modalVisible={this.state.modalVisible}
          setModalStatus={this.setModalStatus}
          SellerProps={SellerProps}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

export default connect(mapStateToProps)(Footer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 5,
    marginBottom: 5,
    height: 55,
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,

    backgroundColor: '#f5f5f5',
  },
  icon: {
    width: 20,
    height: 20,
  },
  text: {
    color: 'grey',
    marginTop: 0,
  },
  activeText: {
    color: '#37aaf2',
    marginTop: 1,
  },
});
