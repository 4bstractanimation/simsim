import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  AsyncStorage,
  TouchableOpacity,
  Alert,
  Platform,
} from 'react-native';
import Data from './data';
import HomePageHeader from './maps/HomePageHeader';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {strings, isRTL} from '../../locales/i18n';
import * as seachActions from '../actions/Search';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/Favourites';
import NoDataFound from './NoDataFound';
import PreLoader from './PreLoader';
import API_URL from '../configuration/configuration';
import Toast from 'react-native-simple-toast';

class Favourites extends Component {
  static navigationOptions = ({navigation}) => ({
    header: (
      <View style={{height: 60, borderColor: '#F1F6F7'}}>
        <HomePageHeader navigation={navigation} viewType="list" />
      </View>
    ),
  });
  constructor() {
    super();
    this.state = {
      isLoading: true,
      favourites: [],
      token: '',
      // responseMessage: '',
      userId: '',
    };
  }

  getAllFavourites(token) {
    fetch(API_URL + '/favorite', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn('fav-->>',responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          this.setState({favourites: [], isLoading: false});
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          this.setState({
            favourites: responseData.FavPinList,
            isLoading: false,
          });
        }
      })
      .catch(err => {
        console.warn(err);
      });
  }

  async componentDidMount() {
    this.props.setSelectedcategory({cat: '', id: ''}); // reset selected categories
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    this.setState({token: token, userId: userData.userId});
    this.getAllFavourites(token);
  }

  remove = (id, uid) => {
    const isRTL = this.props.isRTL;
    const heading = isRTL ? 'إزالة من المفضلة' : 'Remove Favourites';
    const message = isRTL
      ? 'هل تريد إزالة نقطة البيع هذه من قائمة المفضلات؟'
      : 'Do you want to remove this Sales Point from favourites?';
    const yesButtonText = isRTL ? 'نعم' : 'Yes';
    const noButtonText = isRTL ? 'لا' : 'No';
    Alert.alert(heading, message, [
      {
        text: yesButtonText,
        onPress: () => {
          this.setState({isLoading: true});
          fetch(API_URL + '/remove-favorite', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: this.state.token,
            },
            body: JSON.stringify({
              pinId: id,
              userId: uid,
            }),
          })
            .then(response => response.json())
            .then(async responseData => {
              // console.warn(responseData);

              if (
                responseData.status == 654864 &&
                responseData.eData.eCode == 545864
              ) {
                this.getAllFavourites(this.state.token);
                // Alert.alert('Failed to Remove favorite');
                const message = isRTL
                  ? 'فشل في الإزالة من المفضلة'
                  : 'Failed to Remove favorite';
                Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
              } else if (
                responseData.status == 516324 &&
                responseData.eData === null
              ) {
                const message = isRTL
                  ? 'تم الازالة من المفضلة بنجاح'
                  : 'You have removed successfully';
                Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
                this.getAllFavourites(this.state.token);
              }
            })
            .catch(err => {
              Alert.alert('Error', err.message);
            });
        },
      },
      {
        text: noButtonText,
        onPress: () => {
          this.props.navigation.navigate('Favourites');
        },
      },
    ]);
  };

  render() {
    // console.warn('fav-->>',this.state.favourites)
    const styles = StyleSheetFactory.getSheet(this.props.isRTL);
    const {state, navigate} = this.props.navigation;
    return (
      <View
        style={[
          styles.container,
          Platform.OS === 'ios' ? {marginTop: 10} : null,
        ]}>
        <ScrollView>
          {this.state.isLoading ? (
            <PreLoader />
          ) : this.state.favourites.length ? (
            this.state.favourites.map((item, i) => {
              item.isfavourite = true;

              return (
                <TouchableOpacity
                  style={styles._messageBox}
                  key={i}
                  onPress={() => navigate('SellerContainer', {item})}>
                  <View
                    style={{flex: 1.1, right: this.props.isRTL ? 25 : null}}>
                    <Image
                      style={styles._image}
                      source={{uri: item.pin_image}}
                    />
                  </View>
                  <View style={styles._textBox}>
                    <View
                      style={{
                        flexDirection: this.props.isRTL ? 'row-reverse' : 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={styles._title}>{item.pin_name}</Text>

                      <TouchableOpacity
                        onPress={() => this.remove(item.id, this.state.userId)}>
                        <Image
                          style={[
                            styles.icons,
                            {marginHorizontal: 2, width: 30, height: 30},
                          ]}
                          source={require('./../icons/delete2x.png')}
                        />
                      </TouchableOpacity>
                    </View>

                    <View style={styles._descriptionContainer}>
                      <Text style={styles._description}>
                        {item.description}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            })
          ) : (
            <NoDataFound
              message={
                this.props.isRTL
                  ? 'لا يوجد نقاط بيع مفضله'
                  : 'No favourites found!'
              }
            />
          )}
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...layoutActions, ...seachActions}, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favourites);
