import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  Image,
  Linking,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import moment from 'moment';
import MultiSelect from 'react-native-multiple-select';
import Modal from 'react-native-simple-modal';
import StarRating from 'react-native-star-rating';
import {connect} from 'react-redux';
import Toast from 'react-native-simple-toast';
import API_URL from '../configuration/configuration';

class ChatMessages extends Component {
  static navigationOptions = ({navigation}) => {
    let {state} = navigation;

    return {
      title: state.params.ob.userName ? state.params.ob.userName : 'No Name',
    };

    // if (state.params !== undefined) {
    // 	return {
    //     title: (state.params.isRTL === true) ? 'رسالة' : 'Messages',
    //   }
    // }
  };

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      userId: '',
      token: '',
      text: '',
      pins: [],
      selectedPin: [],
      modalOpen: false,
      starCount: 0,
      ratingDetail: '',
      isBoxClicked: false,
    };
  }

  async getAllMessage(token) {
    const {ob} = this.props.navigation.state.params;
    // const fromUserId = arr.from_user_id;
    const toUserId = ob.userId;
    const {isRTL} = this.props;
    // const getUserData = await AsyncStorage.getItem('user_data');
    // const userData = JSON.parse(getUserData);
    // this.userDetail = userData
    // const token = userData.sData;
    fetch(API_URL + '/threadMessages', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        to_user_id: toUserId,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        // console.warn(responseData)
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'كود التوثيق المدخل خطأ'
            : 'Failed to get messages';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          // console.warn(responseData.Allmessage)
          this.setState({messages: responseData.Allmessage.reverse()});
        }
      })
      .catch(err => {
        Alert.alert('Error', err.messages);
      });
  }

  async componentDidMount() {
    AsyncStorage.getItem('user_data').then(json => {
      const userData = JSON.parse(json);
      if (userData.sData) {
        this.setState({token: userData.sData, userId: userData.userId});
        this.getAllMessage(userData.sData);
      }
      // this.getUserAllPin();
    });
  }

  // getUserAllPin() {
  // 	fetch('http://www.simtest.n1.iworklab.com/public/api/usersAllPin', {
  // 		method: 'GET',
  // 		headers: {
  // 			'Content-Type': 'application/json',
  // 			Authorization: this.state.token
  // 		}
  // 	})
  // 		.then(response => response.json())
  // 		.then(responseData => {
  // 			// console.warn('user all pins-->>',responseData)
  // 			if (
  // 				responseData.status == 654864 &&
  // 				responseData.eData.eCode == 545864
  // 			) {
  // 				alert('Failed to get pin');
  // 			} else if (
  // 				responseData.status == 516324 &&
  // 				responseData.eData === null
  // 			) {
  // 				const items = [];
  // 				responseData.allPins.map(item => {
  // 					const data = (({ id, pin_name }) => ({ id, pin_name }))(
  // 						item
  // 					);
  // 					items.push(data);
  // 				});
  // 				if (items.length) {
  // 					this.setState({ pins: items });
  // 				}
  // 			} else if (
  // 				responseData.status == 654864 &&
  // 				responseData.eData === null &&
  // 				responseData.sData === null
  // 			) {
  // 				Alert.alert('you dont have Pins');
  // 			}
  // 		})
  // 		.catch(err => {
  // 			console.warn(err);
  // 		});
  // }

  sendMessage = () => {
    // console.warn('-->>',this.props.navigation.state.params.arr)
    const token = this.state.token;
    const {ob} = this.props.navigation.state.params;
    const toUserId = ob.userId;
    const {isRTL} = this.props;
    // console.warn('chat--->',this.props.navigation.state.params.arr)
    fetch(API_URL + '/sendmessage', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        recipent: toUserId,
        message: this.state.text,
      }),
    })
      .then(response => response.json())
      .then(responseData => {
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'كود التوثيق المدخل خطأ'
            : 'Both fields are required';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (responseData.status == 516324) {
          this.getAllMessage(token);
          this.setState({text: ''});
        }
      })
      .catch(err => {
        Alert.alert('Error', err.messages);
      });
  };

  onSelectedItemsChange = selectedPin => {
    this.setState({selectedPin});
  };

  // sendRatingRequest = () => {

  // 	if(this.state.selectedPin.length) {
  // 		const pin = this.state.pins.filter(pin => pin.id == this.state.selectedPin);
  // 		const pinId = pin[0].id;
  // 		const pinName = pin[0].pin_name;
  // 		const toUserId = this.props.navigation.state.params.arr.to_user_id;
  // 		// console.warn(pinId,pinName,toUserId);
  // 		fetch('http://www.simtest.n1.iworklab.com/public/api/sendmessage', {
  // 		method: 'POST',
  // 		headers: {
  // 			'Content-Type': 'application/json',
  // 			'Authorization': this.state.token
  // 		},
  // 		body: JSON.stringify({
  // 			recipent: toUserId,
  // 			message: 'Rate the seller: ' + pinName,
  // 			pinId: pinId
  //    		 })
  // 		}).then((response) => response.json())
  // 		  .then((responseData) => {
  // 		  	if(responseData.status == 654864 && responseData.eData.eCode == 545864){
  // 		  		Alert.alert('Both fields are required');
  // 		  	} else if(responseData.status == 516324){
  // 		  		Alert.alert('','Rating request has been sent!')
  // 		  	}
  // 		  }).catch((err) => {
  // 		   console.warn(err);
  // 		});
  // 	this.getAllMessage();
  // 	this.setState({selectedPin:[]})
  // 	} else {
  // 		Alert.alert('','Please select the PIN')
  // 	}

  // }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  openModal(item) {
    // console.warn(item)
    const ratingDetail = (({pin_id, message}) => ({pin_id, message}))(item);
    this.setState({modalOpen: true, ratingDetail});
  }

  closeModal = () =>
    this.setState({modalOpen: false, ratingDetail: '', starCount: 0});

  shareCurrentLocation = () => {
    const token = this.state.token;
    const {isRTL} = this.props;
    const toUserId = this.props.navigation.state.params.ob.userId;
    // console.warn("TCL: shareCurrentLocation -> this.props.navigation", this.props.navigation.state.params.ob)
    const message = isRTL
      ? 'هل تود مشاركة موقع الحالي'
      : 'Do you want to share your current location';
    Alert.alert(
      '',
      message,
      [
        {
          text: 'Yes',
          onPress: () => {
            Geolocation.getCurrentPosition(
              position => {
                const lat = position.coords.latitude;
                const lng = position.coords.longitude;

                fetch(API_URL + '/sendmessage', {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/json',
                    Authorization: token,
                  },
                  body: JSON.stringify({
                    recipent: toUserId,
                    message: 'Share location data send!',
                    lat,
                    lng,
                  }),
                })
                  .then(response => response.json())
                  .then(responseData => {
                    console.warn(responseData);
                    if (
                      responseData.status == 654864 &&
                      responseData.eData.eCode == 545864
                    ) {
                      Alert.alert('Error: Not found');
                    } else if (responseData.status == 516324) {
                      this.getAllMessage();

                      const message = isRTL
                        ? 'كود التوثيق المدخل خطأ'
                        : 'Location shared successfully!';
                      Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
                    }
                  })
                  .catch(err => {
                    //console.warn(err);
                  });
              },
              error => {
                // const errorText = isRTL ? 'خطأ' : "Error";
                // const message = isRTL : "Check your location!" : "Check your location!";
                Toast.showWithGravity(error.message, Toast.SHORT, Toast.CENTER);
              },
              {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
            );
          },
        },
        {text: 'No', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };
  openLocationOnMap(lat, lng) {
    let openMap = Platform.select({
      ios: () => {
        Linking.openURL('http://maps.apple.com/maps?daddr=' + lat + ',' + lng);
      },
      android: () => {
        Linking.openURL(
          'http://maps.google.com/maps?daddr=' + lat + ',' + lng,
        ).catch(err => console.error('An error occurred', err));
      },
    });

    openMap();
  }

  clickEnableHandler = () => {
    this.setState({isBoxClicked: true});
  };

  clickDisableHandle = () => {
    this.setState({isBoxClicked: false});
  };

  render() {
    // console.warn('isRTL-->>',this.props.isRTL);
    const isRTL = this.props.isRTL;
    // console.warn('selectedPin-->>',this.state.selectedPin)
    const messages = this.state.messages;
    const userId = this.state.userId;
    const items = messages.map((item, index) => {
      // console.warn('lat-->>',item.lat,'lng-->>', item.lng)
      const dateTime = moment(item.created_at).format('lll');
      if (item.pin_id) {
        return (
          <TouchableOpacity
            key={index}
            onPress={
              this.state.userId !== item.from_user_id
                ? this.openModal.bind(this, item)
                : null
            }>
            <View
              style={
                item.from_user_id == userId
                  ? [styles.messageBox, styles.ownmessage]
                  : [
                      styles.messageBox,
                      {marginRight: 50, backgroundColor: '#71b4f7'},
                    ]
              }>
              <Text style={{color: '#FFF'}}>{item.message}</Text>
              <Text style={{fontSize: 10, marginTop: 10}}>{dateTime}</Text>
            </View>
          </TouchableOpacity>
        );
      } else if (item.lat && item.lng) {
        return (
          <View
            key={index}
            style={
              item.from_user_id == userId
                ? [styles.ownmessage, {backgroundColor: '#FFFFFF'}]
                : {marginRight: 50}
            }>
            <TouchableOpacity
              onPress={() => this.openLocationOnMap(item.lat, item.lng)}>
              <Image
                style={{
                  marginRight: 10,
                  // marginVertical: 8,
                  height: 100,
                  width: 100,
                  resizeMode: 'contain',
                  borderRadius: 5,
                }}
                source={require('./../icons/map-location.jpeg')}
              />
            </TouchableOpacity>
          </View>
        );
      } else {
        return (
          <TouchableOpacity key={index}>
            <View
              style={[
                styles.messageBox,
                item.from_user_id == userId
                  ? styles.ownmessage
                  : {marginRight: 50},
              ]}>
              <Text style={{color: '#FFF'}}>{item.message}</Text>
              <Text style={{fontSize: 10, marginTop: 10}}>{dateTime}</Text>
            </View>
          </TouchableOpacity>
        );
      }
    });
    return Platform.OS === 'ios' ? (
      <KeyboardAvoidingView
        style={[
          styles.container,
          {marginBottom: this.state.isBoxClicked ? 60 : 0},
        ]}
        behavior="padding">
        {this.state.pins.length ? (
          <View style={styles.sendRequest}>
            <MultiSelect
              single={true}
              items={this.state.pins}
              uniqueKey="id"
              ref={component => {
                this.multiSelect = component;
              }}
              onSelectedItemsChange={this.onSelectedItemsChange}
              selectedItems={this.state.selectedPin}
              displayKey="pin_name"
              selectText={isRTL ? 'حدد دبوس' : 'Select Pin'}
              searchInputPlaceholderText="Pick Your Pin"
              fontSize={15}
              tagRemoveIconColor="#00ADDE"
              tagBorderColor="#00ADDE"
              tagTextColor="#00ADDE"
              selectedItemTextColor="#CCC"
              selectedItemIconColor="#CCC"
              itemTextColor="#000"
              fixedHeight={true}
              autoFocusInput={true}
              hideSubmitButton={true}
            />

            <TouchableOpacity
              style={styles.requestButton}
              onPress={this.sendRatingRequest}>
              <Text style={styles.requestButtonText}>
                {isRTL ? 'إرسال طلب التقييم' : 'Send Rating Request'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}

        <ScrollView
          style={{marginTop: 10}}
          ref={ref => (this.scrollView = ref)}
          onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({animated: false});
          }}>
          {items.length ? (
            items
          ) : (
            <ActivityIndicator size="large" color="#40b2ef" />
          )}
        </ScrollView>

        <View
          style={{
            flexDirection: isRTL ? 'row-reverse' : 'row',
            marginVertical: 10,
            borderRadius: 25,
          }}>
          <View
            style={[
              styles.inputBox,
              {flexDirection: 'row', alignItems: 'center'},
            ]}>
            <TextInput
              style={{
                flex: 1,
                padding: 10,
                textAlign: isRTL ? 'right' : 'left',
              }}
              underlineColorAndroid="rgba(0,0,0,0)"
              //multiline={false}
              returnKeyType="done"
              onFocus={this.clickEnableHandler}
              onEndEditing={this.clickDisableHandle}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
              placeholder={isRTL ? 'اكتب الرسالة هنا' : 'Type message here'}
            />
            <TouchableOpacity onPress={this.shareCurrentLocation}>
              <Image
                style={{
                  marginRight: 10,
                  height: 25,
                  width: 25,
                  resizeMode: 'contain',
                }}
                source={require('./../icons/location.png')}
              />
            </TouchableOpacity>
          </View>

          <TouchableOpacity onPress={this.sendMessage}>
            <View
              style={{
                borderRadius: 25,
                backgroundColor: '#40b2ef',
                padding: 15,
              }}>
              <Text style={styles.btnText}>{isRTL ? 'إرسال' : 'Send'}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    ) : (
      <View style={styles.container}>
        {this.state.pins.length ? (
          <View style={styles.sendRequest}>
            <MultiSelect
              single={true}
              items={this.state.pins}
              uniqueKey="id"
              ref={component => {
                this.multiSelect = component;
              }}
              onSelectedItemsChange={this.onSelectedItemsChange}
              selectedItems={this.state.selectedPin}
              displayKey="pin_name"
              selectText={isRTL ? 'حدد دبوس' : 'Select Pin'}
              searchInputPlaceholderText="Pick Your Pin"
              fontSize={15}
              tagRemoveIconColor="#00ADDE"
              tagBorderColor="#00ADDE"
              tagTextColor="#00ADDE"
              selectedItemTextColor="#CCC"
              selectedItemIconColor="#CCC"
              itemTextColor="#000"
              fixedHeight={true}
              autoFocusInput={true}
              hideSubmitButton={true}
            />

            <TouchableOpacity
              style={styles.requestButton}
              onPress={this.sendRatingRequest}>
              <Text style={styles.requestButtonText}>
                {isRTL ? 'إرسال طلب التقييم' : 'Send Rating Request'}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
        <ScrollView
          style={{marginTop: 10}}
          ref={ref => (this.scrollView = ref)}
          onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({animated: false});
          }}>
          {items.length ? (
            items
          ) : (
            <ActivityIndicator size="large" color="#40b2ef" />
          )}
        </ScrollView>

        <View
          style={{
            flexDirection: isRTL ? 'row-reverse' : 'row',
            marginVertical: 10,
          }}>
          <View
            style={[
              styles.inputBox,
              {flexDirection: 'row', alignItems: 'center'},
            ]}>
            <TextInput
              style={{
                flex: 1,
                padding: 10,
                textAlign: isRTL ? 'right' : 'left',
              }}
              underlineColorAndroid="rgba(0,0,0,0)"
              multiline={true}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
              placeholder={isRTL ? 'اكتب الرسالة هنا' : 'Type message here'}
            />
            <TouchableOpacity onPress={this.shareCurrentLocation}>
              <Image
                style={{
                  marginRight: 10,
                  // marginVertical: 8,
                  height: 25,
                  width: 25,
                  resizeMode: 'contain',
                }}
                source={require('./../icons/location.png')}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={this.sendMessage}>
            <Text style={styles.btnText}>{isRTL ? 'إرسال' : 'Send'}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //flexDirection: isRTL ? 'row-reverse' : 'row',
    paddingTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,

    paddingBottom: Platform.OS === 'ios' ? 60 : 0,
    backgroundColor: '#FFFFFF',
  },
  messageBox: {
    marginBottom: 10,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: '#ba8c18',
    borderRadius: 10,
  },
  ownmessage: {
    alignItems: 'flex-end',
    marginLeft: 50,
    backgroundColor: '#a4a6a8',
  },
  inputBox: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 25,
    borderColor: '#40b2ef',
  },
  btnText: {
    borderRadius: Platform.OS === 'ios' ? 0 : 25,
    backgroundColor: '#40b2ef',
    padding: Platform.OS === 'ios' ? 0 : 15,
  },
  sendRequest: {
    // flexDirection: 'row'
    // marginHorizontal: 15
  },
  requestButton: {
    borderRadius: 20,
    backgroundColor: 'green',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  requestButtonText: {
    color: '#FFFFFF',
  },
  contentContainerStyle: {
    // flexDirection: isRTL ? 'row-reverse' : 'row',
    // paddingTop: 10,
    // marginLeft: 10,
    // marginRight: 10,
    // marginTop: 10,
    // backgroundColor: '#FFFFFF',
    // bottom:10,
  },
});

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

export default connect(mapStateToProps)(ChatMessages);
