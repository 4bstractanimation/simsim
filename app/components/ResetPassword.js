import React, {Component} from 'react';
import { 
	Text, 
	Input,
	Image, 
	View, 
	ScrollView, 
	TextInput, 
	Alert, 
	StyleSheet,
	KeyboardAvoidingView, 
	Dimensions,
	TouchableOpacity,
	Keyboard
} from 'react-native';

import Home from './FloatingButton';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { strings, isRTL } from '../../locales/i18n';
import * as layoutActions from '../actions/layout';
import StyleSheetFactory from './../styles/ResetPassword';
import API_URL from '../configuration/configuration';
import Toast from 'react-native-simple-toast';

class ResetPassword extends Component {

 static navigationOptions = ({ navigation }) => {
	let {state} = navigation;
	if (state.params !== undefined) {
		return {
		 	title: (state.params.isRTL === true) ? 'إعادة ضبط كلمة المرور' : 'Reset Password'
		 }}
	}
 
	constructor(props){
	super(props);
	this.state={
	secureTextEntry: true,
	password:'',
	confirmPassword:''
	}
	this.handleSubmit= this.handleSubmit.bind(this);
	}

	componentDidMount() {
		const {setParams} = this.props.navigation;
	 	setParams({isRTL: this.props.isRTL});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isRTL != this.props.isRTL) {
		 this.props.navigation.setParams({ isRTL: nextProps.isRTL });
		 this.props.navigation.state.params.isRTL = nextProps.isRTL
		}
	}
	
	handleSubmit(){
		// console.warn(this.props.navigation.state.params)
		Keyboard.dismiss();
		const isRTL = this.props.isRTL;
		const { navigate } = this.props.navigation;
		if(this.state.password === '') {
			
			const message = isRTL ? 'ادخل كلمة المرور الجديدة' : "Enter new password";
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}
		if(this.state.confirmPassword === '') {
			const message = isRTL ? 'تأكيد كلمة المرور' : "Enter confirm password";
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			return;
		}

		if(this.state.password === this.state.confirmPassword) {
			fetch(API_URL+'/set-new-password-mobile',{
			method: 'POST',
			headers: {
		 'Accept': 'application/json',
			'Content-Type': 'application/json'
			},
			body: JSON.stringify({
			 password:this.state.password, 
			 password_confirmation:this.state.confirmPassword,
			 otpvl:this.props.navigation.state.params.otp,
			 mobile:this.props.navigation.state.params.mob,
			 countryCode: this.props.navigation.state.params.countryCode
		 })
			})
			.then((response) => response.json())
			.then((responseData) => { 
				// console.warn('set-new-password-->>',responseData);
			if(responseData.status == 502368 && responseData.eData.eCode == 684605){
			Alert.alert('Error',responseData.eData.errors.join(','));
			}else if(responseData.status == 502368 && responseData.eData.eCode == 545486){
			const message = isRTL ? 'كود التوثيق المدخل خطأ' : 'You have entered the wrong OTP'
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			}else if(responseData.status == 805465 && responseData.eData === null) {
				const message = isRTL ? 'تم تغيير كلمة المرور بنجاح' : 'Successfully changed password';
				const continueText = isRTL ? 'متابعة' : 'Continue';
				const loginText = isRTL ? 'تسجيل دخول' : 'Login';
				Alert.alert('', message, 
					[{text:continueText, onPress: () => navigate('Home')},
					{text: loginText, onPress: () => navigate('Login')}]
				);
				this.setState({password:'', confirmPassword: ''})
			}
			})
			.catch((err) => { 
				const errorText = isRTL ? 'خطأ' : "Error";
				Alert.alert(errorText, err.message);
			});
		} else {
			const message = isRTL ? 'يجب ان تتطابق كلمة المرور' : 'Password should be match';
			Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
			
		}
	}

	showPass() {
	 if(this.state.secureTextEntry){
	 this.setState({secureTextEntry:false})
	 } else {
	 this.setState({secureTextEntry:true})
	 }
 }

	render(){
		const styles = StyleSheetFactory.getSheet(this.props.isRTL);
		return(
		<View style={ styles.container }>
		<ScrollView style={{padding: 20, backgroundColor: '#F1F6F7'}} keyboardShouldPersistTaps="always">
		<View style={{alignItems:'center'}}>
		<Image style={ styles.logo } source={require('../icons/logo1x.png')}/>
		</View>
		<View style={[styles._textbox, {flexDirection: this.props.isRTL ? 'row-reverse' : 'row', justifyContent:'space-between'}]}>
		<View style={ [styles._textbox, {width:'80%', marginTop:0}] }>
		<TextInput 
			style={ styles._textboxText } 
			onChangeText={(text) => this.setState({password: text})}
			underlineColorAndroid='white' 
			keyboardType={'default'} 
			placeholderTextColor="#545454" 
			placeholder= {strings('reset.new')}
			secureTextEntry={true}
		/>
		</View>
		{
			// <TouchableOpacity onPress={this.showPass.bind(this)}>
			// <Image style={{width:35, 
			// height:27, 
			// marginVertical:15, 
			// marginHorizontal:5,}} 
			// source={require('./../icons/show2x.png')}/>
			// </TouchableOpacity>
		}
		</View>
		
		<View style={[styles._textbox, {flexDirection: this.props.isRTL ? 'row-reverse' : 'row', justifyContent:'space-between'}]}>
			<View style={ [styles._textbox, {width:'80%', marginTop:0}] }>
			<TextInput 
				style={ styles._textboxText } 
				onChangeText={(text) => this.setState({confirmPassword: text})}
				underlineColorAndroid='white' 
				secureTextEntry={true} 
				keyboardType={'default'} 
				placeholderTextColor="#545454" 
				placeholder= {strings('reset.confirm')}
			/>
			</View>
		</View>
		
			<TouchableOpacity 
				style={styles._button} 
				onPress={this.handleSubmit} 
			>
				<Text style={styles._buttonText}>{strings('adNewAd.submit')}</Text>
			</TouchableOpacity>
		</ScrollView>
		</View>
		)
	}
}

function mapStateToProps(state) {
 return {
 isRTL: state.network.isRTL,
 }
}

function mapDispatchToProps(dispatch) {
 return bindActionCreators(layoutActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword)