import React, {Component} from 'react';
import {
  Modal,
  TextInput,
  View,
  StyleSheet,
  Dimensions,
  Keyboard,
  TouchableOpacity,
  Text,
  AsyncStorage,
  Alert,
} from 'react-native';
const {width, height} = Dimensions.get('window');
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';

import API_URL from '../configuration/configuration';
import database from '@react-native-firebase/database';

class MessageModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      textLength: 100,
      description: '',
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modalVisible != this.state.modalVisible) {
      this.setState({modalVisible: nextProps.modalVisible});
    }
  }

  handleChange(text) {
    this.setState({description: text});
  }

  async sendMessage() {
    const {isRTL} = this.props;
    const getUserData = await AsyncStorage.getItem('user_data');
    const userData = JSON.parse(getUserData);
    const token = userData.sData;
    // console.warn(token)
    const data = {
      recipent: this.props.SellerProps.item.user_id,
      message: this.state.description,
      pinId: this.props.SellerProps.item.id,
    };

    // console.warn(data);

    fetch(API_URL + '/sendmessage', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify(data),
    })
      .then(response => response.json())
      .then(responseData => {
        //console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          const message = isRTL
            ? 'كلا الحقلين مطلوبان'
            : 'Both fields are required';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
        } else if (responseData.status == 516324) {
          const message = isRTL
            ? 'تم ارسال الرسالة بنجاح '
            : 'Message sent successfully';
          Toast.showWithGravity(message, Toast.SHORT, Toast.CENTER);
          this.setState({description: ''});

          database()
            .ref(
              'SHOPS/SHOP/' +
                this.props.SellerProps.item.pin_name +
                '/messages',
            )
            .transaction(total => {
              return (total || 0) + 1;
            });

          // DATE UPDATE

          database()
            .ref(
              'SHOPS/SHOP/' +
                this.props.SellerProps.item.pin_name +
                '/date/' +
                new Date().toDateString() +
                '/messages',
            )
            .transaction(total => {
              return (total || 0) + 1;
            });
          //Daily update
          database()
            .ref('D_SHOPS/D_SHOP/' + new Date().toDateString() + '/messages')
            .transaction(total => {
              return (total || 0) + 1;
            });
        }
      })
      .catch(err => {
        const errorText = isRTL ? 'خطأ' : 'Error';
        Alert.alert(errorText, err.message);
      });
  }

  render() {
    // console.warn(this.props.isRTL)
    return (
      <View style={{marginBottom: 55}}>
        <Modal
          animationType={'slide'}
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.props.setModalStatus(false);
          }}>
          <View style={{flex: 1, backgroundColor: 'white', paddingBottom: 0}}>
            <View style={styles._descriptionbox}>
              <TextInput
                maxLength={100}
                style={[
                  styles.adnum,
                  {textAlign: this.props.isRTL ? 'right' : 'left', right: 5},
                ]}
                onChangeText={text => this.handleChange(text)}
                multiline={true}
                underlineColorAndroid="white"
                textAlignVertical="top"
                numberOfLines={7}
                keyboardType={'default'}
                placeholderTextColor="#7A7A7A"
                value={this.state.description}
                placeholder={this.props.isRTL ? 'اكتب رسالة' : 'Write Message'}
                blurOnSubmit={true}
                onSubmitEditing={() => {
                  Keyboard.dismiss();
                }}
                returnKeyType="done"
              />
              <View
                style={{
                  justifyContent: 'flex-end',
                  marginRight: 10,
                  marginBottom: 10,
                }}>
                <Text
                  style={{
                    textAlign: this.props.isRTL ? 'left' : 'right',
                    marginRight: 10,
                    paddingTop: 11,
                    justifyContent: 'flex-end',
                    left: 5,
                  }}>
                  {this.state.description.length}/100
                </Text>
              </View>
            </View>
            <View style={styles.buttonBox}>
              <TouchableOpacity
                style={styles._button}
                onPress={() => this.sendMessage()}>
                <Text
                  style={[
                    styles._buttonText,
                    {textAlign: this.props.isRTL ? 'right' : 'left'},
                  ]}>
                  {this.props.isRTL ? 'إرسال' : 'Send'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.setModalStatus(false)}
                style={styles._button}>
                <Text
                  style={[
                    styles._buttonText,
                    {textAlign: this.props.isRTL ? 'right' : 'left'},
                  ]}>
                  {this.props.isRTL ? 'إلغاء' : 'Cancel'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    isRTL: state.network.isRTL,
  };
}

export default connect(mapStateToProps)(MessageModal);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    marginHorizontal: 10,
    marginBottom: 5,
    height: 55,
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    backgroundColor: 'white',
    flex: 1,
  },
  icon: {
    width: 25,
    height: 25,
  },
  text: {
    color: 'grey',
    marginTop: 5,
    paddingRight: 10,
  },
  _descriptionbox: {
    flexDirection: 'column',
    borderRadius: 25,
    backgroundColor: 'white',
    marginTop: 40,
    height: 200,
    marginHorizontal: 10,
    borderColor: 'black',
    borderWidth: 2,
  },
  adnum: {
    flex: 1,
    fontSize: 15,
    marginLeft: 10,
    marginRight: 0,
    paddingTop: 15,
    fontWeight: 'bold',
    color: '#7A7A7A',
    //	textAlign: isRTL ? 'right' : 'left',
  },
  _button: {
    alignItems: 'center',
    backgroundColor: '#737477',
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 30,
    marginRight: 7,
    marginLeft: 7,
  },
  _buttonText: {
    color: '#ffffff',
    fontWeight: 'bold',
  },
  buttonBox: {
    margin: 10,
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
