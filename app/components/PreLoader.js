import React, { Component } from 'react';
import { Text, StyleSheet, ActivityIndicator } from 'react-native';

export default class PreLoader extends Component {

	render() {
		return (
				<ActivityIndicator 
					size="large" 
					color="#40b2ef" 
					style={{marginTop:20}}
				/>
			)
	}
}
