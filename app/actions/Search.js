export const REQUEST_DATA = 'REQUEST_DATA';
export const SUCCESS_DATA = 'SUCCESS_DATA';
export const ERROR_DATA = 'ERROR_DATA';
export const SET_LOCATION = 'SET_LOCATION';
export const SET_TOAST = 'SET_TOAST';
export const SET_MODAL = 'SET_MODAL';
export const SET_OFFER = 'SET_OFFER';
export const SET_SELECTED_ID = 'SET_SELECTED_ID';
import API_URL from '../configuration/configuration';
import {Alert} from 'react-native';

function requestData(payload) {
  // console.warn('== in requestData ==')
  return {
    type: REQUEST_DATA,
    loader: payload,
  };
}

function successData(payload, status) {
  return {
    type: SUCCESS_DATA,
    loader: false,
    filterData: payload,
    status: status,
  };
}

function errorData(payload) {
  // console.warn('==== in error ====')
  return {
    type: ERROR_DATA,
    loader: payload,
  };
}

export function setLocation(payload) {
  // console.warn('==== in setLocation ====')
  return {
    type: 'SET_LOCATION',
    location: payload,
  };
}

export function setToast(payload) {
  return {
    type: 'SET_TOAST',
    toast: payload,
  };
}

export function openModal(payload) {
  return {
    type: SET_MODAL,
    modalOpen: payload,
  };
}

export function offerData(payload) {
  return {
    type: SET_OFFER,
    response: payload,
  };
}

export function searchWithLocationAPI(
  userId,
  lat,
  lng,
  deviceId,
  keyword,
  cat = null,
  subcat = null,
  subSubCat = null,
  type = '',
  subscription_plan = '',
  delivery = '',
  rating = '',
  ad_status = '',
) {
  return function(dispatch) {
    dispatch(requestData(true));
    const searchData = {
      userId: userId,
      search: keyword,
      deviceId,
      lat: lat,
      lng: lng,
      cat: cat,
      subcat: subcat,
      subSubCat: subSubCat,
      delivery: delivery,
      ratingSort: rating,
      subscription_plan: subscription_plan,
      type: type,
      ad_status: ad_status,
    };
    console.warn(JSON.stringify(searchData));
    console.warn('search', searchData);
    fetch(API_URL + '/searchbar', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(searchData),
    })
      .then(response => response.json())
      .then(responseData => {
        console.warn('searchbar api responseData-->>', responseData);
        if (responseData.status == 516324 && responseData.eData === null) {
          responseData.ad_status = ad_status;
          dispatch(successData(responseData, 'searchData'));
        } else if (
          responseData.status == 654864 &&
          responseData.eData === null &&
          responseData.sData === null
        ) {
          dispatch(successData({targetPins: [], allPins: []}, 'searchData'));
        }
      })
      .catch(err => {
        console.warn(err.message);
        Alert.alert('Error', err.message);
        dispatch(errorData(false));
      });
  };
}

export function getAllPins() {
  return function(dispatch) {
    dispatch(requestData(true));
    fetch(API_URL + '/get-pins', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(async responseData => {
        // console.warn(responseData);
        if (
          responseData.status == 654864 &&
          responseData.eData.eCode == 545864
        ) {
          alert('Failed to get pin');
        } else if (
          responseData.status == 516324 &&
          responseData.eData === null
        ) {
          dispatch(successData(responseData, 'allPins'));
        }
      })
      .catch(err => {
        Alert.alert('Internet Connection is not Working/Available'),
          dispatch(errorData(false));
      });
  };
}

export function setSelectedcategory(selectedCat) {
  return {type: SET_SELECTED_ID, selectedCat: selectedCat}; // body...
}
