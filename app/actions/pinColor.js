export const SELECTED_MARKER = 'SELECTED_MARKER'

 export function pinActions(id) {
 	return {
 		type: SELECTED_MARKER,
 		payload: id,
 	}
 }

