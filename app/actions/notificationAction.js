export const NOTIFICATION_COUNT = 'NOTIFICATION_COUNT'
export const RESET_NOTIFICATION = "RESET_NOTIFICATION";
export const NOTIFICATION = 'NOTIFICATION';
import API_URL from '../configuration/configuration';
 
export function getNotificationCount(payload) {
	return {
		type: 'NOTIFICATION_COUNT',
		count: payload
	};
}
export function resetNotificationCount(payload) {
  return{
    type: 'RESET_NOTIFICATION'
  }
}

export function getData(payload) {
  
	return function(dispatch) {
      fetch(API_URL+'/get-notification-list',
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: payload
          }
        }
      )
        .then(response => response.json())
        .then(responseData => {
           if (
            responseData.status == 516324 &&
            responseData.eData === null
          ) {
            dispatch({
            	type: 'NOTIFICATION',
            	notificationCount: responseData.count
            }) 
          }
          if(responseData.status === 654864){
            dispatch({
              type: 'NOTIFICATION',
              notificationCount: 0
            }) 
          }
        })
        .catch(err => {
          // alert('Network Issue');
          console.warn(err);
        });
    }
}