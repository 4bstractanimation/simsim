import i18n from 'react-native-i18n';
import {
	Text,
	View,
	Alert,
	TouchableHighlight,
	StyleSheet,
	Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');

export default class StyleSheetFactory {
	static getSheet(isRTL) {
		isRTL ? (i18n.locale = 'ar') : (i18n.locale = 'en');
		return StyleSheet.create({
			container: {
				flex: 1,
				flexDirection: 'column'
			},
			map: {
				width: '100%',
				height: '60%'
			},
			content: {
				flex: 1,
				padding: 10,
				marginLeft: 10,
				marginRight: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF'
			},
			contentItem: {
				flex: 1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				marginBottom: 15
			},
			contentItemHeading: {
				fontSize: 18,
				fontWeight: 'bold',
				color: '#000000'
			},
			contentItemText: {
				color: '#000000',
				textAlign: isRTL ? 'right' : null
			},
			icons: {
				marginTop: 4,
				marginRight: 5
			},
			iconsText: {
				fontSize: 12
			},
			abuseButton: {
				alignItems: 'center',
				backgroundColor: '#737477',
				paddingVertical: 10,
				paddingHorizontal: 30,
				borderRadius: 30
			},
			counterContainer: {
				backgroundColor: 'black',
				width: 18,
				height: 18,
				marginTop: 5,
				marginHorizontal: 1
			},
			counterText: {
				color: 'white',
				fontSize: 12,
				textAlign: 'center'
			}
		});
	}
}
