import i18n from 'react-native-i18n';
import {
	Text,
	View,
	Alert,
	TouchableHighlight,
	StyleSheet,
	Dimensions,
	Platform,
} from 'react-native';
const { width, height } = Dimensions.get('window');

export default class StyleSheetFactory {
	static getSheet(isRTL) {
		isRTL ? (i18n.locale = 'ar') : (i18n.locale = 'en');
		return StyleSheet.create({
			container: {
				flex: 1,
				justifyContent: 'center',
				backgroundColor: '#ecf2f9',
				paddingHorizontal: 10
			},
			_title: {
				fontWeight: 'bold',
				fontSize: 20
			},

			_searchText: {
				flex: 1,
				borderBottomLeftRadius: 25,
				borderBottomRightRadius: 25,
				borderTopLeftRadius: 25,
				borderTopRightRadius: 25,
				width: width === 320 ? 180 : 230,
				height: 40,
				flex: 1,
				backgroundColor: 'white'
			},
			_buttonFilter: {
				borderBottomLeftRadius: 20,
				borderBottomRightRadius: 20,
				borderTopLeftRadius: 20,
				borderTopRightRadius: 20,
				width: width === 320 ? '35%' : '30%',
				height: 45,
				backgroundColor: 'white',
				alignItems: 'center',
				justifyContent: 'center',
				marginLeft: 10
			},
			_messageBox: {
				flex: 1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginTop: 2
			},
			_image: {
				width: 80,
				height: 80,
				resizeMode: 'contain'
			},
			_icon: {
				width: 20,
				height: 30,
				marginTop: 20
			},
			_iconContainer: {
				flex: 1,
				justifyContent: 'center',
				marginRight: 20
			},
			_textBox: {},
			_descriptionContainer: {
				flexDirection: isRTL ? 'row-reverse' : 'row',
				justifyContent: 'space-between'
			},
			_description: {
				fontSize: 15,
				color: '#000000',
				flexWrap: 'wrap',
				marginRight: 20
			},
			searchInput: {
				bottom : Platform.OS === 'ios' ? 5 : 0,
				fontWeight: 'bold',
				fontSize: 16,
				color: '#6A6A6A',
				marginLeft: isRTL ? 0 : width === 320 ? 70 : 20,
				marginRight: isRTL ? (width === 320 ? 70 : 90) : 0
			},
			clearButton: {
				marginLeft: 10,
				backgroundColor: '#fff',
				borderRadius: 25,
				padding: 10
			}
		});
	}
}
