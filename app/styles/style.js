import i18n from 'react-native-i18n';
import {Text, View, Alert, TouchableHighlight, StyleSheet} from 'react-native';
export default class StyleSheetFactory{  
    static getSheet(isRTL){
        isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
        return StyleSheet.create({
            container: {
                flex: 1,
                justifyContent: 'center',
                alignItems: isRTL ? 'flex-start': 'flex-end',
                backgroundColor: '#F5FCFF',
                paddingRight:isRTL ? 10:5,
                paddingLeft:isRTL ? 5:10,
            },
            welcome: {
                fontSize: 20,
                textAlign: isRTL ? 'right' : 'left',
                margin: 10,
            },
            instructions: {
                textAlign: isRTL ? 'right' : 'left',
                alignSelf: "center",
                marginBottom: 5,
            },
            langContainer:{
                alignItems:'flex-end'
            },
            select:{
                flexDirection: isRTL ? 'row-reverse':'row',
            }
        });
    }
}