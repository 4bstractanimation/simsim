import i18n from 'react-native-i18n';
import {Text, View, Alert, TouchableHighlight, StyleSheet, Platform} from 'react-native';
export default class StyleSheetFactory{  
    static getSheet(isRTL){
        isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
        return StyleSheet.create({
            container: {
                flex: 1,
                justifyContent: 'center',
                alignItems: isRTL ? 'flex-start': 'flex-end',
                backgroundColor: '#F5FCFF',
                paddingRight:isRTL ? 10:5,
                paddingLeft:isRTL ? 5:10,
            },
            logo: {
                width: '42.5%',
                height:111,
                resizeMode: 'contain'
            },
            welcome: {
                fontSize: 20,
                textAlign: isRTL ? 'right' : 'left',
                margin: 10,
            },
            instructions: {
                textAlign: isRTL ? 'right' : 'left',
                alignSelf: "center",
                marginBottom: 5,
            },
            langContainer:{
                alignItems:'flex-end'
            },
            select:{
                flexDirection: isRTL ? 'row-reverse':'row',
            },            
            _textbox:{
                borderBottomLeftRadius: 25,
                borderBottomRightRadius: 25,
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
                width: '100%',
                height: 50,
                backgroundColor:'white',
                marginTop:20,
            },
            _textboxText: {
                fontSize:17,
                marginLeft: isRTL ? 0 : 10,
                marginRight: isRTL ? 10 : 0,
                fontWeight: 'bold',
                color: "#545454",
                textAlign: isRTL ? 'right' : 'left',
                // borderColor: 'red',
                // borderWidth:2
                marginTop: Platform.OS == 'ios' ? 15 : null,
            },
            _switchBox:{
                margin:10
            },
            _radiotextbox:{
                borderBottomLeftRadius: 25,
                borderBottomRightRadius: 25,
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
                width: '100%',
                height: 50,
                backgroundColor:'white',
                marginTop:20,
                flexDirection: isRTL ? 'row-reverse' : 'row'
            },
            _button:{
                borderBottomLeftRadius: 25,
                borderBottomRightRadius: 25,
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
                width:'48%',
                height: 50,
                marginTop:20,
                alignItems:'center'
            },
            _UpdateButton:{
                backgroundColor:'#00ADDE',
                alignItems:'center',
            },
            _deleteButton:{
                backgroundColor:'#fff',
                alignItems:'center',
            },
            _buttonText: {
                fontSize:17,
                fontWeight: 'bold',
                marginTop:13,
                alignItems: 'center'
            },
            _updatebuttonText:{
                color: '#fff'
            },
            _deletebuttonText:{
                color: '#e60000'
            },
            _radioButton:{
                flexDirection: isRTL ? 'row-reverse' : 'row',
                //borderColor: 'red', borderWidth:2,
                // marginLeft: 5
            }
        });
    }
}