import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				justifyContent: 'center',
				marginHorizontal:0,
				backgroundColor:'#ecf2f9'
			},
			button: {
		    borderRadius:25,
		    backgroundColor: '#FFFFFF',
		    padding: 5,
		    height:50,
		    justifyContent:'center',
		    marginHorizontal:5,
		  },
		  counterContainer:{
		  	backgroundColor:'black',
		  	width:18,
		  	height:18,
		  	marginTop:5,
		  	marginHorizontal:1
		  },
		  counterText:{
		  	color:'white',
		  	fontSize:12,
		  	textAlign:'center'
		  },
		  pause:{
				color:'#00ADDE', 
				fontSize:15,
				fontWeight:'bold'
			},
		  button_text: {
		    color: '#000000',
		    marginHorizontal:10,
		    fontFamily: 'System',
		    fontSize: 16,
		    textAlign: 'center',
		  },
			_title:{
				fontSize:18,
				fontWeight:'bold',
				marginBottom:5,
			},
			_messageBox:{
				flex:1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginTop: 10,
				backgroundColor: '#FFFFFF',
			},
			_image:{
				width: 80,
			 	height:80,
			 	resizeMode: 'contain',
			},
			_date:{
				color:'#545454'
			},
			_icon:{
				width:20,
				height: 30,
				marginTop:20
			},
			_iconContainer:{
				flex:1,
				justifyContent: 'center',
				marginRight:20
			},
			_textBox:{
				flex: 2.5
			},
			_descriptionContainer: {
				flexDirection:isRTL ? 'row-reverse' : "row",
				justifyContent: "space-between",
			},
			_description:{
				textAlign: isRTL ? 'right' : null,
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
			},
    });
  }
}