import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
		    flex:1,
		    justifyContent:'center',
		    marginHorizontal:10,
		    backgroundColor:'#ecf2f9',
		    marginTop:50
		  }, 
			 adBox: {
			  backgroundColor: '#FFFFFF',
			  marginVertical:5, 
			  padding: 10,
			  alignItems:'center',
			  width:'100%',
			  flexDirection: isRTL ? 'row-reverse' : 'row',
			 },
			 _textBox:{
			   marginLeft: 10, 
			   flex:1
			 },
			 icon:{
				 width:60,
				 height:50,
			 },
			 description:{
			  fontSize:13,
			  color:'black',
			  flexWrap: 'wrap', 
			 },
			 title:{
			   fontSize:18,
			   color: "#000",
			   fontWeight: "bold",
			   paddingBottom: 5
			 },
			 button: {
			    borderRadius:25,
			    padding: 5,
			    height:25,
			    justifyContent:'center',
			    marginHorizontal:5,
			  },
			  button_text: {
			    color: '#ffff',
			    marginHorizontal:5,
			    fontSize: 12,
			    textAlign: 'center',
			  },
			  favourites:{
			    width:30, 
			    height:30, 
			    marginHorizontal:3, 
			    resizeMode:'contain'
			  },
			  bottomImage: {
			  position: 'absolute',
			  bottom: 90,
			  right: 30,
			  height: 50,
			},
    });
  }
}