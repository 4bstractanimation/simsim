import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				justifyContent: 'center',
			},
			txnText:{
				bottom:0,
				color: '#00ADDE',
				textAlign:'center',
				marginVertical:20,
				fontWeight:'bold',
				justifyContent:'flex-end'
			},
			_title:{
				fontSize:18,
				fontWeight: 'bold'
			},
			_messageBox:{
				flex:1,
				alignItems: 'center',
				padding: 10,				
				marginHorizontal:10,
				marginBottom:10,
				backgroundColor: '#FFFFFF',

			},
			_textBox:{
				flex: 1,
				width:"100%",
				marginTop:10,
			},
			_descriptionContainer:{
				flexDirection: isRTL ? 'row-reverse' : "row",
				 justifyContent: "space-between",
				marginVertical:10
			},
			_description:{
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
		 		marginRight: isRTL ? 0 : 20,
		 		textAlign: isRTL ? 'right' : null,
			},
			planButton: { 
				backgroundColor:'#FFFFFF',
				borderWidth:1, 
				borderColor: '#00ADDE',
				borderRadius:25,
				margin: 10,
				paddingVertical:5,
				paddingHorizontal: 10,
				width: '30%',
				alignItems: 'center',				
			}
    });
  }
}