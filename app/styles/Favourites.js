import i18n from 'react-native-i18n';
import { Text, View, Alert, Platform, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				justifyContent: 'center',
				marginHorizontal:10,
				backgroundColor:'#ecf2f9',
			},
			_title:{
				fontSize:17,
				fontWeight:'bold',
				marginBottom:5,
				textAlign : isRTL ? 'right' : null
			},
			button: {
		    borderRadius:20,
		    backgroundColor: '#00ADDE',
		    padding: 5,
		    height:25,
		    justifyContent:'center',
		    marginHorizontal:5,
		  },
		  button_text: {
		    color: '#fff',
		    marginHorizontal:5,
		    fontFamily: 'System',
		    fontSize: 15,
		    textAlign: 'center',
		    bottom : Platform.OS === 'ios' ? 2 : 0
		  },
			_messageBox:{
				flex:1,
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginTop: 5,
				backgroundColor: '#FFFFFF',
			},
			_image:{
				width: 80,
			 	height:80,
			 	resizeMode: 'contain',
			},
			_textBox:{
				flex: 2.5
			},
			_descriptionContainer:{
				flexDirection:isRTL ? 'row-reverse' : "row",
				flexWrap: 'wrap',
				justifyContent: "space-between",
			},
			_description:{
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
				 marginRight: isRTL ? null : 20,
				 textAlign : isRTL ? 'right' : null
			},
    });
  }
}