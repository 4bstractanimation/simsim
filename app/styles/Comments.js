import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
	  	container:{
				flex:1,
				alignItems: 'center',
			},
			comment:{
				flex:1 ,
				flexDirection: isRTL ? 'row-reverse' : 'row',

			},
			userImage: {
				flex:1,	
				alignItems: 'center',		
			},
			image: {		
				width: 70,
				height: 70,
				borderRadius:60,		
				resizeMode: 'cover',
			},
			commentContent: {
				flex: 3,
				marginTop: 10,
				marginLeft: 10,
				alignItems: isRTL ? 'flex-end' : 'flex-start'
			},
			userName: {
				marginBottom:10,
			},
			userNameText: {
				fontWeight: 'bold',
				fontSize: 20,
				color: '#000000',
			},
			commentTextBox: {
				marginBottom: 10,
			},
			commentText: {
				color: '#000000',
				lineHeight:25,
			},
			seprator: {
				flex:1,
				borderWidth:1, 
				borderColor:'#eff0f2', 
				width: width-60, 
				marginVertical: 20,				
			},
			viewMore: {
				justifyContent: 'center', 
				alignItems: 'center', 
				backgroundColor: '#ecf2f9',
				marginHorizontal:100,
				borderRadius:25,
				paddingVertical:10,
				marginTop: 10,

			},
			viewMoreText: {
				color:'#000',
				fontWeight: 'bold',
			}
    });
  }
}