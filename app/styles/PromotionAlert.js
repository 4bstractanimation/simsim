import i18n from 'react-native-i18n';
import { Text, View, Alert, TouchableHighlight, StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get("window");

export default class StyleSheetFactory{  
  static getSheet(isRTL){
    isRTL ? i18n.locale = 'ar':i18n.locale = 'en';
    return StyleSheet.create({
		  container:{
				flex: 1,
				
				paddingHorizontal:10,
				backgroundColor:'#ecf2f9',
				
			},
			_button:{
			borderRadius:20,
		    marginVertical: 15,
		    backgroundColor:'#000000',
		    width: 150,
		    alignSelf: 'flex-end',
		    alignItems: 'center',
			},
			_buttonText: {
				fontSize:15,
				fontWeight: 'bold',
				color: 'white',
				marginVertical:10,
			},
			_title:{
				fontSize:20,
				fontWeight:'normal',
				marginBottom:5
			},
			_messageBox:{
				
				flexDirection: isRTL ? 'row-reverse' : 'row',
				alignItems: 'center',
				padding: 10,
				marginBottom: 10,
				backgroundColor: '#FFFFFF',
			},
			_textBox:{
				flex: 2
			},
			_descriptionContainer:{
				flexDirection:isRTL ? 'row-reverse' : "row",
				flexWrap: 'wrap',
				justifyContent: "space-between",
			},
			_description:{
				fontSize: 15,
		 		color: '#000000',
		 		flexWrap: 'wrap',
		 		marginRight:20,
			},
			
    });
  }
}