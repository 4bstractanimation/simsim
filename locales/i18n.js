import ReactNative from 'react-native';
import I18n from 'react-native-i18n';
console.log('======== I18n ==============')
console.log(I18n)

// Import all locales
import en from './en.json';
import he from './he.json';
import ar from './ar.json';
I18n.defaultLocale = 'he'

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en,
  he,
  ar
};

const currentLocale = I18n.currentLocale();
console.log('==== currentLocale =======')
console.log(currentLocale)

// Is it a RTL language?
export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;

// Allow RTL alignment in RTL languages
//I18n.locale = 'he';
ReactNative.I18nManager.allowRTL(isRTL);
//I18n.locale = 'he';

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export default I18n;